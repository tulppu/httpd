#include <pthread.h>

#include "deque.h"
#include "../httpd/httpd.h"



void deque_init(Deque_Store* storage, const char* name, FREE_ITEM_FN fn_free)
{
	storage->num_items = 0;
	storage->head = storage->tail = NULL;
	storage->name = NULL;
	storage->fn_free = fn_free;
	pthread_mutex_init(&storage->lock, NULL);

	if (name) storage->name = strdup(name);
}

static inline void _pair_items(Deque_Item* item1, Deque_Item* item2)
{
	item1->prev = item2;
	item2->next = item1;
}

void deque_lock(Deque_Store* storage)
{
	pthread_mutex_lock(&storage->lock);
} 

void deque_unlock(Deque_Store* storage)
{
	pthread_mutex_unlock(&storage->lock);
}


static inline Deque_Item* _get_next(Deque_Item* item) { return (Deque_Item*) item->next; }
static inline Deque_Item* _get_prev(Deque_Item* item) { return (Deque_Item*) item->prev; }

static inline void _ensure_node(Deque_Store* store, Deque_Item* it)
{
	if (it == store->head)
	{
		if (store->num_items > 1)
		{
			HTTPD_DEBUG_ASSERT( _get_next(it) == NULL );
			HTTPD_DEBUG_ASSERT( _get_prev(it)->next == it );
		}
		else
		{
			HTTPD_DEBUG_ASSERT( store->num_items == 1 );
			HTTPD_DEBUG_ASSERT( it->next == NULL );
			HTTPD_DEBUG_ASSERT( it->prev == NULL );

		}
	}
	else if (it == store->tail)
	{
		HTTPD_DEBUG_ASSERT( _get_prev(it) == NULL );
		HTTPD_DEBUG_ASSERT( _get_next(it)->prev == it );
	}
	else
	{
		HTTPD_DEBUG_ASSERT( _get_prev(it)->next == it );
		HTTPD_DEBUG_ASSERT( _get_next(it)->prev == it );
	}
}

static inline void _ensure_store(Deque_Store* store)
{
	Deque_Item* it = store->head;
	uint16_t items_counted = 0;

	while (it)
	{
		_ensure_node(store, it);
		items_counted++;
		it = it->prev;
	}
	HTTPD_DEBUG_ASSERT(items_counted == store->num_items);

	if (store->num_items == 0)
	{
		HTTPD_DEBUG_ASSERT(store->head == NULL);
		HTTPD_DEBUG_ASSERT(store->tail == NULL);
	}

	if (store->num_items == 1)
	{
		HTTPD_DEBUG_ASSERT(store->head);
		HTTPD_DEBUG_ASSERT(store->head->prev == NULL);
		HTTPD_DEBUG_ASSERT(store->tail == NULL);
	}
	if (store->num_items == 2)
	{
		HTTPD_DEBUG_ASSERT(store->head->prev == store->tail);
		HTTPD_DEBUG_ASSERT(store->tail->next == store->head);
	}
	if (store->num_items >= 2)
	{
		HTTPD_DEBUG_ASSERT(store->tail != store->head);
		HTTPD_DEBUG_ASSERT(store->head != NULL);
		HTTPD_DEBUG_ASSERT(store->tail != NULL);
	}

}


static inline int _add_finish(Deque_Store* store, Deque_Item *item)
{
	//HTTPD_PRINT("%p lisätty jonoon.\n", item);
	store->num_items++;
	//HTTPD_DEBUG_PRINT("Added %p to store %s, new num_items %d\n", item, store->name, store->num_items);

	#ifdef HTTPD_DEBUG
	_ensure_store(store);
	#endif

	deque_unlock(store);
	return 1;
}

int deque_add_item(Deque_Store* store, void* _item)
{
	deque_lock(store);
	if (store->num_items == 1024)
	{
		HTTPD_DEBUG_PRINT("Store %s is full.\n", store->name);
		deque_unlock(store);
		return 0;
	}

	Deque_Item* item = (Deque_Item*) _item;
	//httpd_client_lock(client);
	item->next = item->prev = NULL;
	//httpd_client_unlock(client);

	//HTTPD_DEBUG_PRINT("Lisätään %p varastoon %s\n", _item, store->name);

	if (HTTPD_LIKELY(store->num_items >= 2))
	{
		puts("1.");
		// place to tail.
		HTTPD_DEBUG_ASSERT(store->head);
		HTTPD_DEBUG_ASSERT(store->head->prev);
		HTTPD_DEBUG_ASSERT(store->tail);
		HTTPD_DEBUG_ASSERT(store->tail->next);
		HTTPD_DEBUG_ASSERT(store->tail != store->head);

		item->next = store->tail;
		store->tail->prev = item;
		store->tail = item;
		return _add_finish(store, item);
	}

	// Storage is empty, place to head..
	if (store->head == NULL)
	{
		puts("2.");
		HTTPD_DEBUG_ASSERT(store->num_items == 0);
		HTTPD_DEBUG_ASSERT(store->head == NULL);
		HTTPD_DEBUG_ASSERT(store->tail == NULL);
		store->head = item;

		HTTPD_DEBUG_ASSERT(store->head->next == NULL);
		HTTPD_DEBUG_ASSERT(store->tail == NULL);

		return _add_finish(store, item);
	}

	// Storage has head only.
	if (store->tail == NULL)
	{
		puts("3.");
		HTTPD_DEBUG_ASSERT(store->num_items == 1);
		HTTPD_DEBUG_ASSERT(store->tail == NULL);
		HTTPD_DEBUG_ASSERT(store->head != NULL);
		
		store->head->prev = store->tail = item;
		store->tail->next = store->head;

		HTTPD_DEBUG_ASSERT(store->head->prev == store->tail);
		HTTPD_DEBUG_ASSERT(store->tail->next == store->head);
		HTTPD_DEBUG_ASSERT(store->head != store->tail);
		HTTPD_DEBUG_ASSERT(store->tail != NULL);

		return _add_finish(store, item);
	}
	HTTPD_DEBUG_PRINT("Deque f*cked!\n");
	exit(1);

	return 0;
}

Deque_Item* deque_pop_item (Deque_Store* store)
{
	deque_lock(store);
	#ifdef HTTPD_DEBUG
	_ensure_store(store);
	#endif

	Deque_Item* res = store->head;

	if (store->head)
	{
		if (res->prev)
		{
			((Deque_Item*)res->prev)->next = NULL;
		}
		store->head = res->prev;

		if (store->head == store->tail)
		{
			store->tail = NULL;
		}
		
		//HTTPD_DEBUG_PRINT("Giving %p\n",res);
		store->num_items--;
	}

	#ifdef HTTPD_DEBUG
	_ensure_store(store);
	#endif
	deque_unlock(store);

	return res;
}

int deque_remove_item(Deque_Store* store, Deque_Item* item)
{
	deque_lock(store);

	//_pair_items(item->next, item->prev);
	if (item->prev)
	{
		((Deque_Item*)item->prev)->next = item->next;
	}
	if (item->next)
	{
		((Deque_Item*)item->next)->prev = item->prev;
	}

	if (item == store->head) { store->head = item->prev; }
	if (item == store->tail) { store->tail = item->next; }
	if (store->head == store->tail) { store->tail = NULL; }

	store->num_items--;
	_ensure_store(store);
	HTTPD_DEBUG_PRINT("Removed %p from store %s, new num_items %d\n", item, store->name, store->num_items);

	deque_unlock(store);

	return 1;
}

void deque_store_free(Deque_Store* store)
{
	Deque_Item* item = NULL;
	while ((item = deque_pop_item(store)) != NULL)
	{
		//deque_remove_item(store, item);
		if (store->fn_free)
		{
			store->fn_free(item);
		}
	}
	if (store->name) free(store->name);
}
