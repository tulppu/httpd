#ifndef MIME_H_
#define MIME_H_

#include <stdio.h>

typedef struct Ext2MimeItem
{
	char* ext;
	char* mime;
} Ext2MimeItem;

typedef struct Mime2ExtTableIndex
{
	Ext2MimeItem* item;
	void* next;
} Mime2ExtTableIndex;

typedef struct Mime2ExtTable
{
	//unsigned long* map;
	Mime2ExtTableIndex** map;
	_Atomic size_t n_items;
	_Atomic size_t n_indexes;
	size_t cap;
} Mime2ExtTable;




void mimes_2_ext_table_init(Mime2ExtTable* table, size_t nnumb);
// "converts" item Ext2MimeIndex and frees mime.
void mimes_2_ext_table_add(Mime2ExtTable* table, Ext2MimeItem* item);
Ext2MimeItem* mimes_2_ext_table_get(Mime2ExtTable* table, const unsigned char* ext);
char* mimes_2_ext_table_get_str(Mime2ExtTable* table, const unsigned char* ext);

void mimes_2_ext_table_build(Mime2ExtTable* table, const char* mimes_file, size_t exts_max);
void mimes_2_ext_table_free(Mime2ExtTable* table);

#endif
