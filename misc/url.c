#include <stdbool.h>
#include <stdint.h>
#include <locale.h>
#include <stdio.h>
#include <string.h>
#include <sys/param.h>
#include <stdlib.h>
#include <errno.h>

#include "../httpd/httpd.h"
#include "url.h"

static unsigned char reserved_chars[] = {
	'\'',
	'#', '&', '!', '(', ')',
	'*', '+', ',', '/', ':',
	';', '=', '?', '@', '[',
	']', '-', '_', '.', '~',
	'0', '1', '2', '3', '4',
	'5', '6', '7', '8' ,'9',
};

static const uint8_t MAX_RESERVED_CHARCODE = 126; // ~
unsigned char reserved_chars_table[126];
static bool map_initd = false;

inline static void _init_map()
{
	memset(reserved_chars_table, 0, sizeof(reserved_chars_table));
	for (uint8_t i = 0; i < (sizeof(reserved_chars) / sizeof(unsigned char)); i++)
	{
		unsigned char reserved_key = reserved_chars[i];
		assert(reserved_key <= MAX_RESERVED_CHARCODE);
		reserved_chars_table[reserved_key] = 1;
	}
	map_initd = true;
}

/*
	Returns encoded URI in malloc()'d NULL terminated string and places it's length in encoded_len.
*/
char* encode_uri(const char *source, const uint32_t* source_len, uint32_t *encoded_len)
{
	if (map_initd == false)
	{
		_init_map();
	}
	
	uint32_t encoded_cap = 32;
	char *encoded_dest = malloc(encoded_cap);

	for (uint32_t i = 0; i < (uint32_t) *source_len; i++)
	{
		HTTPD_DEBUG_ASSERT(encoded_cap + 5 < UINT32_MAX);
		if ((*encoded_len) + 4 > encoded_cap)
		{
			encoded_cap *= 2;
			encoded_dest = realloc(encoded_dest, encoded_cap);
			//*encoded_dest = rea
		}
		const unsigned char curr_chr = source[i];
		if (
			(curr_chr >= 'a' && curr_chr <= 'z')
			|| (curr_chr >= 'A' && curr_chr <= 'Z')
		)
		{
			encoded_dest[*encoded_len] = curr_chr;
			(*encoded_len)++;
		}
		else
		{
			const int32_t chars_added = 
				sprintf(
						((char*) encoded_dest + *(encoded_len)),
						"%x", 
						curr_chr);
			*encoded_len += chars_added;
		}	
	}
	encoded_dest[*encoded_len] = 0;

	HTTPD_PRINT_ERROR("%.*s (len %d) -> %s\n",  
			(int) *source_len,
			source,
			(int) *source_len,
			encoded_dest
	);

	return encoded_dest;
}

// https://en.wikipedia.org/wiki/Percent-encoding
int decode_url_path(const char* encoded_url, const uint32_t encoded_len, unsigned char* decoded_url, uint32_t *decoded_len) 
{
	if (map_initd == false)
	{
		_init_map();
	}

	for (uint32_t i = 0; i < encoded_len; i++)
	{
		const unsigned char curr_chr = encoded_url[i];
		// Path ended, query string follows.
		if (curr_chr == '?')
		{
			break;
		}

		// a-Z and (unencoded) reserved characters.
		if (
				curr_chr <= MAX_RESERVED_CHARCODE 
				&& (
					(	curr_chr >= 'a' && curr_chr <= 'z')
					|| (curr_chr >= 'A' && curr_chr <= 'Z')
					|| ( reserved_chars_table[curr_chr])
				)
		)
		{
			decoded_url[*decoded_len] = (unsigned char) curr_chr;
			(*decoded_len)++;
			continue;
		}

		// eg. encoded_url + i should look something like %5D...
		if (curr_chr == '%' && i <= encoded_len - 3)
		{
			// D5...
			const char *hex_start = encoded_url + i + 1;
			const uint8_t hex_len = 2;
			char hex_str[3];
			memcpy(hex_str, hex_start, hex_len);
			hex_str[2] = 0;

			const long hex_val = strtol(hex_str,  NULL, 16);
			// Probably can't really happen with 2 chars?
			if (errno == ERANGE && (hex_val == LONG_MIN || hex_val == LONG_MAX))
			{
				HTTPD_PRINT_NOTICE("Invalid url '%s'", encoded_url);
				return 0;
			}
			//printf("dec_len: %u || num: %ld || hex_len: %d\n", *decoded_len, num, hex_len);
			decoded_url[*decoded_len] = (unsigned char) hex_val;
			(*decoded_len)++;

			i += hex_len;
			continue;
		}
	}
	return 1;
}
