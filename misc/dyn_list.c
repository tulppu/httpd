
#include <stdlib.h>
#include "dyn_list.h"


void httpd_dlist_init(Httpd_DList *list)
{
	list->cap = 0;
	list->n = 0;
	list->items = NULL;
}

void httpd_dlist_realloc(Httpd_DList *list, uint32_t new_cap)
{
	if (new_cap == 0) new_cap = 4;
	list->cap = new_cap;
	list->items = (void**) realloc(list->items, list->cap * 2 * sizeof(void*)); 
}

Httpd_DList* httpd_dlist_new()
{
	Httpd_DList *list = (Httpd_DList*)  malloc(sizeof(Httpd_DList));
	httpd_dlist_init(list);

	return list;
}


Httpd_DList* httpd_dlist_push(Httpd_DList *list, void* item)
{
	if (list == NULL) {
		list = (Httpd_DList*) httpd_dlist_new();
	}
	if (list->n == list->cap) {
		httpd_dlist_realloc(list, 2 * list->cap);
	}
	list->items[list->n] = item;
	list->n++;
	return list;
}

