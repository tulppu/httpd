#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>  
#include <unistd.h>


// https://www.gnu.org/software/libc/manual/html_node/Backtraces.html
// Compile with -rdynamic to make symbols visible.
void
print_trace (void)
{
  void *array[15];
  char **strings;
  int size = backtrace (array, 15);
  strings = backtrace_symbols (array, size);
  if (strings != NULL)
  {
    printf ("\n\nObtained %d stack frames.\n", size);
    for (int i = 0; i < size; i++)
      printf ("%s\n", strings[i]);
  }

  free (strings);
}

int open_fd_count()
{
    DIR *dp = opendir("/proc/self/fd");
    struct dirent *de;
    int count = -3; // '.', '..', dp

    if (dp == NULL)
        return -1;

    while ((de = readdir(dp)) != NULL)
        count++;

    (void)closedir(dp);
	return count;
}


// https://stackoverflow.com/a/11221207
int FILE_get_path(FILE *fp, char* buf)
{
	int MAXSIZE = 0xFFF;
	char proclnk[0xFFF];
	int fno = fileno(fp);
	ssize_t r;
	sprintf(proclnk, "/proc/self/fd/%d", fno);
	r = readlink(proclnk, buf, MAXSIZE);
	if (r < 0)
	{
		printf("failed to readlink\n");
		return 0;
	}
	buf[r] = '\0';
	return 1;
}
