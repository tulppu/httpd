#ifndef DYN_LIST_
#define DYN_LIST_

#include <stdlib.h>
#include <stdint.h>

typedef struct Httpd_Dyn_List
{
	void** items;
	uint32_t n;
	uint32_t cap;
} Httpd_Dyn_List;
typedef Httpd_Dyn_List Httpd_DList;


void httpd_dlist_init(Httpd_DList *list);
void httpd_dlist_realloc(Httpd_DList *list, uint32_t new_cap);
Httpd_DList* httpd_dlist_new();
Httpd_DList* httpd_dlist_push(Httpd_DList *list, void* item);

#endif
