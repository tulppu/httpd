#ifndef URL_H_ 
#define URL_H_ 

#include <stdint.h>

/*
 * Initilisation isn't thread safe, do one dry run in the main thread before starting worker threads. 
 * Result will be placed in decoded_url and it must be able to hold the buffer. Allocating it by encoded_len bytes should be more than enough.
 * decoded_url will not be null terminated.
*/
int decode_url_path(const char* encoded_url, const uint32_t encoded_len, unsigned char* decoded_url, uint32_t *decoded_len);
char* encode_uri(const char *source, const uint32_t* source_len, uint32_t *encoded_len);

#endif
