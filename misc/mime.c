
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <stddef.h>

#include "mime.h"

static inline ssize_t _read_mimes(const char* file_path, Ext2MimeItem** result, const size_t exts_max)
{
	FILE* file = fopen(file_path, "r");
	if (file == NULL)
	{
		printf("failed to read mime types from file '%s' (%s)", file_path, strerror(errno));
		return -1;
	}

	ssize_t nread = 0;
	size_t exts_found = 0;

	const uint16_t line_len = 1024;
	char *line = malloc(line_len);

	while (((nread = getline(&line, (size_t*) &line_len, file)) != -1) && exts_found < exts_max)
	{
		if (line[0] == '#') continue;
		char * mime = strtok(line, " \t\n");
		char* ext = NULL;
		while( (ext = strtok(NULL, " \t\n")) != NULL )
		{
			Ext2MimeItem* item = malloc(sizeof(Ext2MimeItem));
			item->mime = strdup(mime);
			item->ext = strdup(ext);
			result[exts_found] = item;
			exts_found++;
			if (exts_found == exts_max)
			{
				printf("Max mimes %ld reached, can't add more.", exts_max);
				break;
			}
		}
	}
	free(line);

	fclose(file);
	return exts_found;
}


// www.cse.yorku.ca/~oz/hash.html
static inline unsigned long
_hash(unsigned char *str)
{
	if (str == NULL) return 0;

	unsigned long hash = 5381;
	int c;

	while ((c = *str++))
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

static inline unsigned long
_mimes_2_ext_hash(Mime2ExtTable* table, char *ext)
{
	return _hash((unsigned char*)ext) % table->cap;
}

void mimes_2_ext_table_init(Mime2ExtTable* table, size_t nnumb)
{
	table->cap = nnumb;
	//int reqd_size = table->cap * sizeof(void*);
	//void *data =  malloc((int)(nnumb * sizeof(void*)));
	//table->map = data;
	table->map = malloc((int)(nnumb * sizeof(void*)));
	//void*mount_point  =  (Mime2ExtTableIndex**) malloc((int)(nnumb * sizeof(void*)));
	//memset(table->map, 0, table->cap * sizeof(void*));
	for (unsigned long i = 0; i < table->cap; i++) table->map[i] = NULL;
	assert(table->map);
	table->n_items = 0;
	table->n_indexes = 0;
}

void mimes_2_ext_table_add(Mime2ExtTable* table, Ext2MimeItem* item)
{
	unsigned long hash = _mimes_2_ext_hash(table, item->ext);
	assert(hash <= table->cap);
		
	Mime2ExtTableIndex *index = malloc(sizeof(Mime2ExtTableIndex));
	index->item = item;
	index->next = NULL;

	Mime2ExtTableIndex* col = table->map[hash];
	if (col)
	{
		index->next = col;
		table->map[hash] = index;
	}
	else
	{
		table->map[hash] = index;
		table->n_indexes++;
	}
	table->n_items++;
}

Ext2MimeItem* mimes_2_ext_table_get(Mime2ExtTable* table, const unsigned char* ext)
{
	if (table->n_items == 0) return NULL;

	unsigned long hash = _mimes_2_ext_hash(table,  (char*) ext);
	Mime2ExtTableIndex* index = table->map[hash];
	while (index)
	{
		if (strcmp(index->item->ext, (const char*) ext) == 0)
		{
			return index->item; 
		}
		index = index->next;
	}
	return NULL;
}

char* mimes_2_ext_table_get_str(Mime2ExtTable* table, const unsigned char* ext)
{
	Ext2MimeItem* res = mimes_2_ext_table_get(table, ext);
	return (res) ? res->mime : NULL;
}

void mimes_2_ext_table_build(Mime2ExtTable* table, const char* mimes_file, size_t exts_max)
{
	Ext2MimeItem* items[exts_max];
	const size_t exts_found = _read_mimes(mimes_file, items, exts_max);

	if (exts_found) {
		printf("Found %ld exts\n", exts_found);
		mimes_2_ext_table_init(table, exts_found * 1.3);
		for (size_t i = 0; i < exts_found; i++)
			mimes_2_ext_table_add(table, items[i]);
	}
	assert(mimes_2_ext_table_get(table, (const unsigned char*)"html") != NULL);
}


void mimes_2_ext_table_free(Mime2ExtTable* table)
{
	for (size_t i = 0; i < table->cap; i++)
	{
		Mime2ExtTableIndex* index = table->map[ (unsigned long) i];
		if (index == NULL) continue;

		while (index)
		{
			Mime2ExtTableIndex* next = index->next;
			free(index->item->ext);
			free(index->item->mime);
			free(index->item);
			free(index);
			index = next;
		}
	}
	free(table->map);
}
