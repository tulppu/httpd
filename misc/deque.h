#ifndef DEQUE_H_
#define DEQUE_H_ 

#include <stdint.h>
#include <pthread.h>

typedef void(*FREE_ITEM_FN)(void*);

typedef struct Deque_Item 
{
	void* next;
	void* prev; // could be stipped off.
} Deque_Item;

// Linked list used to store malloc'd unused clients for reuse.
typedef struct Deque_Store
{
	_Atomic uint32_t	num_items;
	Deque_Item*			head;
	Deque_Item*			tail;
	pthread_mutex_t		lock;
	char*				name;
	FREE_ITEM_FN		fn_free; // function used to call as store item as paramater when freeing the whole store.
} Deque_Store;

void 			deque_lock(Deque_Store* storage);
void 			deque_unlock(Deque_Store* storage);

void 			deque_init(Deque_Store* store, const char* name, FREE_ITEM_FN fn_free);
void 			deque_store_free(Deque_Store* store);
/*
 * Item is casted to Deque_Item and first 16 bytes of it are assigned to for next and prev pointers.
 * Item is appended to the tail.
*/
int 			deque_add_item(Deque_Store* store, void* _item);

int				deque_remove_item(Deque_Store* store, Deque_Item* item);

/*
 * Removes first item from the queue and returs it 
*/
Deque_Item* 	deque_pop_item (Deque_Store* store);


#endif
