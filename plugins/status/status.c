#include <unistd.h>
#include <stdio.h>
//#include <errno.h>
#include <string.h>
#include <pwd.h>
#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "../../httpd/httpd.h"
#include "../../httpd/request.h"
#include "../../httpd/callbacks.h"
#include "../../httpd/endpoint.h"
#include "../../httpd/header.h"
#include "../../httpd/plugin.h"


int status_head_finished_cb(Httpd_Request *request)
{
	if (strcmp((char*)request->path, "/status/") != 0) return 0;
	
	request->found_provider = true;

	Httpd_Client *client = request->client;
	httpd_req_write(request, request->headers, request->headers_len);

	const uint32_t res_len = 1024 + strlen((char*)request->path);
	assert(strlen((char*)request->path) + 1024 <= UINT32_MAX);
	char msg_stats[res_len + 1];
	memset(msg_stats, 0, res_len + 1);


	const char *s = R"(

Polku: %s
Luettu: %ld
Lahettty: %ld
Yhteyksia: %d
Saie: %ld
Asiakas: %p
Vapautettuja asiakkaita: %d
Laajennuksia: %d

)";

	const int msg_len = sprintf(msg_stats, s,
								request->path,
								request->client->httpd->stats.bytes_read,
								request->client->httpd->stats.bytes_sent,
								request->client->httpd->clients_amount,
								pthread_self(),
								request->client,
								request->client->httpd->stores.client_store.num_items,
								request->client->httpd->stores.plugin_store.num_items
																// request->client->httpd->worker.works_queued);
	);
	httpd_req_write(request, msg_stats, msg_len);
	
	Worker* worker = &request->client->httpd->worker;
	for (uint8_t i = 0; i < worker->num_processors; i++)
	{
		char msg_worker[128];
		int msg_worker_len = snprintf(msg_worker, 127, "Tyolaisella %d tehtavia %d\n", i, worker->processors[i]->num_works);
		httpd_req_write(request, msg_worker, msg_worker_len);
	}
	httpd_req_write(request, "\n", 1);
	httpd_client_write(client, " on ", 4);

	Httpd_Headers_Container headers;
	httpd_headers_container_init(&headers);
	////headers.list.
	httpd_headers_parse(request, &headers);
	for (uint32_t i = 0; i < headers.list.n; i++)
	{
		const Httpd_Header *header = headers.list.items[i];

		const char *msg_header = "Otsikkotieto ";
		httpd_client_write(client, msg_header, strlen(msg_header));
		httpd_client_write(client, header->key, header->key_len);
		httpd_client_write(client, " on ", 4);
		httpd_client_write(client, header->val, header->val_len);
		httpd_req_write(request, "\n", 1);
	}

	for (uint8_t i = 0; i < client->httpd->endpoint_storage->num_mounts; i++)
	{
		Httpd_Endpoint_Mount* mount = client->httpd->endpoint_storage->mounts[i];
		char msg[256];
		const uint16_t msg_mount_len = (uint16_t) snprintf(msg, 255, "'%s':n alla %d kohdetta\n", mount->root_url, mount->num_endpoints);

		httpd_req_write(request, msg, msg_mount_len);

		for (uint16_t y = 0; y < mount->num_endpoints; y++)
		{
			const Httpd_Endpoint* endpoint = mount->endpoints[y];

			char endpoint_msg[512];

			// Access only if endpoint->handler->type is static
			Httpd_Endpoint_Handler_Static *static_handler = (Httpd_Endpoint_Handler_Static*) endpoint->handler;

			const int msg_endpoint_len = snprintf(endpoint_msg, 511, "\t[%s] [%s] [%ld tavua]\n\t%s\n\n",
				(endpoint->handler->type == ENDPOINT_HANDLER_STATIC) ? "kiintea" : "joustava",
				(endpoint->handler->type == ENDPOINT_HANDLER_STATIC) ? static_handler->mime : "",
				(endpoint->handler->type == ENDPOINT_HANDLER_STATIC) ? static_handler->size  : 0,
				endpoint->name
			);
			httpd_req_write(request, endpoint_msg, msg_endpoint_len);
		}
		httpd_req_write(request, "\n", 1);
	}
	httpd_req_write(request, "\n", 1);

	HTTPD_DEBUG_PRINT("GET valmis, vastataan ja suljetaan %d\n", request->client->fd);

	httpd_client_request_close(client);

	return 1;
}

int httpd_plugin_start(Httpd* httpd, Httpd_Plugin* plugin)
{
	httpd_plugin_init(httpd, plugin, "HTTPD Status (example plugin)", "Serves /status/ -endpoint providing some statistics and info.");
	httpd_callback_register_head_finished(httpd, status_head_finished_cb);
	//httpd_callback_unregister(plugin->httpd, HTTPD_CB_ID_HEAD_FINISHED, mount_point_head_finished_cb);
	return 1;
} 

int httpd_plugin_exit(Httpd_Plugin* plugin)
{
	//httpd_callback_unregister(plugin->httpd, HTTPD_CB_ID_HEAD_FINISHED, status_head_finished_cb);

	return 1;
}
