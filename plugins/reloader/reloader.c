#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <sys/types.h>

#include <sys/socket.h>
#include <sys/inotify.h>
#include <sys/epoll.h>

#include <linux/limits.h>

#include "../../httpd/httpd.h"
#include "../../httpd/engine.h"
#include "../../httpd/plugin.h"


int inotify_fd, inotify_wd = 0;

const char* MONITORED_DIR = "plugins/bin/";

Httpd_Plugin* self = NULL;

int file_changed(Httpd* httpd, int fd, struct epoll_event* _ev)
{
	HTTPD_PRINT("File changed!\n");
	char buffer[sizeof (struct inotify_event) * 8];

	int read_len, i = 0;
	while (
		(read_len = read(fd, buffer, sizeof(buffer)))
		 && errno != EINTR
		 && i < read_len 
		// && (errno != EAGAIN
		//&& errno != EWOULDBLOCK
	)
	{

		struct inotify_event *event = ( struct inotify_event * ) &buffer;
		i += event->len;

		if (strlen(event->name) < 3)
			continue;

		if (strcmp(".so", event->name + strlen(event->name) - 3) != 0)
			continue;

		char _full_path[PATH_MAX];
		snprintf(_full_path, sizeof(_full_path), "%s%s", MONITORED_DIR, (char*) event->name);

		// If problems occurs see https://insanecoding.blogspot.com/2007/11/pathmax-simply-isnt.html
		char real_path[PATH_MAX];
		realpath(_full_path, real_path);

		HTTPD_PRINT("Processing file %s\n", real_path);
		HTTPD_PRINT("mask: %u\n", event->mask);;

		if (strlen(real_path) == 0)
		{
			HTTPD_DEBUG_PRINT("Failed to resolve real path of '%s'\n", _full_path);
			continue;
		}

		Httpd_Plugin* plugin_existing = httpd_plugin_get_by_path(httpd, real_path);

		if (
				event->mask & IN_DELETE
			//||	event->mask & IN_MODIFY	
		)
		{
			HTTPD_PRINT("..unloading..\n");

			if (plugin_existing == self)
			{
				HTTPD_PRINT_NOTICE("..wont self unload, for now.\n");
				continue;
			}

			if (plugin_existing)
			{
				httpd_plugin_unload(httpd, plugin_existing);
				httpd_plugin_free(plugin_existing);
				HTTPD_PRINT("..unloaded.\n");
			} else HTTPD_PRINT_NOTICE("File '%s' was deleted but it wasn't a registered plugin.\n", real_path);
		}

		if (
				event->mask & IN_CLOSE_WRITE
			||	event->mask & IN_MOVED_TO
			||	event->mask & IN_CREATE
			//||	event->mask & IN_MODIFY
		)
		{
			HTTPD_PRINT("Adding..?\n");
			if (plugin_existing)
			{
				HTTPD_PRINT_NOTICE("Won't addp plugin '%s' since it's already loaded.", real_path);
			} else httpd_plugin_load_file(httpd, real_path);
			continue;
		}

	}
	return 1;
}


int httpd_plugin_start(Httpd* httpd, Httpd_Plugin* plugin)
{
	struct epoll_event ev;
	inotify_fd = inotify_init1(IN_NONBLOCK | IN_CLOEXEC);
	if (inotify_fd < 0)
	{
		HTTPD_PRINT_PERROR("Failed to initalize inotify (returned %d)\n", inotify_fd);
		return 0;
	}

	if ((inotify_wd = inotify_add_watch(inotify_fd, MONITORED_DIR, IN_CLOSE_WRITE | IN_MOVED_TO | IN_DELETE | IN_CREATE)) == -1)
	//if ((inotify_wd = inotify_add_watch(inotify_fd, MONITORED_DIR, IN_ALL_EVENTS)) == -1)
	{
		HTTPD_PRINT_PERROR("Failed to add %s to inowatch (returned %d)\n", MONITORED_DIR, inotify_fd);
		return 0;
	}

	HTTPD_PRINT_NOTICE("ionified(), adding it to epollers.\n");

	ev.events = EPOLLIN;
	ev.data.fd = inotify_fd;
	httpd_engine_event_register_on_cb(httpd, inotify_fd, &ev, file_changed);

	self = plugin;

	httpd_plugin_init(httpd, plugin, "Plugin Reloader", "Adds or reloads plugins from build/plugins directory using inotify. Can cause crash if plugin is about to be executed same time when it's unloaded.");
	return 1;
} 

int httpd_plugin_exit(Httpd_Plugin* plugin)
{
	if (inotify_fd)
	{
		httpd_engine_unpoll_event(plugin->httpd, inotify_fd);
		inotify_rm_watch(inotify_fd, inotify_wd);
		close(inotify_fd);
	}

	return 1;
}
