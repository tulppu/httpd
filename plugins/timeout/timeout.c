#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>

#include <sys/timerfd.h>
#include <sys/epoll.h>

#include <dlfcn.h> 

#include "../../httpd/httpd.h"
#include "../../httpd/client.h"
#include "../../httpd/engine.h"
#include "../../httpd/plugin.h"


int timer_fd = 0;

#define TIMEOUT 4
#define TIMEOUT_INTERVAL 1

/*
 * Qeueue made out of Httpd_Client(s) sorted by client->last_activity 
 * The last active clients gets pushed to tail.
 * The queue is checked/consumed from tail every TIMEOUT_INTERVAL seconds.
 * 
 * next points to left (head) 
 * prev point to right (tail)
 */
typedef struct 
{
	Httpd_Client* head;
	Httpd_Client* tail;
	pthread_mutex_t lock;
} Sorted_Client_Queue;

static Sorted_Client_Queue queue;

static inline void _lock()		{ pthread_mutex_lock(&queue.lock); }
static inline void _unlock()	{ pthread_mutex_unlock(&queue.lock); }

static inline void _pair(Httpd_Client* cl1, Httpd_Client* cl2)
{
	if (cl1) cl1->prev = cl2;
	if (cl2) cl2->next = cl1;
}

/*
 * Replace head or add new one.
 */
static inline void _add_to_queue(Httpd_Client* client)
{
	_lock();
	client->next = NULL;
	_pair(client, queue.head);
	queue.head = client;
	if (queue.tail == NULL) { queue.tail = client; }
	_unlock();
}

static inline void _remove_from_queue(Httpd_Client* client)
{
	_lock();
	_pair(client->next, client->prev);
	if (queue.head == client) { queue.head = client->prev; }
	if (queue.tail == client) { queue.tail = client->next; }
	_unlock();
}

static inline void _move_to_head(Httpd_Client* client)
{
	_remove_from_queue(client);
	_add_to_queue(client);
}

static inline void _eat_tail(Httpd* httpd)
{
	uint32_t now = time(NULL) - httpd->stats.started_at;
	while (queue.tail)
	{
		if (now < TIMEOUT || queue.tail->last_activity > now - TIMEOUT)
			break;

		Httpd_Client* client = queue.tail;
		queue.tail = client->next;
		if (queue.tail == client) queue.tail = NULL;
		if (httpd_client_is_status_or_less(client, CLIENT_READING))
		{
			HTTPD_PRINT_NOTICE("Timeouting %d\n", client->fd);
			httpd_client_set_status(client, CLIENT_CLOSING);
			httpd_client_terminate(client);
		}
	}
}

extern void httpd_client_refresh(Httpd_Client* client)
{
	_move_to_head(client);
}

static int _cb_client_registered(Httpd_Client* client)
{
	_add_to_queue(client);

	return 1;
}

static int _cb_client_unregistering(Httpd_Client* client)
{
	_remove_from_queue(client);

	return 1;
}

static int _interval_tick(Httpd* httpd, int fd, struct epoll_event* ev)
{
	char buf[10];
	// consume the mock event
	int read_status = read(fd, buf, sizeof(buf));

	_eat_tail(httpd);

	return 1;
}

static void _create_intervaller(Httpd* httpd)
{
	timer_fd = timerfd_create(CLOCK_REALTIME, 0);
	// Interval every 1 second.
	struct itimerspec spec =
	{
		{ TIMEOUT_INTERVAL, 0 }, 
		{ TIMEOUT_INTERVAL, 0 }
	};

	timerfd_settime(timer_fd, 0, &spec, NULL);
	struct epoll_event ev = httpd_engine_create_event(timer_fd, 0, EVENT_TYPE_CALLBACK);
	httpd_engine_event_register_on_cb(httpd, timer_fd, &ev, _interval_tick);
}

#include <sys/ptrace.h>

int httpd_plugin_start(Httpd* httpd, Httpd_Plugin* plugin)
{
	/*
	if (ptrace(PTRACE_TRACEME, 0, NULL, 0) == -1) {
		HTTPD_DEBUG_PRINT("Disbaled timeouting inside trace.\n");
		return 0;
	}*/

	queue.head = queue.tail = NULL;
	pthread_mutex_init(&queue.lock, NULL);

	httpd_plugin_init(httpd, plugin, "Timeout", "Timeout inactive connections.");

	httpd_callback_register_client_registerd(httpd, _cb_client_registered);
	httpd_callback_register_client_unregistering(httpd, _cb_client_unregistering);

	_create_intervaller(httpd);

	return 1;
} 

int httpd_plugin_exit(Httpd_Plugin* plugin)
{
	if (timer_fd) close(timer_fd);
	return 1;
}
