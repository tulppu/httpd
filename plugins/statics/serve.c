
#define _GNU_SOURCE 
#define _XOPEN_SOURCE

#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h> 
#include <stdio.h> 
#include <assert.h>
#include <magic.h>
#include <errno.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/unistd.h>
#include <dlfcn.h>

#include <poll.h>
#include <time.h>
#include <zlib.h>

#include "statics.h"
#include "zpipe.h"
#include "../../httpd/httpd.h"
#include "../../httpd/httpd.h"
#include "../../httpd/endpoint.h"
#include "../../httpd/header.h"
#include "../../httpd/worker.h"


struct Serve_Params
{
	Httpd_Endpoint_Handler_Static* handler;
	//FILE* file;
	size_t file_size;
	off_t total_processed;
	Httpd_Request* req;
	Work* work;
	void* zstream;
};

void chunked_stream_ready(void* _, void* _work)
{
	Work* work = (Work*) _work;
	work_free(work);
}



static inline int _httpd_endpoint_static_sendfile(struct Serve_Params* params);
static inline int _httpd_endpoint_static_chunked(struct Serve_Params* params);

int httpd_endpoint_serve_static(Httpd_Endpoint_Handler* _handler, Httpd_Request* request)
{
	const Httpd_Endpoint_Handler_Static *handler =
		(Httpd_Endpoint_Handler_Static*) _handler;
	HTTPD_PRINT(
			"Serving static file '%s' (%ld bytes)\n",
			handler->file_path,
			handler->size
	);
	request->found_provider = true;

	char *mofified_since = NULL;
	int dest_len = 0;
	if (
		httpd_header_get_str(
			request, "if-modified-since",
			&mofified_since,
			&dest_len)
		&& dest_len < 64
		)
	{
		struct tm tm;
		memset(&tm, 0, sizeof(tm));
		// Not sure if safe to accept unsanitized string..
		strptime(
			mofified_since, 
			"%a, %d %b %Y %H:%M:%S %Z",
			&tm
		);

		time_t time = mktime(&tm);
		bool not_changed = time < handler->mdate;

		if (not_changed)
		{
			HTTPD_PRINT("..not modified\n");
			//HTTPD_PRINT("..unchanged file?");
			httpd_req_write(request, "HTTP/1.1 304 Not Modified\r\n", 0);
			httpd_req_end_headers(request);
			httpd_req_finished(request);
			return 1;
		}
	}

	httpd_req_write(request, "HTTP/1.1 200 OK\r\n", 0);
	httpd_req_write_header(request, "Connection", "keep-alive");
	httpd_req_write_header(request, 
			"Cache-Control",
			"public, must-revalidate, proxy-revalidate, max-age=1"
	);

	if (handler->mime)
	{
		httpd_req_write_header(request, "Content-Type", handler->mime);
	}

	if (handler->str_mdate)
	{
		httpd_req_write_header(request, "Last-Modified", handler->str_mdate);
	}

	if (request->method == HTTPD_HEAD)
	{
		httpd_client_refresh(request->client);
		httpd_req_finished(request);

		return 1;
	}

	struct Serve_Params params;

	params.handler = (Httpd_Endpoint_Handler_Static*) handler;
	params.file_size = handler->size;
	params.total_processed = 0;
	params.req = request;
	params.work = NULL;
	params.zstream = NULL;
	//process_compressed_stream(&params);


	if ((handler->stream_deflate) && false)
	{
		return _httpd_endpoint_static_chunked(&params);
	} else {
		return _httpd_endpoint_static_sendfile(&params);
	}
	//httpd_req_finished(request);
}

static inline int _out_chunked(
		struct Serve_Params* params, 
		char *buf, 
		size_t buf_size);

static inline int _out_deflated(
		struct Serve_Params* params, 
		char *buf, 
		size_t buf_size
)
{
	if (params->zstream == NULL)
	{
		HTTPD_DEBUG_PRINT("Initalizing the zstream\n");

		//httpd_req_write_header(params->req, "Transfer-Encoding", "chunked");
		//httpd_req_write_header(params->req, "Content-Encoding", "deflate");
		z_stream* zstream = (z_stream*) malloc(sizeof(z_stream));

		zstream->zalloc = Z_NULL;
		zstream->zfree = Z_NULL;
		zstream->opaque = Z_NULL;
		zstream->avail_in = Z_NULL;
		zstream->next_in = Z_NULL;
		// https://github.com/libyal/assorted/blob/main/documentation/Deflat&e%20(zlib)%20compressed%20data%20format.asciidoc
		int windowBits = 8;
		int ENCODING = 15;

		int ret = deflateInit2(
				zstream,
				Z_DEFAULT_COMPRESSION,
				Z_DEFLATED, 
				(windowBits | ENCODING), 
				8,
				Z_DEFAULT_STRATEGY
		);
		if (ret != Z_OK)
		{
			HTTPD_PRINT_ERROR("Failed to initalize zstream\n");
			free(params);
			exit(-1);
		}
		params->zstream = zstream;
	}
	
	const int CHUNK = 1024 * 16;
	unsigned char out[CHUNK];

	z_stream* strm = params->zstream;
	strm->avail_in = buf_size;
	strm->next_in = (unsigned char*) buf; 

	//int flush = feof(infile) ? Z_FINISH : Z_NO_FLUSH;
	int flush = (params->total_processed >= (off_t) params->file_size - CHUNK)
			? Z_FINISH : Z_NO_FLUSH;
	HTTPD_DEBUG_PRINT("Deflate iteration (flush: %d)\n", flush);
	//HTTPD_DEBUG_PRINT("CHUNK: %d\n", CHUNK);
	do {
		strm->avail_out = CHUNK;
		strm->next_out = out;
		const uint32_t had = strm->avail_in;

		int ret = deflate(strm, flush);
		assert(ret != Z_STREAM_ERROR);
		int have = CHUNK - strm->avail_out;
		HTTPD_DEBUG_PRINT("avail_in %d, avail_out: %d\n",
				strm->avail_in,
				strm->avail_out);

		const uint32_t consumed = had - strm->avail_in;
		HTTPD_DEBUG_PRINT("deflator took %d bytes in and gave %d bytes out..\n",
			consumed,
			have
		);
		params->total_processed += consumed;
		HTTPD_DEBUG_PRINT("..%ld/%ld bytes deflated\n",
			params->total_processed,
			params->file_size
		);
		httpd_client_refresh(params->req->client);
		if (have) {
			const int wrote = _out_chunked(params, (char*) out, have);
			if (wrote <= 0) {
				HTTPD_DEBUG_PRINT("_out_chunked returned %d\n", wrote);
				(void)deflateEnd(strm);
				return 0;
			}
		}
	} while (strm->avail_out == 0);

	if ((size_t) params->total_processed == params->file_size)
	{
		HTTPD_DEBUG_PRINT("..file compressed!\n");
		deflateEnd(strm);
		free(params->zstream);
	}

	return 1;
}
static inline int _out_chunked(struct Serve_Params* params, char *buf, size_t buf_size)
{
	const int sent_out = httpd_req_write_chunk(
		params->req,
		(char*) buf, 
		buf_size
	);
	httpd_client_refresh(params->req->client);
	//fwrite(buf, 1, read_n, outfile);
	HTTPD_DEBUG_PRINT("..sent %d\n", sent_out);
	return sent_out;
}


static inline int _httpd_endpoint_static_chunked(struct Serve_Params* params)
{
	typedef const uint16_t _TYPE_BUF;
	_TYPE_BUF BUF_SIZE = 1024 * 16;
	char buf[BUF_SIZE];
	const Httpd_Endpoint_Handler_Static* handler = params->handler;
	//FILE *infile = fdopen(handler->served_fd, "rb");
	FILE *infile = fopen(handler->file_path, "rb");
	if (!infile) {
		HTTPD_DEBUG_PRINT("Failed to open fd %d (%s)\n", handler->served_fd, handler->file_path)
	}
	if (ferror(infile)) {
		HTTPD_PRINT_ERROR("Failed to open file for read!\n");
		return -1;
	}
	fseek(infile, 0, SEEK_SET);

	httpd_req_write_header(params->req, "Content-Encoding", "deflate");
	httpd_req_write_header(params->req, "Transfer-Encoding", "chunked");
	httpd_req_end_headers(params->req);

	while (
			(size_t)params->total_processed < params->file_size
			&& httpd_client_is_status(params->req->client, CLIENT_READING)
		)
	{
		if (ferror(infile)) {
			HTTPD_DEBUG_PRINT("File error!\n")
			break;
		}
		//fseek(infile, params->total_processed, SEEK_SET);
		//_TYPE_BUF read_n = fread(buf, 1, BUF_SIZE, infile);
		memset(buf, 0, sizeof(buf));
		//int read_n = read(handler->served_fd, buf, BUF_SIZE);
		int read_n = fread(buf, 1, BUF_SIZE, infile);
		HTTPD_DEBUG_PRINT("REaD %d\n", read_n);
		//params->total_processed += read_n;
		if (read_n) {
			//const int sent_out = _out_chunked(params, buf, read_n);
			const int write_ok = _out_deflated(params, buf, read_n);
			if (write_ok <= 0) {
				HTTPD_DEBUG_PRINT("..breaking\n");
				break;
			}
		}
	}
	HTTPD_DEBUG_PRINT("Completed %ld/%ld\n", params->total_processed, params->file_size);
	//fseek(infile, 0, SEEK_SET);
	httpd_req_end_chunked(params->req);
	fclose(infile);

	httpd_client_request_close(params->req->client);
	httpd_req_finished(params->req);

	return 1;
}

static inline int _httpd_endpoint_static_sendfile(struct Serve_Params* params)
{

	Httpd_Request *request = params->req;
	Httpd_Endpoint_Handler_Static *handler = params->handler;

	if (handler->encoding == ENCODING_DEFLATE)
	{
		httpd_req_write_header(request, "Content-Encoding", "deflate");
	}
	httpd_req_write_header(request, "Content-Length", handler->str_size);
	httpd_req_end_headers(request);


	off_t *off = &params->total_processed;
	const int cli_fd = request->client->fd;

	const clock_t begin = clock();
	while ((size_t) off != handler->size)
	{
		// TODO: don't send massive files in single tick to avoid blockig the thread.
		const ssize_t bytes_sent = sendfile(
				request->client->fd,
				handler->served_fd,
				off,
				handler->size
		);

		if (bytes_sent == -1 && errno != EAGAIN)
		{
			HTTPD_PRINT_PERROR("sendfile() %d", cli_fd);
			return 0;
		}

		if (bytes_sent == -1 && errno == EAGAIN)
		{
			struct pollfd pfds[2];
			pfds[0].events = POLLOUT;
			pfds[0].fd = request->client->fd;

			pfds[1].events = POLLRDHUP | POLLHUP;
			pfds[1].fd = request->client->fd;

			const int poll_status = poll(
				pfds, 
				sizeof(pfds) / sizeof(struct pollfd),
				1.5 * 1000
			);

			if (poll_status == -1)
			{
				HTTPD_PRINT_PERROR("poll() %d", cli_fd);
				break;
			}

			if (pfds[1].revents & POLLERR)
			{
				HTTPD_DEBUG_PRINT(
					"POLLERR while waiting sendfile() to become ready.\n"
				);
				break;
			}

			if (pfds[1].revents &= (POLLHUP | POLLRDHUP))
			{
				HTTPD_DEBUG_PRINT(
					"Sock %d closed while poll()'d sendfile() to become ready.\n",
					cli_fd
				);
				return 0;
			}
			if (poll_status == 0)
			{
				HTTPD_DEBUG_PRINT("timeout..?\n");
			}
			HTTPD_PRINT("..again writable?\n");
			continue;
		}
		httpd_client_refresh(request->client);
		if (bytes_sent == 0)
		{
			break;
		}
		request->client->httpd->stats.bytes_sent += bytes_sent;
	}
	const clock_t end = clock();
	const double time_spent = (double)(end - begin);  
	HTTPD_DEBUG_PRINT("a Send of '%s' [%ld bytes] to fd %d took %fµs\n",
			request->path,
			*off, 
			handler->served_fd, 
			time_spent
	);

	HTTPD_PRINT("Wrote %ld bytes to client %d(%p)\n",
			*off, 
			request->client->fd, 
			request->client
	);

	if ((size_t)off != handler->size)
	{
		HTTPD_DEBUG_PRINT("Sent only %ld of total %ld bytes to client %d\n",
				*off, 
				handler->size,
				request->client->fd
		);
	}

	httpd_client_refresh(request->client);
	httpd_req_finished( (Httpd_Request*) request);

	return 1;

}
