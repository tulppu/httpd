
#ifndef _ZPIPE_H_
#define _ZPIPE_H_

#include <zlib.h> 
#include "../../httpd/endpoint.h"

#define CHUNK_SIZE 16384 
int		def(FILE* source , int *dest_fd, int level, size_t *bytes_written);
int		deflate_file(Httpd_Endpoint_Handler_Static* handler, const int* out_file_fd);
size_t 	def_chunk(FILE* source, unsigned char* out, size_t *out_size);
int 	inflate(struct z_stream_s* strm, int flush);
int 	inflateEnd(struct z_stream_s* strm);
//void 	def_tick(struct Chunk_Params *params);

extern void* zlib_handle;

//typedef struct z_stream_s {
//    z_const Bytef *next_in;     /* next input byte */
//    uInt     avail_in;  /* number of bytes available at next_in */
//    uLong    total_in;  /* total number of input bytes read so far */
//
//    Bytef    *next_out; /* next output byte will go here */
//    uInt     avail_out; /* remaining free space at next_out */
//    uLong    total_out; /* total number of bytes output so far */
// 
//   z_const char *msg;  /* last error message, NULL if no error */
//    struct internal_state FAR *state; /* not visible by applications */
//
//    alloc_func zalloc;  /* used to allocate the internal state */
//    free_func  zfree;   /* used to free the internal state */
//    voidpf     opaque;  /* private data object passed to zalloc and zfree */
//
//    int     data_type;  /* best guess about the data type: binary or text
//                           for deflate, or the decoding state for inflate */
//    uLong   adler;      /* Adler-32 or CRC-32 value of the uncompressed data */
//    uLong   reserved;   /* reserved for future use */
//} z_stream;


#endif
