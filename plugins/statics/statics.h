#ifndef _STATICS_H_
#define _STATICS_H_

#include "../../httpd/endpoint.h"

// zpipe chunk size.
// #define CHUNK_SIZE 512 

int static_index_executor(Httpd_Endpoint_Handler* _handler, Httpd_Request* req);
int load_mount_dirs(Httpd* httpd, FN_ENDPOINT_PREPARER preparer);
int httpd_endpoint_serve_static(Httpd_Endpoint_Handler* handler, Httpd_Request* request);

int prepare_endpoint_static(void *_mount, Httpd_Request* req);
int prepare_endpoint_static_lazy(void *_mount, Httpd_Request* req);

// TODO: would fit better in the main program..?
static Httpd_Endpoint_Handler_Dynamic STATIC_INDEX_HANDLER = {
	.type		= ENDPOINT_HANDLER_DYNAMIC,
	.executor	= static_index_executor
};



static Httpd_Endpoint_Mount* root_dir = NULL;
#endif
