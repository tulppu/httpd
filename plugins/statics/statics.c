
#define _GNU_SOURCE 
#define _XOPEN_SOURCE

#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h> 
#include <stdio.h> 
#include <assert.h>
#include <magic.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/unistd.h>
#include <dlfcn.h>

#include <poll.h>
#include <time.h>

#include "statics.h"
#include "zpipe.h"

#include "../../httpd/httpd.h"
#include "../../httpd/plugin.h"
#include "../../httpd/endpoint.h"

void* zlib_handle = NULL;

// TODO: move to main progam.
int _cb_head_finised(Httpd_Request *req)
{
	Httpd* httpd = req->client->httpd;
	uint32_t mount_len = 0;

	Httpd_Endpoint_Mount* mount = httpd_endpoint_mount_storage_find_mount_by_url(
		httpd->endpoint_storage,
		(char*) req->path,
		mount_len
	);

	if (mount == NULL)
	{
		HTTPD_PRINT_NOTICE("Mount not found.\n");
		return 0;
	}

	return mount->endpoint_preparer(mount, req);
}


/* in mount.c */
//int load_mount_dirs(Httpd* httpd, FN_ENDPOINT_PREPARER preparer);

int httpd_plugin_start(Httpd* httpd, Httpd_Plugin* plugin)
{
	zlib_handle = dlopen("libz.so", RTLD_NOW | RTLD_GLOBAL);
	if (!zlib_handle)
	{
		HTTPD_PRINT_ERROR("%s\n", dlerror());
		HTTPD_PRINT_NOTICE("libz.so not found, won't deflate files.\n")
	} else HTTPD_PRINT_NOTICE("Found libz.so! :-)\n");

	httpd_plugin_init(httpd, plugin, "Static files", "Loads and serves static directories.");
	httpd_callback_register_head_finished(httpd, _cb_head_finised);

	load_mount_dirs(httpd, prepare_endpoint_static);
	
	return 1;
}

int httpd_plugin_exit(Httpd_Plugin* plugin)
{
	//httpd_callback_unregister(plugin->httpd, HTTPD_CB_ID_HEAD_FINISHED, _cb_head_finised);
	if (zlib_handle)
	{
		HTTPD_PRINT_NOTICE("Closing zlib handle\n");
		if (dlclose(zlib_handle) != 0)
		{
			HTTPD_PRINT_ERROR("..dlclose error: %s\n", dlerror());
		}
	}

	return 1;
}
