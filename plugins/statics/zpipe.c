/* zpipe.c: example of proper use of zlib's inflate() and deflate()
   Not copyrighted -- provided to the public domain
   Version 1.4  11 December 2005  Mark Adler */

/* Version history:
   1.0  30 Oct 2004  First version
   1.1   8 Nov 2004  Add void casting for unused return values
                     Use switch statement for inflate() return values
   1.2   9 Nov 2004  Add assertions to document zlib guarantees
   1.3   6 Apr 2005  Remove incorrect assertion in inf()
   1.4  11 Dec 2005  Add hack to avoid MSDOS end-of-line conversions
                     Avoid some compiler warnings for input and output buffers
 */

#include <stdio.h>
//#include <string.h>
#include <assert.h>
#include <errno.h>

#include "../../httpd/httpd.h"
#include "zpipe.h"



/* Compress from file source to file dest until EOF on source.
   def() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_STREAM_ERROR if an invalid compression
   level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
   version of the library linked do not match, or Z_ERRNO if there is
   an error reading or writing the files.
   
   Changed FILE* dest to be int *dest_fd so write() can be done to socket instead of file.
   Added size_t *bytes_written to keep track of written size.
   */
int def(FILE* source , int *dest_fd, int level, size_t *bytes_written)
{
    int ret, flush;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK_SIZE];
    unsigned char out[CHUNK_SIZE];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK_SIZE, source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            strm.avail_out = CHUNK_SIZE;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK_SIZE - strm.avail_out;
            size_t write_res = 0;
            if ((write_res = write(*dest_fd, out, have)) != have) {
                (void)deflateEnd(&strm);
                return Z_ERRNO;
            }
            *bytes_written += write_res;
        } while (strm.avail_out == 0);
        assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */

    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}

int def_part(FILE* source , int *dest_fd, int level, size_t *bytes_written)
{
    int ret, flush;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK_SIZE];
    unsigned char out[CHUNK_SIZE];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK_SIZE, source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            strm.avail_out = CHUNK_SIZE;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK_SIZE - strm.avail_out;
            size_t write_res = 0;
            if ((write_res = write(*dest_fd, out, have)) != have) {
                (void)deflateEnd(&strm);
                return Z_ERRNO;
            }
            *bytes_written += write_res;
        } while (strm.avail_out == 0);
        assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */

    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}

size_t def_chunk(FILE* source, unsigned char* out, size_t *out_size)
{

    /* allocate deflate state */
    z_stream strm;
    strm.zfree = strm.opaque = strm.zalloc = Z_NULL;
    //strm.total_out = 0;
    if (deflateInit(&strm, -1) != Z_OK)
    {
        HTTPD_PRINT_ERROR("deflateInit: '%s'", strm.msg);
        return -1;
    }

    unsigned char in[CHUNK_SIZE];
	const int bytes_to_process = fread(in, 1, CHUNK_SIZE, source);

    strm.avail_in = bytes_to_process;
    strm.next_in = in;
    strm.next_out = out;
    strm.avail_out = CHUNK_SIZE;
    deflate(&strm, Z_FINISH);
    *out_size = bytes_to_process - strm.avail_out;
    (void)deflateEnd(&strm);

    return strm.total_in - strm.avail_in;
}

/* Decompress from file source to file dest until stream ends or EOF.
   inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_DATA_ERROR if the deflate data is
   invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
   the version of the library linked do not match, or Z_ERRNO if there
   is an error reading or writing the files. */
int inf(FILE *source, FILE *dest)
{
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK_SIZE];
    unsigned char out[CHUNK_SIZE];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    int ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK_SIZE, source);
        if (ferror(source)) {
            (void)inflateEnd(&strm);
            return Z_ERRNO;
        }
        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = CHUNK_SIZE;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
				case Z_NEED_DICT:
					ret = Z_DATA_ERROR;     /* and fall through */
					//break; // changed
					(void)inflateEnd(&strm);
					return ret;
				case Z_DATA_ERROR:
				case Z_MEM_ERROR:
					(void)inflateEnd(&strm);
					return ret;
				}
            have = CHUNK_SIZE - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)inflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

#include <sys/stat.h>
#include "../../httpd/httpd.h"
#include "../../httpd/endpoint.h"

int deflate_file(Httpd_Endpoint_Handler_Static* handler, const int* out_file_fd)
{
	HTTPD_DEBUG_PRINT("Deflating %s\n", handler->file_path);
	FILE* file_to_deflate = fopen(handler->file_path, "r");

	if (!file_to_deflate) {
		HTTPD_PRINT_PERROR("Failed to open file %s for deflation", handler->file_path);
		return 0;
	}

    HTTPD_DEBUG_ASSERT(file_to_deflate);
    HTTPD_DEBUG_ASSERT(*out_file_fd);

    size_t deflated_size = 0;

    def(file_to_deflate, (int*) out_file_fd, -1, &deflated_size);
   
	if (!deflated_size) {
		return -1;
	}

    #ifdef HTTPD_DEBUG
	HTTPD_DEBUG_ASSERT(deflated_size);
    struct stat out_file_stat;
    if (fstat(*out_file_fd, &out_file_stat) == -1)
    {
        HTTPD_PRINT_ERROR("stat for %d returned -1", (int)*out_file_fd);
        return -1;
    }
	HTTPD_DEBUG_ASSERT(out_file_stat.st_size);
    HTTPD_DEBUG_ASSERT(out_file_stat.st_size == (off_t) deflated_size);
    #endif


    /*HTTPD_PRINT_NOTICE(
		"Deflated %s [%.2f%%][%zd -> %ld]\n",
		handler->file_path,
		(1.0-(float)((float)deflated_size/(float)handler->size))*100.0,
		handler->size, 
		deflated_size
    );*/

    if (deflated_size < handler->size)
    {
        handler->served_fd = (int) *out_file_fd;
        handler->size = deflated_size;
        handler->encoding = ENCODING_DEFLATE;
        return 1;
    }

    //HTTPD_PRINT_NOTICE("Omitted deflated file since the original file is smaller.\n");
    return 0;
}

/* report a zlib or i/o error */
/*
void zerr(int ret)
{
    fputs("zpipe: ", stderr);
    switch (ret) {
    case Z_ERRNO:
        if (ferror(stdin))
            fputs("error reading stdin\n", stderr);
        if (ferror(stdout))
            fputs("error writing stdout\n", stderr);
        break;
    case Z_STREAM_ERROR:
        fputs("invalid compression level\n", stderr);
        break;
    case Z_DATA_ERROR:
        fputs("invalid or incomplete deflate data\n", stderr);
        break;
    case Z_MEM_ERROR:
        fputs("out of memory\n", stderr);
        break;
    case Z_VERSION_ERROR:
        fputs("zlib version mismatch!\n", stderr);
    }
}*/
