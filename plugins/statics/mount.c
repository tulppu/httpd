#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>

#include "statics.h"
#include "zpipe.h"

#include "../../httpd/httpd.h"
#include "../../httpd/request.h"
#include "../../httpd/endpoint.h"


static inline int _read_static_file(Httpd_Endpoint_Handler_Static* handler, Httpd* httpd)
{
	struct stat sb;
	if (stat(handler->file_path, &sb) == -1)
	{
		//HTTPD_PRINT("load_file error: %s\n", strerror(errno));
		HTTPD_PRINT_ERROR("Failed to stat() '%s'\n", handler->file_path);
		return 0;
	}

	if (S_ISDIR(sb.st_mode))
	{
		handler->is_dir = true;
	}

	handler->executor		= httpd_endpoint_serve_static;
	handler->size			= sb.st_size;
	handler->mdate			= sb.st_mtim.tv_sec;
	handler->encoding		= ENCODING_NONE;
	handler->stream_deflate	= handler->tmp == true || handler->size >= 1024 * 1024 * 4; 
	handler->stream_deflate	= true; 

	const size_t file_len = strlen(handler->file_path);
	// find ext
	char* ext_start = NULL;
	uint8_t i_ext = 0;
	while (ext_start == NULL && i_ext < 6 && file_len - i_ext >= 1)
	{
		if (handler->file_path[file_len - i_ext] == '.')
			ext_start = handler->file_path + file_len - i_ext + 1;
		i_ext++;
	}
	handler->mime = mimes_2_ext_table_get_str(&httpd->mimes_2_ext_table, (const unsigned char*) ext_start);


	if (
			false
			&& zlib_handle 
			&& handler->stream_deflate == false 
			&& handler->is_dir == false 
			&& handler->mime 
			&& strncmp(handler->mime, "image/", 7) != 0
	)
//	if (true)
	{
		FILE* out_file = tmpfile();
		if (out_file) {
			const int out_file_fd = fileno(out_file);
			if (deflate_file(handler, &out_file_fd) <= 0)
			{
				close(out_file_fd);
			}
		}
	}

	if (handler->served_fd == 0)
		handler->served_fd = open(handler->file_path, O_RDONLY);
	HTTPD_DEBUG_ASSERT(handler->served_fd);
	lseek(handler->served_fd, 0, SEEK_SET);

	if (handler->size)
	{
		char str_size[80];
		sprintf(str_size, "%ld", handler->size);
		handler->str_size = strdup(str_size);
	} else handler->str_size = strdup("0");

	if (handler->mdate)
	{
		char str_mdate[128];
		strftime(str_mdate, 128, "%a, %d %b %Y %H:%M:%S %Z", gmtime(&handler->mdate));
		handler->str_mdate = strdup(str_mdate);
	}

	//HTTPD_PRINT_NOTICE("Loaded static file %s\n", handler->file_path);

	return 1;
}


static inline void _httpd_mount_recursive(const Httpd_Endpoint_Mount *mount, const char* child_dir)
{
	struct dirent *dir;

	// dirname from mount combined with possible child_dir
	char *full_dir = NULL; 

	if (child_dir)
	{
		//HTTPD_PRINT("child: %s\n", child_dir);
		assert(child_dir[strlen(child_dir) - 1] == '/');
	}
	assert(
		mount->root_dir[strlen(mount->root_dir) - 1] == '/'
	);

	if (child_dir)
	{
		size_t len = strlen(mount->root_dir) + strlen(child_dir) + 2;
		char _full_dir[len];
		memset(_full_dir, 0, len);
		len = sprintf(_full_dir, "%s%s", mount->root_dir, (char*) child_dir);
		//_full_dir[len] = '\0';
		full_dir = strdup(_full_dir);

	}
	else 
	{
		full_dir = strdup(mount->root_dir);
	}

	DIR *d;
	d = opendir(full_dir);

	if (d == 0)
	{
		HTTPD_PRINT_PERROR("Failed to open %s\n", full_dir);
		exit(0);
	}

	while ((dir = readdir(d)) != NULL)
	{
		const int len_child_dir = (child_dir) ? strlen(child_dir) : 0;
		char child_path[
				len_child_dir
			+	strlen(dir->d_name) 
			+	2
		];
		#pragma GCC diagnostic push
		#pragma GCC diagnostic ignored "-Wformat-overflow"
		// len_child_dir should secure overflow or weird behaviour from happening if chil_dir is NULL. i hope :--)
		sprintf(child_path, "%.*s%s", len_child_dir, child_dir, dir->d_name);
		#pragma GCC diagnostic pop


		switch(dir->d_type)
		{
			// TODO: resolve and add symlinks
			case DT_DIR:
				if ( 
					strcmp(dir->d_name, ".") != 0
					&& (strcmp(dir->d_name, "..") != 0)
				)
				{
					char* subdir_name = malloc(
						(strlen(child_path))
						 + 1
						 + strlen(dir->d_name )
						 + 2);

					sprintf(subdir_name, "%s/", child_path);
					//HTTPD_PRINT("Ladataan lapsi %s\n", subdir_name);
					_httpd_mount_recursive(mount, subdir_name);
					free(subdir_name);
					continue;
				}
				break;
			case DT_REG:
				uint8_t methods = 0;
				methods |= ((uint8_t) 1 << HTTPD_GET);
				Httpd_Endpoint* endpoint = httpd_endpoint_create(child_path, methods);
				// includes filename
				char* complete_path = malloc(strlen(full_dir) + strlen(dir->d_name) + 2);
				sprintf(complete_path, "%s%s", full_dir, dir->d_name);

				endpoint->handler = (Httpd_Endpoint_Handler*) httpd_endpoint_handler_create_static(NULL, complete_path);
				httpd_endpoint_mount_add_endpoint((Httpd_Endpoint_Mount*) mount, endpoint);
				free(complete_path);
				break;
			default:
				break;
		}
	}
	free(full_dir);
	closedir(d);
}

int prepare_endpoint_static(void *_mount, Httpd_Request* req)
{
	Httpd_Endpoint_Mount *mount = (Httpd_Endpoint_Mount*) _mount;
	Httpd* httpd = req->client->httpd;
	const char* endpoint_name = (char*) req->path + strlen((char*)mount->root_url);

	Httpd_Endpoint* endpoint = httpd_endpoint_mount_find_endpoint(mount, endpoint_name);
	if (endpoint == NULL)
	{
		return 0;
	}

	HTTPD_DEBUG_ASSERT(endpoint->handler->executor);
	FN_ENDPOINT_EXECUTOR executor = (FN_ENDPOINT_EXECUTOR) endpoint->handler->executor;
	executor(endpoint->handler, req);

	return 1;
}

int prepare_endpoint_static_lazy(void *_mount, Httpd_Request* req)
{

	Httpd_Endpoint_Mount *mount = (Httpd_Endpoint_Mount*) _mount;
	Httpd* httpd = req->client->httpd;
	const char* endpoint_name = (char*) req->path + strlen((char*)mount->root_url);

	char file_path[strlen(endpoint_name) + strlen(mount->root_dir) + 1];
	sprintf(file_path, "%s%s", mount->root_dir, endpoint_name);
	HTTPD_PRINT_NOTICE("Serving lazy static file: %s\n", file_path);

	// Not very efficient, should at least stream deflated files.
	Httpd_Endpoint_Handler_Static tmp_static_handler = {
		.tmp = true
	};
	httpd_endpoint_handler_create_static(&tmp_static_handler, file_path);

	if (_read_static_file(&tmp_static_handler, httpd))
	{
		if (tmp_static_handler.is_dir)
		{
			FN_ENDPOINT_EXECUTOR executor =
				(FN_ENDPOINT_EXECUTOR) mount->index_handler->executor;
			executor( (Httpd_Endpoint_Handler*) &tmp_static_handler, req);
		} else {
			httpd_endpoint_serve_static(
				(Httpd_Endpoint_Handler*) &tmp_static_handler, req
			);
		}

		httpd_endpoint_handler_free( (Httpd_Endpoint_Handler*) &tmp_static_handler);
		req->found_provider = true;
		return 1;
	}

	return 0;
}


static inline void _preload_static_mounts(Httpd* httpd)
{
	for (uint8_t i= 0; i < httpd->endpoint_storage->num_mounts; i++)
	{
		Httpd_Endpoint_Mount* mount = httpd->endpoint_storage->mounts[i];
		for (uint32_t y = 0; y < mount->num_endpoints; y++)
		{
			Httpd_Endpoint* endpoint = mount->endpoints[y];
			if (endpoint->handler->type == ENDPOINT_HANDLER_STATIC)
			{
				_read_static_file( (Httpd_Endpoint_Handler_Static*) endpoint->handler, httpd);
			}
		}
	}
}

Httpd_Endpoint_Mount* _httpd_endpoint_mount_statics(Httpd_Endpoint_Mount* mount, const char* _root_dir, FN_ENDPOINT_PREPARER preparer, const bool is_lazy)
{
	mount->root_dir = strdup(_root_dir);
	mount->endpoint_preparer = preparer;

	if (is_lazy == false)
	{
		_httpd_mount_recursive(mount, NULL);
	}

	return mount;
}

int load_mount_dirs(Httpd* httpd, FN_ENDPOINT_PREPARER preparer)
{
	root_dir = httpd_endpoint_mount_create("/");



	Httpd_Endpoint_Mount* mounts[] =
	{
		_httpd_endpoint_mount_statics(
			root_dir,
			"/var/www/html/",
			prepare_endpoint_static,
			false
		),
		_httpd_endpoint_mount_statics(
			httpd_endpoint_mount_create("/piilo/"), 
			"/home/kukas/Ohjelmointi/httpd/static/",
			prepare_endpoint_static,
			false
		),
		_httpd_endpoint_mount_statics(
			httpd_endpoint_mount_create("/tereve/"), 
			"/home/kukas/Ohjelmointi/tereve/client/",
			prepare_endpoint_static,
			false
		),
		_httpd_endpoint_mount_statics(
			httpd_endpoint_mount_create("/static/"), 
			"/home/kukas/Ohjelmointi/httpd/static/",
			prepare_endpoint_static_lazy,
			true	
		),
		_httpd_endpoint_mount_statics(
			httpd_endpoint_mount_create("/tereve_lazy/"), 
			"/home/kukas/Ohjelmointi/tereve/client/",
			prepare_endpoint_static_lazy,
			true
		),
		_httpd_endpoint_mount_statics(
			httpd_endpoint_mount_create("/musa/"), 
			"/home/kukas/Musiikki/",
			prepare_endpoint_static_lazy,
			true
		)
	};

	for (size_t i = 0; i < (sizeof(mounts) / sizeof(Httpd_Endpoint_Mount*)); i++)
	{
		httpd_endpoint_mount_storage_add_item(httpd->endpoint_storage, mounts[i]);
		mounts[i]->index_handler = (Httpd_Endpoint_Handler*) &STATIC_INDEX_HANDLER;
	}

	_preload_static_mounts(httpd);

	return 1;
}
