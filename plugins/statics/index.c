#define _GNU_SOURCE 
#include <ctype.h>
#include <dirent.h>
#include <unistd.h>
#include <linux/limits.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>

#include "../../httpd/endpoint.h"
#include "../../httpd/request.h"
#include "../../misc/url.h"


inline static bool _is_link_dir(const char *link_path)
{
	char buf[PATH_MAX];
	size_t bytes = readlink(link_path, buf, sizeof(buf));
		
	if (bytes == 0) {
		HTTPD_PRINT_PERROR("read_link %s error: \n", link_path);
		return false;
	}
	struct stat link_stat;
	memset(&link_stat, 0, sizeof(link_stat));
	lstat(buf, &link_stat);

	if (S_ISLNK(link_stat.st_mode))
	{
		return _is_link_dir(buf);
	}

	return S_ISDIR(link_stat.st_mode);
}

struct _File_List_Item 
{
	char		*file_name;
	int			*file_name_len;
	__off_t		*file_size;
	bool		*is_dir;
};

inline static int _list_file(Httpd_Request *request, struct _File_List_Item *item)
{
	static const char* LINEBASE = "<a href='%s%c' title='%lld'> %s%c</a>";

	uint32_t encoded_url_len = 0;
	/*const char* encoded_uri = encode_uri(
			item->file_name, 
			(const uint32_t*) item->file_name_len, 
			&encoded_url_len
		);*/

	//HTTPD_PRINT_ERROR("Url: %s\n", encoded_uri);

	/*
	static uint8_t LINEBASE_LEN = 0;
	if (LINEBASE_LEN == 0) LINEBASE_LEN = strlen(LINEBASE);
	char line[
		(int) LINEBASE_LEN 
		+ encoded_url_len
		+ *item->file_name_len
		+ (int)((ceil(log10(*item->file_size))+1)*sizeof(char))
		+ 2
		+ 1];*/


	const int line_len_needed = snprintf(NULL,
		0,
		(char*) LINEBASE,
		//encoded_uri,
		item->file_name,
		((*item->is_dir) ? '/' : ' '),
		*item->file_size,
		item->file_name,
		((*item->is_dir) ? '/' : ' ')
	) + 1;
	//char line[line_len + 1];
	char *line = malloc(line_len_needed + 1);

	const int line_len = snprintf(line,
		line_len_needed + 1,
		(char*) LINEBASE,
		//encoded_uri,
		item->file_name,
		((*item->is_dir) ? '/' : ' '),
		*item->file_size,
		item->file_name,
		((*item->is_dir) ? '/' : ' ')
	);

	line[line_len_needed] = '\0';

	//free((char*)encoded_uri);

	const int res = httpd_req_write_chunk(
			request,
			line,
			line_len) > 0;
	free(line);

	return res;
}


inline static int _list_dirent_file
(Httpd_Request* request, struct dirent *dirent, Httpd_Endpoint_Handler_Static* handler)
{
	const bool is_link = dirent->d_type == DT_LNK;
	const uint32_t entry_name_len = strlen(dirent->d_name);
	char file_path[strlen(handler->file_path) + 1 + entry_name_len + 1];
	file_path[0] = 0;
	const int path_len = sprintf(file_path, "%s%s", handler->file_path, dirent->d_name);

	bool link_is_dir = false;
	if (is_link)
	{
		link_is_dir = _is_link_dir(file_path); 
	}
	//printf("%s is_dir: %d\n", dirent->d_name, link_is_dir);

	const bool is_dir = link_is_dir || dirent->d_type == DT_DIR;

	struct stat s;
	if (stat(file_path, &s) == -1)
	{
		return 0;
	}

	struct _File_List_Item item = {
		.file_name			= dirent->d_name,
		.file_name_len		= (int*) &entry_name_len,
		.file_size			= &s.st_size,
		.is_dir				= (bool*) &is_dir
	};	

	return _list_file(request, &item);
}


int static_index_executor(Httpd_Endpoint_Handler* _handler, Httpd_Request* request)
{
	Httpd_Endpoint_Handler_Static* handler = (Httpd_Endpoint_Handler_Static*) _handler;

	httpd_req_write(request, "HTTP/1.1 200 OK\r\n", 0);
	httpd_req_write_header(request, "Connection", "keep-alive");
	httpd_req_write_header(request, "Content-Type", "text/html");
	httpd_req_write_header(request, "Transfer-Encoding", "chunked");
	httpd_req_write_header(request, "Cache-Control", "public, must-revalidate, proxy-revalidate, max-age=1");
	httpd_req_end_headers(request);

	static const char* head = "<!DOCTYPE html><html><head><meta charset='UTF-8'><style>a { display: block; }</style></head><body><!--<script type='text/javascript' defer src='/static/dir_filter.js'></script>!-->";
	static const char* tail = "</body></html>";

	httpd_req_write_chunk(request, head, strlen(head));

	struct dirent** entries = NULL; 
	// TODO: http://localhost:4488/musa/%2e%2e resolves to home, fix that.
	const int n_ents = scandir(handler->file_path, &entries, NULL, versionsort);
	//qsort(entries, n_ents, sizeof(struct dirent*), dirsort);

	for (int i = 0; i < n_ents; i++)
	{
		const bool break_loop = _list_dirent_file(request, entries[i], handler) == false;
		if (i % 1 == 0) httpd_req_flush(request);
		free(entries[i]);
		//usleep(15000);
		if (break_loop) break;
	}
	free(entries);
	char asd[rand() % 1024 * 16];
	memset(asd, 'A', sizeof(asd));
	httpd_req_write_chunk(request, "<p>A", 4);
	httpd_req_write_chunk(request, asd, sizeof(asd));
	httpd_req_write_chunk(request, "</p>", 4);

	//httpd_req_write_chunk(request, "häh", sizeof("häh"));
	//httpd_req_write_chunk(request, tail, strlen(tail));
	httpd_req_end_chunked(request);

	httpd_client_refresh(request->client);
	httpd_req_finished(request);

	return 1;
}
