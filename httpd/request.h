#ifndef REQUEST_H_
#define REQUEST_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>

#include "httpd.h"
#include "client.h"

typedef struct Httpd_Client Httpd_Client;
typedef struct Httpd Httpd;

#define HTTPD_MAX_HEADERS_SIZE	1024 * 8
#define HTTPD_MAX_BODY_SIZE		1024 * 4
// Min bytes to be allocated for headers for every request. Must always be pow2
#define HTTPD_MIN_HEADERS_SIZE	512 

#define HTTPD_HEADERS_COUNTER	uint16_t
#define HTPPD_HEADERS_MAX_NUM	UINT16_MAX

//const char* CRLF = "\r\n\r\n";

#define HEADER_BUF_LEN			uint16_t	
#define HEADER_BUF_MAX_LEN		UINT16_MAX	

typedef enum _HTTPD_REQ_METHOD {
	HTTPD_GET		= 1,
	HTTPD_POST		= 2,
	HTTPD_PUT		= 3,
	HTTPD_HEAD		= 4,
	HTTPD_CONNECT	= 5,
	HTTPD_OPTIONS	= 6,
	HTTPD_TRACE		= 7,
	HTTPD_DELETE	= 8
} HTTPD_REQ_METHOD;

typedef struct 
{
	size_t max_body_len;
	size_t max_headers_len;
} Httpd_Request_Options;

typedef enum REQ_STATUS
{
	REQ_OK = 1,
	REQ_CLOSE = 2,
	REQ_WOULDBLOCK = 3,
	REQ_HEADERS_FINISHED = 4,
	REQ_BODY_FINISHED = 5,
} REQ_STATUS;


typedef struct Httpd_Request
{
	Httpd_Client*		client;

	HTTPD_REQ_METHOD	method;
	unsigned char*		path;
	char*				query;

	
	struct head_out_buffer
	{
		char buf[HEADER_BUF_MAX_LEN];
		HEADER_BUF_LEN len;
	} head_out_buffer;

	char*				headers;
	size_t				headers_len;

	int 				fd;

	char*				body;
	size_t				body_len;

	void*				next; 
	void*				prev; 
	Httpd_Request_Options* options;

	// Set true after "\r\n\r\n"  is found.
	_Atomic bool				headers_ready;
	_Atomic bool				finished;
	_Atomic bool				found_provider;
	_Atomic bool				headers_out;
} Httpd_Request; 


void			httpd_req_init(Httpd_Request* req);
int				httpd_req_read(Httpd_Client *client, Httpd_Request** _request);
extern int		httpd_req_write(Httpd_Request *request, const char* buf, ssize_t buflen);
extern int 		httpd_req_write_chunk(Httpd_Request *request, const char *buf, ssize_t buflen);
extern int 		httpd_req_end_chunked(Httpd_Request *request);
extern int 		httpd_req_flush(Httpd_Request* request);
extern int		httpd_req_write_header(Httpd_Request *request, const char *key, const char* val);
int				httpd_req_finished(Httpd_Request *request);
Httpd_Request*	httpd_req_alloc(Httpd_Client *client);
void			httpd_req_free(Httpd_Request *request);
void			httpd_req_free_real(Httpd_Request *request);

void 			httpd_req_end_headers(Httpd_Request* request);


#endif
