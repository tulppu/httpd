#ifndef HTTPD_HEADER_H_
#define HTTPD_HEADER_H_

#include <stdlib.h>

#include "httpd.h"
#include "request.h"

#include "../misc/dyn_list.h"

typedef struct 
{
	// Pointer to Httpd_Request->headers
	char*		key;
	int  		key_len;

	/*
	 * Points to request->headers + key_val + key_len
	 * eg. to position in client's header buffer where value for the seeked header starts.
	*/
	char*		val;
	int 		val_len;
} Httpd_Header;

typedef struct 
{
	Httpd_Dyn_List list;
} Httpd_Headers_Container;


void
httpd_headers_container_init(
		Httpd_Headers_Container* container
);

//__attribute__((visibility("default")))
//extern
Httpd_Header*
httpd_header_get
(
	const char* key,
	uint16_t key_len, 
	Httpd_Headers_Container *headers
);

//__attribute__((visibility("default")))
//extern
int 
httpd_header_get_str
(
		Httpd_Request* req,
		const char* key,
		char **res,
		int *res_len
); 

//__attribute__((visibility("default")))
//extern 
int httpd_headers_parse(Httpd_Request* req, Httpd_Headers_Container* headers);

extern 
int
httpd_header_has_str(
	Httpd_Request	*req,
	const char		*key,
	const char 		*q,
	int 			q_len
);
#endif

