#include <dlfcn.h>
#include <errno.h>

#include "plugin.h"
#include "callbacks.h"


void httpd_plugin_load_dir(Httpd* httpd, char* dir_path)
{
	DIR *d;
	struct dirent *f;
	d = opendir(dir_path);

	if (d)
	{
		while ( (f = readdir(d)) != NULL)
		{
			if (f->d_type != DT_REG )
				continue;

			// TODO: Follow symlinks.

			if (strlen(f->d_name) < 3)
				continue;

			size_t name_len = strlen(f->d_name);
			if (strstr(f->d_name, ".so") != f->d_name + name_len-3)
				continue;

			char *plugin_to_add = malloc((strlen(dir_path) + 1 + strlen(f->d_name) + 1));
			sprintf(plugin_to_add, "%s/%s", dir_path, f->d_name);

			char real_path[1024];
			realpath(plugin_to_add, real_path);

			if (strlen(real_path) > 0) 
			{
			   	httpd_plugin_load_file(httpd, real_path); }
			else 
			{
				HTTPD_DEBUG_PRINT("Failed to resolve real path for file '%s'\n", plugin_to_add);
				//printf("Failed to resolve real path for file '%s'\n", plugin_to_add);
			}

			free(plugin_to_add);
		}
	}
	else
	{
		HTTPD_PRINT_PERROR("Failed to load plugin dir '%s'\n", dir_path);
	}
	closedir(d);

	httpd_callback_execute_plugins_loaded(httpd);
}

void httpd_plugin_load_file(Httpd* httpd, char* file_path)
{
	HTTPD_PRINT_NOTICE("Loading '%s'...\n", file_path);
	dlerror();
	void *handle = dlopen(file_path, RTLD_NOW | RTLD_GLOBAL);
	if (handle == NULL)
	{
		HTTPD_PRINT_ERROR("Failed to open plugin!\n");
		HTTPD_PRINT_ERROR("%s\n", dlerror());
		HTTPD_PRINT_NOTICE("..trying lazy..\n");
		handle = dlopen(file_path, RTLD_LAZY | RTLD_GLOBAL);
		if (handle == NULL)
		{
			HTTPD_PRINT_NOTICE("..giving up..\n");
			return;
		}
	}

	FN_PLUGIN_INIT* init_func =  (FN_PLUGIN_INIT*) dlsym(handle, "httpd_plugin_start");
	if (init_func == NULL)
	{
		HTTPD_PRINT_ERROR("No init function found from plugin '%s' \n\n", file_path);
		return;
	}


	Httpd_Plugin* plugin = httpd_plugin_alloc();
	plugin->handle = handle;
	int init_res = 0;
	if ((init_res = init_func(httpd, plugin)) != 1)
	{
		HTTPD_PRINT_ERROR("Failed to initalize plugin '%s' (returned %d)\n\n", file_path, init_res);
		//httpd_plugin_unload(httpd, plugin);
		//dlclose(handle);
		httpd_plugin_free(plugin);
		return;
	}
	httpd_plugin_register(httpd, plugin);
	plugin->exit_func = (FN_PLUGIN_EXIT*) dlsym(handle, "httpd_plugin_exit");
	HTTPD_DEBUG_ASSERT(plugin->exit_func);

	plugin->plugin_path = strdup(file_path);
	HTTPD_PRINT_NOTICE("Initalized %s\n", plugin->plugin_name);
}

Httpd_Plugin* httpd_plugin_alloc()
{
	Httpd_Plugin* plugin = (Httpd_Plugin*) malloc(sizeof(Httpd_Plugin));
	memset(plugin, 0, sizeof(Httpd_Plugin));
	plugin->next
		= plugin->prev 
		= plugin->plugin_desc 
		= plugin->plugin_path
		= plugin->handle
		= plugin->exit_func 
		= NULL;
	return plugin;
}

Httpd_Plugin* httpd_plugin_get_by_path(Httpd* httpd, const char* path)
{
	Deque_Store* store = &httpd->stores.plugin_store;
	deque_lock(store);
 	Httpd_Plugin* res = NULL;
	
	Deque_Item* it = store->tail;
	while (it)
	{
		Httpd_Plugin* plugin = (Httpd_Plugin*) it;
		if (strcmp(plugin->plugin_path, path) == 0)
		{
			res = plugin;
			break;
		}
		it = it->next;
	}
	deque_unlock(store);
	return res;
}

void httpd_plugin_init(Httpd* httpd,  Httpd_Plugin* plugin, char* plugin_name, char* plugin_desc)
{
	plugin->httpd = httpd;
	plugin->plugin_name = strdup(plugin_name);
	plugin->plugin_desc = strdup(plugin_desc);
	plugin->next = plugin->prev = NULL;
}

void httpd_plugin_register(Httpd* httpd, Httpd_Plugin* plugin)
{
	deque_add_item(&httpd->stores.plugin_store, plugin);
}

void httpd_plugin_unload(Httpd* httpd, Httpd_Plugin* plugin)
{
	((FN_PLUGIN_EXIT*)plugin->exit_func)(plugin);
	deque_remove_item(&httpd->stores.plugin_store,  (Deque_Item*) plugin);
}

void httpd_plugin_free(Httpd_Plugin* plugin)
{
	HTTPD_PRINT_NOTICE("Freeing plugin '%s'\n", plugin->plugin_name);
	if (dlclose(plugin->handle) != 0)
	{
		HTTPD_PRINT_ERROR("Failed to close plugin handle: %s\n", dlerror());
	}

	free(plugin->plugin_name);
	free(plugin->plugin_desc);
	free(plugin->plugin_path);

	free(plugin);
}
