
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h> 
#include <stdio.h> 
#include <assert.h>
#include <magic.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <dlfcn.h>

#include "httpd.h"
#include "endpoint.h"


static inline int _cmp_endpoints(const void* ab, const void* bb)
{
	Httpd_Endpoint* a = *(Httpd_Endpoint**) ab;
	Httpd_Endpoint* b = *(Httpd_Endpoint**) bb;
	HTTPD_DEBUG_ASSERT(a->name_len);
	HTTPD_DEBUG_ASSERT(b->name_len);
	return strcmp(a->name, b->name);
}

int httpd_endpoint_mount_add_endpoint(Httpd_Endpoint_Mount* mount, Httpd_Endpoint* endpoint)
{
	mount->endpoints = (Httpd_Endpoint**) realloc(mount->endpoints, (mount->num_endpoints + 1) * sizeof(Httpd_Endpoint*));
	assert(mount->endpoints);
	mount->endpoints[mount->num_endpoints] = endpoint;
	mount->num_endpoints++;

	qsort(
		mount->endpoints,
		mount->num_endpoints,
		sizeof(Httpd_Endpoint*),
		_cmp_endpoints
	);
	//assert(httpd_endpoint_mount_find_endpoint(mount, endpoint->name) != NULL);

	return 1;
}

Httpd_Endpoint* httpd_endpoint_mount_find_endpoint(Httpd_Endpoint_Mount* mount, const char* name)
{
	assert(mount);
	Httpd_Endpoint* key[1] = {
		&(Httpd_Endpoint) {
			.name = (char*) name,
			.name_len = 255 
		}
	};

	//HTTPD_PRINT("Finding provider for '%s'\n", name);

	// TODO: replace by proper index handling.
	if (strnlen(name, 1) == 0 || (name[0] == '/' && strnlen(name, 2) ==  1))
	{
		key[0]->name = "index.html";
		key[0]->name_len = 10;
	}

	Httpd_Endpoint* endpoint = (Httpd_Endpoint*) bsearch(
		&key,
		mount->endpoints,
		mount->num_endpoints,
		sizeof(Httpd_Endpoint*),
		_cmp_endpoints
	);

	return (endpoint) ? *(Httpd_Endpoint**) endpoint : NULL;
}

Httpd_Endpoint* httpd_endpoint_create(const char* name, const uint8_t methods)
{
	Httpd_Endpoint* endpoint = (Httpd_Endpoint*) malloc(sizeof(Httpd_Endpoint));
	endpoint->name = strdup(name);
	endpoint->name_len = strlen(name);
	endpoint->methods =  (uint8_t) methods;

	return endpoint;
} 

void httpd_endpoint_handler_free(Httpd_Endpoint_Handler* handler);
void httpd_endpoint_free(Httpd_Endpoint* endpoint)
{
	Httpd_Endpoint_Handler* handler = endpoint->handler;
	httpd_endpoint_handler_free(handler);
	free(endpoint->name);
	free(endpoint);
}

void httpd_endpoint_handler_free(Httpd_Endpoint_Handler* handler)
{
	switch (handler->type)
	{
		case ENDPOINT_HANDLER_STATIC:
			//Httpd_Endpoint_Handler_Static* handler_static = (Httpd_Endpoint_Handler_Static*) handler;
			Httpd_Endpoint_Handler_Static* handler_static = (Httpd_Endpoint_Handler_Static*) handler;
			free(handler_static->file_path);
			free(handler_static->etag);
			// Remmber, do not set mime free!
			//free(handler_static->mime);
			free(handler_static->str_mdate);
			free(handler_static->str_size);

			close(handler_static->served_fd);

			if (handler_static->tmp == 0)
			{
				pthread_mutex_destroy(&handler_static->mutex);
				free(handler_static);
			} 
			break;
		
		default:
			free(handler);
			break;
	}
}

Httpd_Endpoint_Handler_Static* httpd_endpoint_handler_create_static(Httpd_Endpoint_Handler_Static* handler,  const char* file_path)
{
	if (handler == NULL)
	{
		handler = (Httpd_Endpoint_Handler_Static*) malloc(sizeof(Httpd_Endpoint_Handler_Static));
		handler->tmp = 0;
		//memset(handler, 0, sizeof(Httpd_Endpoint_Handler_Static));
	} else {
		// Assume tmp is allocated from stack and doesn't need to be freed.
		handler->tmp = 1;
	}
	handler->type			= ENDPOINT_HANDLER_STATIC;
	handler->encoding		= ENCODING_NONE;
	handler->file_path		= strdup(file_path);
	
	handler->mime			= NULL;
	handler->etag			= NULL;
	handler->served_fd		= 0;
	handler->size			= 0;
	handler->mdate			= 0;
	handler->str_size		= NULL;
	handler->str_mdate		= NULL;

	handler->stream_deflate	= false;

	if (handler->tmp == 0)
	{
		pthread_mutex_init(&handler->mutex, NULL);
	}

	return handler;
}


/*
 * Mallocs, initalises and return Httpd_Endpoint_Mount.
*/
Httpd_Endpoint_Mount* httpd_endpoint_mount_create(const char* root_url)
{
	Httpd_Endpoint_Mount* mount = (Httpd_Endpoint_Mount*) malloc(sizeof(Httpd_Endpoint_Mount));
	assert(mount);
	mount->root_url = strdup(root_url);
	mount->root_url_len = strlen(root_url);
	mount->root_dir = NULL;

	mount->endpoints = NULL;
	mount->num_endpoints = 0;

	return mount;
}

void httpd_endpoint_mount_free(Httpd_Endpoint_Mount* mount)
{
	free(mount->root_url);
	free(mount->root_dir);

	for (uint32_t i = 0; i < mount->num_endpoints; i++)
	{
		httpd_endpoint_free(mount->endpoints[i]);
	}
	free(mount->endpoints);
	free(mount);
}



void httpd_endpoint_mount_storage_init(Httpd_Endpoint_Mount_Storage* storage)
{
	storage->mounts = NULL;
	storage->num_mounts = 0;
}

Httpd_Endpoint_Mount_Storage* httpd_endpoint_mount_storage_create()
{
	Httpd_Endpoint_Mount_Storage* storage = (Httpd_Endpoint_Mount_Storage*) malloc(sizeof(Httpd_Endpoint_Mount_Storage));
	httpd_endpoint_mount_storage_init(storage);
	return storage;
} 

void httpd_endpoint_mount_storage_free(Httpd_Endpoint_Mount_Storage* storage)
{
	for (uint32_t i = 0; i < storage->num_mounts; i++)
		httpd_endpoint_mount_free(storage->mounts[i]);

	if (storage->mounts)
		free(storage->mounts);

}


static inline int _cmp_mounts(const void* ap, const void* bp)
{
	Httpd_Endpoint_Mount* a = *(Httpd_Endpoint_Mount**) ap;
	Httpd_Endpoint_Mount* b = *(Httpd_Endpoint_Mount**) bp;

	uint32_t min_len = (a->root_url_len > b->root_url_len) ? a->root_url_len : b->root_url_len; 

	return strncmp( (char*) a->root_url, (char*) b->root_url, min_len);
}

void httpd_endpoint_mount_storage_add_item(Httpd_Endpoint_Mount_Storage* storage, Httpd_Endpoint_Mount* mount)
{
	assert(
		httpd_endpoint_mount_storage_find_mount_by_url(storage, mount->root_url, 0) == NULL
	);
	
	storage->mounts = (Httpd_Endpoint_Mount**) realloc(storage->mounts, (storage->num_mounts  + 1) * sizeof(void*));
	assert(storage->mounts);
	storage->mounts[storage->num_mounts] = mount;
	storage->num_mounts++;

	qsort(
		storage->mounts,
		storage->num_mounts,
		sizeof(Httpd_Endpoint_Mount*),
		_cmp_mounts
	);

	assert(
		httpd_endpoint_mount_storage_find_mount_by_url(storage, mount->root_url, 0) != NULL
	);
}

// Finds mount /mount_point/ from path /mount_point/index.html or /mount_point/subdir/index.html (or /asd/mount_point/index.html maybe not?)
Httpd_Endpoint_Mount*
httpd_endpoint_mount_storage_find_mount_by_url
(Httpd_Endpoint_Mount_Storage* storage, const char* url, uint32_t url_len)
{
	if (url_len == 0) url_len = strlen(url);

	
	// cut "index.html" out from "/mount_point/index.html"
	char* url_end = (char*) url + url_len - 1;
	while(url_len && *url_end != '/' && url_end != url)
	{
		url_end--;
	}

	char* pos_mount_end = memchr(url + 1, '/', url_len - 1);
	uint8_t mount_name_len = ( (uintptr_t) pos_mount_end - (uintptr_t) url);
	if (pos_mount_end == NULL) 
	{
		mount_name_len = 0;
	}
	Httpd_Endpoint_Mount* key[1] = {
		&(Httpd_Endpoint_Mount) {
			.root_url = (char*) url,
			.root_url_len = mount_name_len,
		}
	};

	Httpd_Endpoint_Mount* mount = (Httpd_Endpoint_Mount*) bsearch(
		&key,
		storage->mounts,
		storage->num_mounts,
		sizeof(Httpd_Endpoint_Mount*),
		_cmp_mounts
	);
	
	if (mount == NULL)
	{
		HTTPD_PRINT("Mount not found for url '%s'\n", url);
	}
	// TODO: decrease url_len and recurse if not found?
	// "/some/example/path/" will fail to find mount "/some/" or "/example"
	// allow mounting only to first level "/some/" and find mount from beginning?
	return (mount) ? *(Httpd_Endpoint_Mount**) mount : NULL;
}


void httpd_endpoint_mount_storage_serve(Httpd_Endpoint_Mount_Storage* storage, Httpd* httpd)
{
	storage->active = true;
}

