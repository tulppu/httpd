#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>

#include "header.h"
#include "httpd.h"
#include "request.h"


#include "../misc/dyn_list.h"


static inline void _trim_header_value(Httpd_Header *header)
{
	for (
			;
			*header->val <= 32;
			header->val++
		) {
		header->val_len--;
	}	
	for (
			;
		  	*header->val + header->val_len <= 32;
			header->val_len--
		) {
	}	
}

void
httpd_headers_container_init(
		Httpd_Headers_Container* container
) {
	httpd_dlist_init(&container->list);
}


extern
int
httpd_headers_parse(
		Httpd_Request* req,
		Httpd_Headers_Container* headers
)
{
	const char* head = (const char*)(req->headers);
	const char* head_end = req->headers + (unsigned int) req->headers_len;
	
	for (
		char *cursor = (char*) head; 
		cursor <= head_end; 
		cursor++
	)
	{
		const size_t left = head_end - cursor;
		const char* key_end = memchr(cursor, ':', left);
		if (!key_end) {
			HTTPD_PRINT_NOTICE("No key found for header\n");
			break;
		}
		char* val_end = strstr(
			key_end,
			"\r\n"	
			//(int)((uintptr_t) head_end - (uintptr_t) key_end)
		);

		if (!val_end) {
			HTTPD_PRINT_NOTICE("No value ending found for header\n");
			const int len = (int) (val_end - cursor);
			HTTPD_PRINT_NOTICE("len: %d | %.*s\n", len, len, cursor);
			break;
		}

		Httpd_Header *item = malloc(sizeof(Httpd_Header));
		item->key = cursor;
		item->key_len = key_end - cursor;

		item->val = (char*) key_end + 1;
		item->val_len = val_end -  item->val;
		_trim_header_value(item);

		//HTTPD_PRINT_NOTICE("key_len: %d val_len: %d\n", item->key_len, item->val_len);

		httpd_dlist_push(&headers->list, (void*) item);

		//HTTPD_PRINT_NOTICE("Row:  '%.*s'\n", (int) (val_end - item->key), item->key);
		//HTTPD_PRINT_NOTICE("Key:  '%.*s'\n", (int) (item->key_len), item->key);
		//HTTPD_PRINT_NOTICE("Val:  '%.*s'\n'", (int) (item->val_len), item->val);
		cursor = val_end + 1;
	}
	return 1;
}

extern
Httpd_Header*	
httpd_header_get(
	const char* key,
	uint16_t key_len, 
	Httpd_Headers_Container *headers
)
{
	for (uint32_t i = 0; i < headers->list.n; i++)
	{
		Httpd_Header* item = headers->list.items[i];
		if (item->key_len == key_len && strncasecmp(key, item->key, key_len) == 0) {
			return item;
		}
	}	
	return NULL;
}	

extern 
int
httpd_header_get_str(
	Httpd_Request	*req,
	const char		*key,
	char 			**res,
	int 			*res_len
)
{
	Httpd_Headers_Container headers;
	httpd_dlist_init(&headers.list);
	httpd_headers_parse(req, &headers);
	const Httpd_Header* header = httpd_header_get(key, strlen(key), &headers);
	if (!header) return false;
	*res = header->val;
	*res_len = header->val_len;
	return true;
}

extern 
int
httpd_header_has_str(
	Httpd_Request	*req,
	const char		*key,
	const char 		*q,
	int 			q_len
)
{
	if (q_len == 0) q_len = strlen(q);
	Httpd_Headers_Container headers;
	httpd_dlist_init(&headers.list);
	httpd_headers_parse(req, &headers);
	const Httpd_Header* header = httpd_header_get(key, strlen(key), &headers);
	if (!header) return false;

	return q_len == header->val_len && strcasecmp(q, header->val);
}


