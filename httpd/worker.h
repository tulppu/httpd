#ifndef WORKER_H_ 
#define WORKER_H_ 

#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>

#define WORKS_COUNTER uint16_t 
#define MAX_COUNTED_WORKS UINT16_MAX 

#define PROCESSOR_COUNTER uint8_t
#define MAX_PROCESSORS UINT8_MAX - 1

#define WORK_ID uint16_t

typedef int(*CALLBACK_FUNC)(void**, int);

typedef void(*WORKER_FN_TARGET)(void** params);
typedef void(*WORKER_FN_FINAL_CB)(void* ret_value, void* finished_work);


typedef struct Work 
{
//	void* fn_target;
	WORKER_FN_TARGET fn_target;
	void** params;

	void* next;
	void* prev;

	WORK_ID id;

	//void* fn_cb_final; 
	WORKER_FN_FINAL_CB final_cb;
	bool mallocd:1;
	bool autofree:1;
} Work;

typedef struct Work_Free 
{
	void* next;
	void* prev;
} Work_Free;

typedef struct Work_Processor
{
	Work* head;
	Work* tail;
	_Atomic WORKS_COUNTER num_works;
	pthread_t thread;

	pthread_cond_t cv;
	pthread_mutex_t mutex;

	uint8_t id;
	void* worker;
	_Atomic bool is_running;
} Work_Processor;


typedef struct Worker
{
	Work_Processor **processors;
	uint8_t num_processors;
	_Atomic Work_Free* work_list_free;
	_Atomic bool keep_running;
} Worker;

void	work_free(Work *work);

void	work_processor_append_work(Work_Processor* processor, Work* work);
Work*	work_processor_consume_work(Work_Processor* processor);
void	work_processor_remove_works(Work_Processor *processor, WORK_ID id);
void 	work_processor_free(Work_Processor* processor);
//void	processor_remove_jobs_by_id(Work_Processor* processor, WORK_ID id)

void			worker_init(Worker* worker, uint8_t num_threads);
void			worker_start(Worker* worker);
void 			worker_stop_processors(Worker *queue);
void			worker_free(Worker* worker);

void 			init_processor(Work_Processor *processor);

Work_Processor* worker_add_processor(Worker* worker, Work_Processor *processor);
Work_Processor* worker_get_processor(Worker* worker);

Work* work_processor_create_work
(Work_Processor* processor, WORKER_FN_TARGET fn_target, void** params, WORKER_FN_FINAL_CB final_cb);

//Work_Processor*
//worker_get_processor(Worker* worker);

#endif

