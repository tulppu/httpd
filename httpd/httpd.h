#ifndef HTTPD_H_
#define HTTPD_H_

//#define _GNU_SOURCE

#include <netinet/in.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/epoll.h>
#include <pthread.h>

#include <unistd.h>
#include <sys/syscall.h>

#include "client.h"
#include "worker.h"
#include "callbacks.h"
#include "endpoint.h"
#include "engine.h" 


#include "../misc/deque.h"
#include "../misc/mime.h"
#include "../misc/debug.h"


typedef struct Httpd_Client Httpd_Client;
typedef struct Deque_Store Deque_Store;
typedef struct Deque_Item Deque_Item;
typedef struct Httpd_Endpoint_Mount_Storage Httpd_Endpoint_Mount_Storage;

#define HTTPD_LIKELY(x)		__builtin_expect(!!(x), 1)
#define HTTPD_UNLIKELY(x)	__builtin_expect(!!(x), 0)


#define EPOLL_EVENT_CLIENT	1
#define EPOLL_EVENT_SERVER	2
#define EPOLL_EVENT_WAKER	3

#define EPOLL_MAX_EVENTS	16

#define DEFAULT_TARGET_HOST "0.0.0.0"
#define DEFAULT_TARGET_PORT 4488 


// Execute make rebuild after changing these.
#define HTTPD_DEBUG			1
#define HTTPD_VERBOSE		1

#define LOG_ERROR			1
#define LOG_NOTICE			2
#define LOG_PRINT			3
#define LOG_DEBUG			4
#define LOG_CRITICAL		5


// TODO: move to log.h
extern uint8_t	__HTTPD_VERBOSE_FLAGS;

#define	HTTPD_LOGGING_ENABLE_TYPE(_HTTPD_LOG_TYPE)				((__HTTPD_VERBOSE_FLAGS) |= ((uint) 1 << (_HTTPD_LOG_TYPE)));
#define	HTTPD_LOGGING_DISABLE_TYPE(_HTTPD_LOG_TYPE)				((__HTTPD_VERBOSE_FLAGS) &~ ((uint) 1 << (_HTTPD_LOG_TYPE)));

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat"
#endif

#define BASH_COLOR_RED 		"0;31"
#define BASH_COLOR_LRED 	"1;31"
#define BASH_COLOR_GREEN 	"0;32"
#define BASH_COLOR_LGREEN 	"1;32"
#define BASH_COLOR_YELLOW 	"0;33"
#define BASH_COLOR_CYAN 	"0;36"
//#define BASH_COLOR_ORANGE 	"0;33"


#define _HTTPD_PRINT_PREFIXED(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, __HTTPD_LOG_PREFIX, __BASH_COLOR, ... ) \
	if (__HTTPD_VERBOSE_FLAGS & (1 << __HTTPD_LOG_TYPE) || __HTTPD_LOG_TYPE == LOG_CRITICAL) \
	{\
		char __HTTPD_MSG[2048];\
		int __PAD_LEN = ( 5 - (int) strlen(__HTTPD_LOG_PREFIX) );\
		int __MSG_PREFIX_LEN = snprintf( \
				__HTTPD_MSG,\
				254,\
				"[ %ld ] [ %s ] [ %s:%d ] \e[%sm→\e[0m %*.*s", \
				syscall(SYS_gettid),\
				__HTTPD_LOG_PREFIX,\
				__FILE__,\
				__LINE__,\
				__BASH_COLOR,\
				__PAD_LEN,\
				__PAD_LEN,\
				" "\
		);\
		snprintf(__HTTPD_MSG + __MSG_PREFIX_LEN, 2048 - __MSG_PREFIX_LEN - 1, __VA_ARGS__); \
		fprintf(__HTTPD_LOG_STREAM, __HTTPD_MSG); \
	}
#ifdef __clang__
#pragma diagnostic pop
#endif

#ifdef HTTPD_VERBOSE
	#define _HTTPD_PRINT(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, ...) \
	switch(__HTTPD_LOG_TYPE) {\
		case LOG_PRINT:\
			_HTTPD_PRINT_PREFIXED(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, "", BASH_COLOR_YELLOW, __VA_ARGS__);\
			break;\
		case LOG_DEBUG:\
			_HTTPD_PRINT_PREFIXED(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, "debug",  BASH_COLOR_YELLOW, __VA_ARGS__);\
			break;\
		case LOG_NOTICE:\
			_HTTPD_PRINT_PREFIXED(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, "notice", BASH_COLOR_CYAN, __VA_ARGS__);\
			break;\
		case LOG_ERROR:\
			_HTTPD_PRINT_PREFIXED(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, "error", BASH_COLOR_RED, __VA_ARGS__);\
			break;\
		case LOG_CRITICAL:\
			_HTTPD_PRINT_PREFIXED(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, "critical", BASH_COLOR_RED, __VA_ARGS__);\
			break;\
		default:\
			printf("Log type not handled!\n");\
			exit(-1);\
	}
#else
	#define _HTTPD_PRINT(__HTTPD_LOG_STREAM, __HTTPD_LOG_TYPE, ...) 
#endif


#define 	HTTPD_PRINT(...) 			_HTTPD_PRINT(stdout, LOG_PRINT,		__VA_ARGS__);	
#define 	HTTPD_PRINT_NOTICE(...) 	_HTTPD_PRINT(stdout, LOG_NOTICE,	__VA_ARGS__);	
//#define 	HTTPD_PRINT(...) 			_HTTPD_PRINT(stdout, LOG_NOTICE,	__VA_ARGS__);	
#define 	HTTPD_PRINT_ERROR(...) 		_HTTPD_PRINT(stderr, LOG_ERROR,		__VA_ARGS__);	
#define 	HTTPD_PRINT_CRITICAL(...)	_HTTPD_PRINT(stderr, LOG_CRITICAL,	__VA_ARGS__);	
#ifdef HTTPD_DEBUG
	#define HTTPD_DEBUG_PRINT(...)		_HTTPD_PRINT(stderr, LOG_DEBUG, __VA_ARGS__);
#else
	#define HTTPD_DEBUG_PRINT(...) 
#endif

#define		HTTPD_PRINT_PERROR(__MSG__, ...) \
	char base_msg[512]; \
	snprintf(base_msg, 511, __MSG__, __VA_ARGS__); \
	char perr_msg[256]; \
	memset(perr_msg, 0, 256); \
	strerror_r(errno, perr_msg, 255); \
	HTTPD_PRINT_CRITICAL("%s: '%s'\n", base_msg, perr_msg);

// https://mcuoneclipse.com/2021/01/23/assert-__file__-path-and-other-cool-gnu-gcc-tricks-to-be-aware-of/
#ifdef HTTPD_DEBUG

void print_trace (void);
#define	HTTPD_DEBUG_ASSERT(__e) { \
		if (__e){ (void)0; } else\
		{\
			print_trace(); \
			HTTPD_PRINT_ERROR("\n%s:%d: %s: Assertion '%s' failed.\n", __FILE__, __LINE__, __FUNCTION__,  #__e); \
			puts("\n\n");\
			exit(1); \
		} \
}
#else
	#define HTTPD_DEBUG_ASSERT(__e) ((void)0)
#endif

typedef enum HTTPD_STATUS
{
	STATUS_STARTING		= 0,
	STATUS_RUNNING		= 1,
	STATUS_CLOSING		= 2,
	STATUS_CLOSED		= 3, 
} HTTPD_STATUS;


typedef struct Httpd
{
	int 							fd;
	int								waker_fd;
	int								epoll_fd;

	#ifdef HTTPD_DEBUG
	unsigned long					main_thread_id;
	#endif

	struct sockaddr_in 				addr;
	uint16_t						port;
	in_addr_t						host;

	_Atomic HTTPD_STATUS			status;

	struct epoll_event				ev;
	struct epoll_event				events[EPOLL_MAX_EVENTS];

	Worker							worker;
	Work_Processor					main_thread_processor;

	pthread_mutex_t					mutex;

	struct
	HTTPD_CALLBACK_TABLE*			callback_table;
	Httpd_Endpoint_Mount_Storage*	endpoint_storage;
	Mime2ExtTable					mimes_2_ext_table;

	struct Httpd_Stores
	{
		Deque_Store client_store;
		Deque_Store request_store;
		Deque_Store plugin_store;
		Httpd_Engine_Event_Store event_store;
	} stores;

	struct Httpd_Stats 
	{
		_Atomic time_t started_at;
		_Atomic size_t bytes_read;
		_Atomic size_t bytes_sent;
		_Atomic size_t requests_served;
	} stats;
	
	void*					clients_connected;
	_Atomic size_t			clients_amount;
} Httpd;

void			httpd_init				(Httpd* httpd);
void			httpd_free				(Httpd* httpd);
void			httpd_bind				(Httpd* httpd);
void			httpd_listen			(Httpd* httpd);
void			httpd_close				(Httpd* httpd);
void			httpd_accept_client		(Httpd* httpd);
void			httpd_set_status		(Httpd* httpd, HTTPD_STATUS status);

void 			httpd_lock				(Httpd* httpd);
void 			httpd_unlock			(Httpd* httpd);

Httpd_Client*	httpd_get_client		(Httpd* httpd, int cli_fd);
void			httpd_register_client	(Httpd* httpd, Httpd_Client* client);
void			httpd_unregister_client	(Httpd* httpd, Httpd_Client* client);

#endif
