#ifndef ENDPOINT_H_
#define ENDPOINT_H_ 

#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h> 
#include <stdio.h> 
#include <assert.h>

#include "httpd.h"
#include "request.h"


typedef enum Endpoint_Handler_Type 
{
	ENDPOINT_HANDLER_STATIC		= 0,
	ENDPOINT_HANDLER_DYNAMIC	= 1,
} Endpoint_Handler_Type;

typedef enum HTTPD_FILE_ENCODING
{
	ENCODING_NONE				= 0,
	ENCODING_DEFLATE			= 1
} HTTPD_FILE_ENCODING;

// Handlers, must always start by type and executor.
typedef struct Httpd_Endpoint_Handler 
{
	Endpoint_Handler_Type	type;
	void*					executor;
} Httpd_Endpoint_Handler;

typedef int	(*FN_ENDPOINT_PREPARER)		(void*, Httpd_Request*);
typedef int	(*FN_ENDPOINT_EXECUTOR)		(Httpd_Endpoint_Handler*, Httpd_Request*);

typedef struct Httpd_Endpoint_Handler_Static 
{
	Endpoint_Handler_Type	type;
	FN_ENDPOINT_EXECUTOR	executor;
	char*					file_path;
	char*					etag;
	// Don't free (or manipulate) mime; it's pointer to record in shared table returned by mimes_2_ext_table_get()
	char*					mime;
	char*					str_mdate;
	char*					str_size;
	int						served_fd;
	time_t					mdate;
	size_t					size;
	pthread_mutex_t			mutex;
	HTTPD_FILE_ENCODING		encoding:2;
	bool					is_dir:1;

	// When set true it's assumed the handler has not been malloc()'d and must not to be free()d.
	// Used when serving "lazy" mounts, eg. static files resolved on demand and freed the request has been served.
	bool					tmp:1;

	// If set true deflated file will not be saved in tmp file, instead it will be streamed on the fly.
	// Used for big files and tmp handles.
	bool					stream_deflate:1;
} Httpd_Endpoint_Handler_Static;

typedef struct Httpd_Endpoint_Handler_Dynamic 
{
	Endpoint_Handler_Type	type;
	FN_ENDPOINT_EXECUTOR	executor;
} Httpd_Endpoint_Handler_Dynamic;

// Could be item used to serve /mount_point/index.html
typedef struct  Httpd_Endpoint 
{
	char*					name;
	uint8_t					name_len;
	Httpd_Endpoint_Handler*	handler;
	uint8_t					methods; /* Bit flags, see HTTPD_REQ_METHOD from request.h */
} Httpd_Endpoint;

// Can hold endpoint / or /mount_point/ for example.
typedef struct Httpd_Endpoint_Mount
{
	char*					root_url;
	uint32_t				root_url_len;
	char*					root_dir;
	Httpd_Endpoint**		endpoints;
	uint32_t				num_endpoints;

	Httpd_Endpoint_Handler* index_handler;
	Httpd_Endpoint_Handler* not_found_handler;

	FN_ENDPOINT_PREPARER	endpoint_preparer;

	// If set true the mount point will be serviced/resolved on fly, eg. the files will not be preloaded and they will be freed after the request is finished.
	// Usefull for directories with lot of files or when memory capacibility of the server is low.
	bool 					lazy:1;
} Httpd_Endpoint_Mount;

typedef struct Httpd_Endpoint_Mount_Storage
{
	Httpd_Endpoint_Mount**	mounts;
	uint8_t					num_mounts;
	bool					active;
} Httpd_Endpoint_Mount_Storage;

void 							httpd_endpoint_mount_storage_init(Httpd_Endpoint_Mount_Storage* storage);
Httpd_Endpoint*					httpd_endpoint_create (const char* name, uint8_t methods);
void							httpd_endpoint_free	(Httpd_Endpoint* endpoint);
void							httpd_endpoint_handler_free	(Httpd_Endpoint_Handler* handler);
//int								httpd_endpoint_handler_server(Httpd_Endpoint_Handler* handler, Httpd_Request* request);

Httpd_Endpoint_Handler_Static*	httpd_endpoint_handler_create_static (Httpd_Endpoint_Handler_Static* handler, const char* file_path);
int 							httpd_endpoint_handler_static_load(Httpd_Endpoint_Handler_Static* handler, Httpd* httpd);
//int 							httpd_endpoint_serve_static (Httpd_Endpoint_Handler_Static* handler, Httpd_Request* request);

Httpd_Endpoint_Mount*			httpd_endpoint_mount_create(const char* root_url);
void 							httpd_endpoint_mount_free(Httpd_Endpoint_Mount* mount);
Httpd_Endpoint_Mount*			httpd_endpoint_mount_mount_statics(Httpd_Endpoint_Mount* mount, const char* root_dir);
Httpd_Endpoint_Mount*			httpd_endpoint_mount_mount_lazy_statics(Httpd_Endpoint_Mount* mount, const char* root_dir);
int								httpd_endpoint_mount_add_endpoint(Httpd_Endpoint_Mount* mount, Httpd_Endpoint* endpoint);
Httpd_Endpoint*					httpd_endpoint_mount_find_endpoint(Httpd_Endpoint_Mount* mount, const char* name);


Httpd_Endpoint_Mount_Storage*	httpd_endpoint_mount_storage_create();
void 							httpd_endpoint_mount_storage_add_item(Httpd_Endpoint_Mount_Storage* storage, Httpd_Endpoint_Mount* mount);
void 							httpd_endpoint_mount_storage_free(Httpd_Endpoint_Mount_Storage* storage);
Httpd_Endpoint_Mount*			httpd_endpoint_mount_storage_find_mount_by_url(Httpd_Endpoint_Mount_Storage* storage, const char* url, uint32_t url_len);
void							httpd_endpoint_mount_storage_serve(Httpd_Endpoint_Mount_Storage* storage, Httpd* httpd);

#endif
