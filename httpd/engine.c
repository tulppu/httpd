#include <stdint.h>
#include <sys/eventfd.h>
#include <sys/epoll.h>
#include <sys/param.h>
#include <errno.h>


#include "httpd.h"
#include "engine.h"
#include "worker.h"
#include "header.h"

//uint8_t __HTTPD_VERBOSE_FLAGS = UINT8_MAX;
uint8_t __HTTPD_VERBOSE_FLAGS = 0;

static inline void		_process_client_events(Httpd* httpd, struct epoll_event* events, Httpd_Client *client);
// functions below runs in worker thread.
static inline void		_read_client(void** params);
static inline void		_close_client(Httpd_Client* client);
static inline void		_close_client_proxy(void** params);
static inline void		_finish_client(void** params);

static inline int _cmp_events(const void* a, const void *b)
{
	return (*(Httpd_Engine_Callbackd_Event**)a)->fd - (*(Httpd_Engine_Callbackd_Event**)b)->fd;
}

void httpd_engine_store_init(Httpd_Engine_Event_Store* store)
{
	store->num_items = 0;
	store->items = NULL;
}

void httpd_engine_store_add(Httpd_Engine_Event_Store* store, Httpd_Engine_Callbackd_Event* event)
{
	store->items = (Httpd_Engine_Callbackd_Event**) realloc(store->items, (store->num_items + 1) * sizeof(void*));
	store->items[store->num_items] = event;
	store->num_items++;

	qsort(
		store->items,
		store->num_items,
		sizeof(void*),
		_cmp_events
	);
}

extern void httpd_engine_event_register_on_cb(Httpd* httpd, int fd, struct epoll_event* ev, FN_EVENT_FIRED_CB* cb)
{
	Httpd_Engine_Callbackd_Event* event = (Httpd_Engine_Callbackd_Event*) malloc(sizeof(Httpd_Engine_Callbackd_Event));
	event->fd = fd;
	event->cb = cb;

	httpd_engine_store_add(&httpd->stores.event_store, event);

	httpd_engine_register_event(httpd, fd, ev);
}

extern void httpd_engine_free(Httpd* httpd)
{
	Httpd_Engine_Event_Store* store = &httpd->stores.event_store;
	for (uint32_t i = 0; i < store->num_items; i++)
	{
		free(store->items[i]);
	}
	free(store->items);
	store->items = NULL;
}

Httpd_Engine_Callbackd_Event* httpd_engine_store_get(Httpd_Engine_Event_Store* store, int fd)
{
	Httpd_Engine_Callbackd_Event* key[1] = {
		&(Httpd_Engine_Callbackd_Event) {
			.fd = fd 
		}
	};
	Httpd_Engine_Callbackd_Event** event = (Httpd_Engine_Callbackd_Event**) bsearch(
		key,
		store->items,
		store->num_items,
		sizeof(void*),
		_cmp_events
	);
	return (event) ? *event : NULL;
	//return (event) ? *(Httpd_Endpoint**) event : NULL;
}

// TODO: use tree instead of array after all. at least if the use grows.
extern void httpd_engine_store_remove(Httpd_Engine_Event_Store* store, Httpd_Engine_Callbackd_Event *event)
{
	// assume same item isn't in a list twice.
	Httpd_Engine_Callbackd_Event** items_filtered = (Httpd_Engine_Callbackd_Event**) malloc(sizeof(store->num_items * sizeof(void)));
	uint32_t len_new = 0;
	for (uint32_t i = 0; i < store->num_items; i++)
	{
		if (store->items[i] != event)
		{
			items_filtered[len_new] = store->items[i];
			len_new++;
		}
	}
	store->items = items_filtered;
	store->num_items = len_new;
}

int httpd_engine_register_event(Httpd* httpd, int fd, struct epoll_event *ev)
{
	HTTPD_DEBUG_PRINT("Registering ev %p with fd %d to the polled fds\n", ev, fd);
	int epoll_success = epoll_ctl(httpd->epoll_fd, EPOLL_CTL_ADD, fd, ev);
	if (epoll_success != 0)
	{
		HTTPD_PRINT_PERROR("Failed to epoll fd %d\n", fd);
		exit(0);
	}
	return epoll_success;
}


struct epoll_event httpd_engine_create_event(unsigned long long u48_data, uint8_t u8_flags, uint8_t u8_type)
{
	static uint64_t UINT48_MAX = 2.8147498e+14;
	HTTPD_DEBUG_ASSERT((uintptr_t) u48_data <= UINT48_MAX);
	
	Httpd_Engine_Event_Data event;
	event.items.data = u48_data;
	event.items.type = u8_type;
	event.items.flags = u8_flags;

	struct epoll_event epoll_event;
	epoll_event.data.u64 = event.store;
	epoll_event.events = EPOLLIN;

	return epoll_event;
} 

void* httpd_engine_event_data_cast_to_ptr(Httpd_Engine_Event_Data *ev_data)
{

	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"
	return (void*) ev_data->items.data;
	#pragma GCC diagnostic pop
}


int httpd_engine_unpoll_event(Httpd* httpd, int fd)
{
	return epoll_ctl(httpd->epoll_fd, EPOLL_CTL_DEL, fd, NULL);
}

// write to httpd->wake_fd causes httpd->epoll_fd to fire a tick in main loop.
void httpd_engine_wake_up(Httpd* httpd)
{
	//httpd_lock(httpd);
	eventfd_write(httpd->waker_fd, 1);
	//httpd_unlock(httpd);
}

void httpd_consume_waker(Httpd* httpd)
{
	eventfd_t in;
	eventfd_read(httpd->waker_fd, &in);
}


void httpd_engine_register_waker_fd(Httpd* httpd)
{
	httpd->waker_fd = eventfd(0, EFD_SEMAPHORE| EFD_NONBLOCK);
	httpd->ev.events = EPOLLIN;

	struct epoll_event ev;
	ev.events = EPOLLIN;
	ev.data.fd = httpd->waker_fd;

	//epoll_ctl(httpd->epoll_fd, EPOLL_CTL_ADD, httpd->waker_fd, &ev);
	httpd_engine_register_event(httpd, httpd->waker_fd, &ev);

	HTTPD_PRINT("eventfd %d created\n", httpd->waker_fd);
	//fcntl(httpd->waker_fd, F_SETFL, O_NONBLOCK);
}

void httpd_engine_start(struct Httpd* httpd)
{
	//httpd_engine_poll_events(httpd);
	
	httpd_engine_poll_events(httpd);
	void** params = malloc(1 * sizeof(void*));
	params[0] = httpd;

	for (int i = 0; i < 8; i++)
	work_processor_append_work(&httpd->main_thread_processor, work_processor_create_work(
		worker_get_processor(&httpd->worker),
		(void*) httpd_engine_poll_events,
		(void**) httpd,
		NULL
	));
}

void httpd_engine_poll_events(struct Httpd* httpd)
{
	httpd_set_status(httpd, STATUS_RUNNING);
	while(httpd->status != STATUS_CLOSED)
	{
		//HTTPD_PRINT("Waiting for events\n\n");
		int nfds = epoll_wait(httpd->epoll_fd, httpd->events, EPOLL_MAX_EVENTS, -1);	
		//HTTPD_PRINT("Processing %d events\n", nfds);

		// TODO: check if was waken by waker and unlock the mutex.

		if (httpd->status == STATUS_CLOSED)
		{
			HTTPD_PRINT("Closing httpd\n");
		}

		if (nfds == -1)
		{
			perror("epoll_wait\n");
			//return;
			continue;
		}

		for (int n = 0; n < nfds; n++)
		{
			struct epoll_event* event = &httpd->events[n]; 
			Httpd_Engine_Event_Data ev_data = {
				.store = event->data.u64
			};
			const uint64_t fd = ev_data.items.data;

			//HTTPD_DEBUG_PRINT("Polling event %p (fd %d)\n", &httpd->events[n].data, fd);
			//HTTPD_DEBUG_ASSERT(httpd->events[n].data.fd);

			if (event->events  &= (EPOLLERR | EPOLLHUP | EPOLLRDHUP))
			{
				HTTPD_PRINT_NOTICE("FD %d:/ closed (or err?) ?\n", (int) fd);
				if (errno && event->events & EPOLLERR)
				{
					HTTPD_DEBUG_PRINT("..epoll err..\n");
					HTTPD_PRINT_PERROR("Epoll error %d", errno)
				}
				httpd_engine_unpoll_event(httpd, fd);
				//Httpd_Client *client = httpd_get_client(httpd, fd);
				Httpd_Client* client =  (Httpd_Client*)
					httpd_engine_event_data_cast_to_ptr(&ev_data);

				if (client)
				{
					HTTPD_DEBUG_PRINT("..removing works from %d.\n", client->fd);
					work_processor_remove_works(client->processor, (WORK_ID) client->fd);
					httpd_client_add_work(client, _close_client_proxy, NULL);
					httpd_engine_wake_up(httpd);
				} else close((int)fd);
				continue;
			}


			if (
				ev_data.items.type == EVENT_TYPE_CALLBACK
			)
			{
				//HTTPD_DEBUG_PRINT("Even with last bit set!\n");
				//const int fd_last_bit_unset = fd &~ ((int) 1 << 31);
				//HTTPD_PRINT("Last bit was set for (now unset) fd %d\n", fd_last_bit_unset);
				Httpd_Engine_Callbackd_Event* event_callbacked = 
					httpd_engine_store_get(&httpd->stores.event_store, fd);

				if (event_callbacked)
				{
					HTTPD_DEBUG_ASSERT(event_callbacked->cb);
					event_callbacked->cb(httpd, fd, event);
					continue;
				}
				else
				{
					HTTPD_DEBUG_PRINT("Didn't find callback for event with marked bit.\n")
				}
			}
					
			if
			((int)fd == httpd->fd)
			{
				httpd_accept_client(httpd);
				continue;
			}
			else if 
			((int)fd == httpd->waker_fd)
			{
				httpd_consume_waker(httpd);
				Work* work = NULL;
				while ((work = work_processor_consume_work(&httpd->main_thread_processor)) != NULL)
				{
					void (*cb)(void **) = work->fn_target;
					cb(work->params);
					work_free(work);
				}
				continue;
			}
			else
			{
				//Httpd_Client *client = httpd_get_client(httpd, fd);
				Httpd_Client* client =  (Httpd_Client*) httpd_engine_event_data_cast_to_ptr(&ev_data);
				if (client)
				{
					_process_client_events(httpd, event,  client);
				}
				else
				{
					HTTPD_PRINT_NOTICE("Unknown client %d, closing it.\n.", (int)fd);
					close(fd);
				} 
				continue;
			}
		}
	}
} 


void httpd_engine_add_work(Httpd_Client* client, WORKER_FN_TARGET target_fn, WORKER_FN_FINAL_CB* work_done_cb)
{
	void** params = malloc(1 * sizeof(void*));
	params[0] = client;

	work_processor_append_work(&client->httpd->main_thread_processor, work_processor_create_work(
		client->processor,
		target_fn,
		params,
		NULL
	));
}

// still in main thread.
static inline void _process_client_events(Httpd* httpd, struct epoll_event* event, Httpd_Client* client)
{
	//Httpd_Client *client = httpd_get_client(httpd, client_fd);
	//HTTPD_PRINT("%d wants something\n", client->fd);
	//HTTPD_PRINT("client %d has status %d\n", client->fd, client->status);
	HTTPD_DEBUG_ASSERT(client);

	if (httpd_client_is_status_or_more(client, CLIENT_CLOSE_REQD) && httpd_client_is_status(client, CLIENT_FREE) == false)
	{
		HTTPD_PRINT_NOTICE("recv()'d client %p(%d) while it's already closing.\n", client, client->fd);
		return;
	}
	//httpd_client_halt_events(client);


	//HTTPD_PRINT("fd '%d' is read()able, creating a work.\n", client->fd);
	void** params = malloc(sizeof(void*) * 1);
	params[0] = client;
	//return _read_client(params);
	Work* work = work_processor_create_work(
		client->processor,
		_read_client,
		params,
		NULL
	);
	work->id =  (WORK_ID) client->fd;
	work_processor_append_work(client->processor, work);
}


// now in worker thread.
static void _read_client(void** params)
{
	Httpd_Client *client = (Httpd_Client*) params[0];
	free(params);

	if (httpd_client_is_status_or_more(client, CLIENT_CLOSE_REQD) && httpd_client_is_status(client, CLIENT_FREE) != true)
	{
		HTTPD_PRINT("On jo suljettu..\n");
		return;
	}

	if (client && httpd_client_is_status_or_less(client, CLIENT_READING))
	{
		httpd_client_set_status(client, CLIENT_READING);
	}

	HTTPD_PRINT("Reading %d..\n", client->fd);
	Httpd_Request* request = NULL;
	//const int read_status = REQ_OK;
	const int read_status = httpd_req_read(client, &request);
	HTTPD_PRINT("..read %d\n", client->fd);

	if (read_status == REQ_WOULDBLOCK)
	{	
		HTTPD_PRINT("Continuing read of fd %d\n", client->fd);
		//httpd_client_unhalt_events(client);
		return;
	}

	if (request == NULL)
	{
		return _close_client(client);
	}

	if (httpd_client_is_status(client, CLIENT_CLOSE_REQD))
	{
		return _close_client(client);
	}
	
	if (httpd_client_is_status_or_more(client, CLIENT_CLOSE_REQD))
	{
		HTTPD_PRINT("On jo suljettu..\n");
		return;
	}

	const int path_not_found = request
		&& request->headers_ready == true
		&& request->found_provider == false;

	if (path_not_found)
	{
		HTTPD_PRINT("Ei palvelijaa polulle '%s' (req %p)\n", request->path, request)
		httpd_req_write(request, "HTTP/1.1 404 Not Found\r\n", 0);
		httpd_req_write_header(request, "Content-Length", "0");
		//if (request->head_out_buffer.len)
			httpd_req_flush(request);

		httpd_req_end_headers(request);
		request->finished = true;
	}

	if (request->finished)
	{
		HTTPD_DEBUG_ASSERT(request->head_out_buffer.len == 0);
		const bool close_conn = httpd_header_has_str(request, "connection", "close", 5);

		httpd_req_free(client->request);
		client->request = NULL;
		if (close_conn)
		{
			HTTPD_PRINT_NOTICE("keep-alive not requested, closing the connection.\n");
			httpd_engine_unpoll_event(client->httpd, client->fd);
			work_processor_remove_works(client->processor, (WORK_ID) (client->fd));
			_close_client(client);
		} else httpd_client_set_status(client, CLIENT_READING);
		return;
	}


	HTTPD_PRINT("\n");
}

static inline void _close_client_proxy(void** params)
{
	_close_client(params[0]);
	free(params);
}
// Move task to main thread to avoiding having multiple clients with same fd registerd.
static inline void _close_client(Httpd_Client* client)
{
	if (httpd_client_is_status_or_more(client, CLIENT_CLOSING))
	{
		HTTPD_DEBUG_PRINT("Client %p already closing or closed!\n", client);
		return;
	}

	httpd_client_request_close(client);
	httpd_client_set_status(client, CLIENT_CLOSING);
	httpd_client_halt_events(client);

	void **_params = malloc(sizeof(void*) * 1);
	_params[0] = client;
	//return _finish_thread(_params, 1);

	Work* close_work = work_processor_create_work(
		&(client->httpd->main_thread_processor),
		_finish_client,
		_params,
		NULL
	);
	Httpd* httpd = client->httpd;
	work_processor_append_work(&client->httpd->main_thread_processor, close_work);
	httpd_engine_wake_up(httpd);
}


// back in main thread.
static inline void _finish_client(void** params)
{
	Httpd_Client* client = (Httpd_Client*) params[0];
	httpd_client_terminate(client);
	free(params);
}
