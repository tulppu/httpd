#include <unistd.h> 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/eventfd.h>
#include <sys/epoll.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>


#include <poll.h>
#include <fcntl.h>
#include <math.h>

#include "httpd.h"
#include "client.h"
#include "request.h"
#include "worker.h"
#include "engine.h"
#include "plugin.h"

#include "../misc/deque.h"

void httpd_init(Httpd* httpd)
{
	assert((ceil(log2(HTTPD_MIN_HEADERS_SIZE)) == floor(log2(HTTPD_MIN_HEADERS_SIZE))));

	pthread_mutexattr_t mutex_attr;//= (sizeof(pthread_mutexattr_t));
	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);

	pthread_mutex_init(&httpd->mutex, &mutex_attr);


	httpd->port = DEFAULT_TARGET_PORT;
	httpd->host = inet_addr(DEFAULT_TARGET_HOST);

	httpd->fd = 0;
	bzero((char *) &httpd->addr, sizeof(httpd->addr));

	httpd->epoll_fd = 0;
	httpd->waker_fd = 0; 
	httpd_set_status(httpd, STATUS_STARTING);
	httpd->stats.started_at = 0;
	httpd->stats.bytes_sent = 0;
	httpd->stats.bytes_read = 0;
	httpd->stats.requests_served = 0;
	httpd->stats.started_at = time(NULL);

	httpd->clients_connected = NULL;
	httpd->clients_amount = 0;

	httpd->epoll_fd = epoll_create(1);
	if (httpd->epoll_fd == -1)
	{
		HTTPD_PRINT_PERROR("%s", "epoll_create(1) failed!\n")
		exit(1);
	}
	#ifdef HTTPD_DEBUG
	httpd->main_thread_id= syscall(SYS_gettid);
	#endif
	
	deque_init(&httpd->stores.client_store, "Clients", (void*) httpd_client_free_real);
	deque_init(&httpd->stores.request_store, "Requests", (void*) httpd_req_free_real);
	deque_init(&httpd->stores.plugin_store, "Plugins", (void*) httpd_plugin_free);

	httpd_engine_store_init(&httpd->stores.event_store);

	mimes_2_ext_table_build(&httpd->mimes_2_ext_table, "/etc/mime.types", 1600);

	worker_init(&httpd->worker, sysconf(_SC_NPROCESSORS_ONLN));
	//worker_init(&httpd->worker, 2);

	init_processor(&httpd->main_thread_processor);
	pthread_mutex_destroy(&httpd->main_thread_processor.mutex);

	pthread_mutex_init(&httpd->main_thread_processor.mutex, &mutex_attr);
	httpd->main_thread_processor.is_running = true;
	httpd->main_thread_processor.worker = &httpd->worker;

	//HTTPD_CALLBACK_TABLE table;
	//HTTPD_CALLBACK_TABLE *table = (HTTPD_CALLBACK_TABLE*) malloc(sizeof(HTTPD_CALLBACK_TABLE));
	httpd->callback_table =  (HTTPD_CALLBACK_TABLE*) malloc(sizeof(HTTPD_CALLBACK_TABLE));
	httpd_callback_table_init(httpd->callback_table);

	httpd->endpoint_storage = (Httpd_Endpoint_Mount_Storage*) malloc(sizeof(Httpd_Endpoint_Mount_Storage));
	httpd_endpoint_mount_storage_init(httpd->endpoint_storage);
	
}

void httpd_free(Httpd* httpd)
{
	httpd_set_status(httpd, STATUS_CLOSING);
	Worker* worker = &httpd->worker;
	worker_stop_processors(worker);
	for (int i = 0; i < worker->num_processors; i++)
	{
		work_processor_free(worker->processors[i]);
	}

	httpd_close(httpd);


	httpd_callback_table_free(httpd->callback_table);
	httpd_endpoint_mount_storage_free(httpd->endpoint_storage);
	free(httpd->endpoint_storage);

	mimes_2_ext_table_free(&httpd->mimes_2_ext_table);

	httpd_engine_free(httpd);

	deque_store_free(&httpd->stores.client_store);
	deque_store_free(&httpd->stores.request_store);
	deque_store_free(&httpd->stores.plugin_store);


	pthread_mutex_destroy(&httpd->mutex);

	free(httpd->worker.processors);
	free(httpd);
}

void httpd_lock(Httpd* httpd)
{
	//HTTPD_PRINT("Lukitaan..\n");
	pthread_mutex_lock(&httpd->mutex);
	//HTTPD_PRINT("...lukittu.\n");
}

void httpd_unlock(Httpd* httpd)
{
	//HTTPD_PRINT("Vapautetaan lukko..\n");
	pthread_mutex_unlock(&httpd->mutex);
	//HTTPD_PRINT("..vapautettu.\n");
}

void httpd_bind(Httpd* httpd)
{
	httpd->fd = socket(AF_INET, SOCK_STREAM, 0);
	if (httpd->fd < 0)
	{ 
		perror("ERROR opening socket");
		exit(1);
	}
	int optval = 1;
 	setsockopt(httpd->fd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval , sizeof(int));

	int qlen = 5;
	if (setsockopt(httpd->fd, IPPROTO_TCP, TCP_FASTOPEN, &qlen, sizeof(qlen)) == -1)
	{
		HTTPD_PRINT_PERROR("Failed to set sockops for listening sock %d\n", httpd->fd);
	}

	httpd->addr.sin_family = AF_INET;
	httpd->addr.sin_addr.s_addr = httpd->host;
	httpd->addr.sin_port = htons(httpd->port);

	if (bind(httpd->fd, (struct sockaddr *) &httpd->addr, sizeof(httpd->addr)) < 0)
	{
		HTTPD_DEBUG_PRINT("Failed bind to %s%d\n",  inet_ntoa(httpd->addr.sin_addr), ntohs(httpd->addr.sin_port))
		exit(1);
	}
	
	HTTPD_PRINT_NOTICE("Kuunnellaan osoitteessa http://%s:%u fd %d\n",  inet_ntoa(httpd->addr.sin_addr), ntohs(httpd->addr.sin_port), httpd->fd);  
}

void httpd_listen(Httpd* httpd)
{
	static int recvbuf_size = 1024 * 1024 * 8;
	setsockopt(httpd->fd, SOL_SOCKET, SO_RCVBUF, &recvbuf_size, sizeof(httpd->addr)); 

	static int sendbuf_size = 1024 * 1024 * 16;
	setsockopt(httpd->fd, SOL_SOCKET, SO_SNDBUF, &sendbuf_size, sizeof(httpd->addr)); 
	if (listen(httpd->fd, 512) == -1)
	{
		perror("listen()");
		exit(1);
	}
	fcntl(httpd->fd, F_SETFL, O_NONBLOCK);
	httpd->ev.events = EPOLLIN | EPOLLHUP | EPOLLRDHUP;
	//httpd->ev.data.fd = httpd->fd;
	httpd->ev.data.u64 = httpd->fd;

	//epoll_ctl(httpd->epoll_fd, EPOLL_CTL_ADD, httpd->fd, &httpd->ev);
	httpd_engine_register_event(httpd, httpd->fd, &httpd->ev);

}

#include "../misc/debug.h"
void httpd_accept_client(Httpd* httpd)
{
	struct sockaddr_in cli_addr;
	socklen_t cli_len = sizeof(cli_addr);

	//httpd_lock(httpd);
	const int cli_fd = accept(httpd->fd, (struct sockaddr *) &cli_addr, &cli_len);
	//httpd_unlock(httpd);

	//httpd_unlock(httpd);
	if (cli_fd == -1)
	{
		//perror("accept()");
		int n_open_fds = open_fd_count();
		HTTPD_PRINT_PERROR("accept() %d", cli_fd);
		HTTPD_PRINT_NOTICE("Open fds: %d\n", n_open_fds);
		//exit(0);
		return;
	}

	//int optval = 1;
 	//setsockopt(cli_fd, SOL_SOCKET, SO_NOSIGPIPE, (const void *)&optval , sizeof(int));
	/*
	if (httpd->clients_amount >= 1024 * 8)
	{
		HTTPD_PRINT_NOTICE("%ld concurrent connections already open, refusing to server %d\n", httpd->clients_amount, cli_fd);
		close(cli_fd);
		return;
	}*/

	Httpd_Client* client = httpd_client_create(httpd);
	httpd_client_init(httpd, client);
	client->fd = cli_fd;
	httpd_register_client(httpd, client);

	HTTPD_DEBUG_PRINT("accept()'d new client %p with fd %d\n", client, cli_fd);
	const int flags = fcntl(client->fd, F_GETFL, 0);	
	if (fcntl(client->fd, F_SETFL, (int) flags | O_NONBLOCK) == -1)
	{
		perror("fcntl()");
		//exit(0);
		httpd_unregister_client(httpd, client);
		close(cli_fd);
		return;
	}
	/*
	client->ev = (struct epoll_event) {
		.events = EPOLLIN | EPOLLET | EPOLLHUP | EPOLLRDHUP,
		.data.fd = client->fd,
	};*/
	client->ev = httpd_engine_create_event(
		(unsigned long long) client,
		0,
		0
	);
	client->ev.events = EPOLLIN | EPOLLET | EPOLLHUP | EPOLLRDHUP;

	httpd_lock(httpd);
	int epoll_success = httpd_engine_register_event(httpd, client->fd, &client->ev);
	//int epoll_success = epoll_ctl(httpd->epoll_fd, EPOLL_CTL_ADD, client->fd, ev);
	httpd_unlock(httpd);

	if (epoll_success != 0)
	{
		//perror("epoll_ctl:");
		HTTPD_PRINT_PERROR("epoll_ctl returned %d", epoll_success);
		exit(0);
	}
	HTTPD_DEBUG_PRINT("Client ready\n");
}

void httpd_close(Httpd* httpd)
{
	httpd_set_status(httpd, STATUS_CLOSING);
	shutdown(httpd->fd, SHUT_RD);
	close(httpd->epoll_fd);
	close(httpd->fd);
	httpd_set_status(httpd, STATUS_CLOSED);
}


void httpd_set_status(Httpd* httpd, HTTPD_STATUS status)
{
	httpd->status = status;
}


#define _GNU_SOURCE
#include <search.h>
#include "client.h"

// https://www.gnu.org/software/libc/manual/html_node/Tree-Search-Function.html
// IIRC CLib's implementation is using red-black tree
// If bored: check if hash-table would work better if maxium number of concurrent clients is limited to low number.
 static inline int
 clients_compare(const void *pa, const void *pb)
 {
	const Httpd_Client *a = (Httpd_Client*) pa;
	const Httpd_Client *b = (Httpd_Client*) pb;

	if (a->fd < b->fd)
		return -1;
	if (a->fd > b->fd)
		return 1;
	return 0;
 }
 


void httpd_register_client(Httpd* httpd, Httpd_Client* client)
{
	httpd_lock(httpd);


	client->httpd = httpd;
	client->processor = worker_get_processor(&httpd->worker); 
	//client->processor = httpd->worker.processors[0];
	/*
	// https://www.gnu.org/software/libc/manual/html_node/Tree-Search-Function.html
	// "The return value is a pointer to the matching element in the tree.
	// If a new element was created the pointer points to the new data (which is in fact key).
	// If an entry had to be created and the program ran out of space NULL is returned."
	HTTPD_PRINT("Lisätään fd %d\n", client->fd);
	//HTTPD_DEBUG_ASSERT(httpd_get_client(httpd, client->fd) == NULL);

	void** inserted_client = tsearch(client, &httpd->clients_connected, clients_compare);
	httpd_unlock(httpd);
	//tsearch((void*) client, &httpd->clients_connected, compare);
	assert(inserted_client);
	assert(*inserted_client == client);
	*/
	httpd->clients_amount++;

	httpd_callback_execute_client_registered(client);
}

void httpd_unregister_client(Httpd* httpd, Httpd_Client* client)
{
	/*
	httpd_lock(httpd);
	httpd_callback_execute_client_unregistering(client);
	HTTPD_DEBUG_PRINT("unregistering client %p: %d\n", client, client->fd);
	Httpd_Client** item = (Httpd_Client**) tdelete(client, &httpd->clients_connected, clients_compare);
	if (!item) {
		HTTPD_DEBUG_PRINT("ÖH..%d\n", client->fd);
	}
	//assert(item);
	httpd_unlock(httpd);
	*/
	httpd->clients_amount--;
}

Httpd_Client* httpd_get_client(Httpd* httpd, int cli_fd)
{
	httpd_lock(httpd);
	if (httpd->clients_connected == NULL) {
		httpd_unlock(httpd);
		return NULL;
	}

	Httpd_Client needle = { .fd = cli_fd };
	//HTTPD_PRINT("Etsitään fd %d\n", needle.fd);
	Httpd_Client **client = (Httpd_Client**) tfind(&needle, &httpd->clients_connected, clients_compare);
	httpd_unlock(httpd);

	if (client) {
		//HTTPD_PRINT("Löyty %d @ %p\n", (*client)->fd, *client);
		assert((*client)->fd == cli_fd);
		return *client;
	}

	return NULL;
}
