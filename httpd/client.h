#ifndef CLIENT_H_
#define CLIENT_H_

#include <netinet/in.h>
#include <pthread.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>

#include "httpd.h"
#include "request.h"
#include "worker.h"

typedef struct Httpd Httpd;
typedef struct Httpd_Client Httpd_Client;
typedef struct Httpd_Request Httpd_Request;
typedef struct Work_Processor Work_Processor;

typedef enum
{
	CLIENT_INITALIZED		= 0,
	CLIENT_READING			= 1,
	CLIENT_CLOSE_REQD		= 2,
	CLIENT_CLOSING			= 3,
	CLIENT_CLOSED			= 4,
	CLIENT_FREE				= 5,
} CLIENT_STATUS;

typedef struct Httpd_Client
{
	int						fd;

	Httpd*					httpd;
	Httpd_Request*			request; 
	pthread_mutex_t			mutex;

	// TODO: Change to uint8_t processor_id
	Work_Processor*			processor;

	struct sockaddr_in		addr;
	// time() - httpd->stats->started_at 
	uint32_t				last_activity; 
	struct epoll_event		ev;

	_Atomic CLIENT_STATUS	status;

	void*					next;
	void*					prev;
} Httpd_Client;

Httpd_Client*	httpd_client_create(Httpd* httpd);
void			httpd_client_init(Httpd* httpd, Httpd_Client* client);
void			httpd_client_free(Httpd_Client* client);
extern void 	httpd_client_refresh(Httpd_Client* client);
void			httpd_client_free_real(Httpd_Client* client);
void			httpd_client_lock(Httpd_Client *client);
void			httpd_client_unlock(Httpd_Client *client);
void			httpd_client_add_req(Httpd_Client* client, Httpd_Request* req);
void 			httpd_client_remove_req(Httpd_Client* client, Httpd_Request* req);
void			httpd_client_close(Httpd_Client* client);
void			httpd_client_terminate(Httpd_Client* client);
void			httpd_client_start_offing(Httpd_Client* client);
ssize_t			httpd_client_read(Httpd_Client *client, char* buf, size_t nbytes);
ssize_t			httpd_client_write(
		Httpd_Client *client,
			const char* buf,
			size_t buf_len
	);
//ssize_t 		httpd_client_writef(Httpd_Client *client, const char* buf, ...);
ssize_t httpd_client_writef(
		Httpd_Client *client,
		const char* fmt,
		...);


extern void				httpd_client_set_status(Httpd_Client* client, CLIENT_STATUS status);
extern CLIENT_STATUS  	httpd_client_get_status(Httpd_Client* client);
extern bool 			httpd_client_is_status(Httpd_Client* client, CLIENT_STATUS status);
extern bool 			httpd_client_is_status_or_more(Httpd_Client* client, CLIENT_STATUS status);
extern bool 			httpd_client_is_status_or_less(Httpd_Client* client, CLIENT_STATUS status);
extern int 				httpd_client_is_closed(Httpd_Client* client);
extern void
httpd_client_request_close(Httpd_Client* client);

void 		httpd_client_halt_events(Httpd_Client* client);
void		httpd_client_unhalt_events(Httpd_Client* client);

/* Adds work to the client's worker loop. */
void httpd_client_add_work(Httpd_Client* client, WORKER_FN_TARGET target_fn, void* work_done_cb);



#endif
