#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include "worker.h"
#include "httpd.h"

inline static void* _processor_loop(void* _processor_);


void worker_init(Worker* worker, uint8_t num_processors)
{
	worker->work_list_free = NULL;
	worker->keep_running = true;
	worker->num_processors = 0;
	worker->processors = (Work_Processor**) malloc(num_processors * sizeof(Work_Processor));
	for (uint8_t i = 0; i < num_processors; i++)
	{
		Work_Processor *p = worker_add_processor(worker, NULL);
		p->id = i;
	}
}

void init_processor(Work_Processor *processor)
{
	pthread_mutexattr_t mutex_attr;
	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&processor->mutex, &mutex_attr);

	pthread_cond_init(&processor->cv, NULL);

	processor->worker = NULL;
	processor->head = NULL;
	processor->tail = NULL;
	processor->num_works = 0;
	processor->thread = 0;
	processor->is_running = false;
}

Work_Processor* worker_add_processor(Worker* worker, Work_Processor *processor)
{

	if (processor == NULL)
	{
		processor = (Work_Processor*) malloc(sizeof(Work_Processor));
	}

	init_processor(processor);
	processor->worker = (void*) worker;


	const int i = (int) worker->num_processors;

	assert(i <= MAX_PROCESSORS);

	worker->processors[i] = processor;
	worker->num_processors++;

	return processor;
}

Work_Processor* worker_get_processor(Worker* worker)
{
	static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

	pthread_mutex_lock(&lock);
	assert(worker->num_processors);
	//return worker->processors[0];
	
	static PROCESSOR_COUNTER counter = 0;
	PROCESSOR_COUNTER _counter = counter;
	assert( _counter < worker->num_processors);

	//HTTPD_PRINT("Assigning processor %hhu\n", counter);
	Work_Processor *res =  worker->processors[_counter];

	counter++;
	if (counter + 1 == worker->num_processors) counter = 0;

	pthread_mutex_unlock(&lock);
	return res;
}

void worker_start(Worker* worker)
{
	HTTPD_PRINT_NOTICE("Launching workers\n");
	worker->keep_running = true;
	for (uint8_t i = 0; i < worker->num_processors; i++)
	{
		Work_Processor* processor = worker->processors[i];
		HTTPD_PRINT_NOTICE("Luodaan säie %d\n", i);
		pthread_create(&processor->thread, NULL, _processor_loop, (void*) processor);
	}
	for (uint8_t i = 0; i < worker->num_processors; i++)
	{
		pthread_detach((worker->processors[i])->thread);
	}
	//sleep(1);
}



void init_work(Work* work)
{
	work->mallocd = false;
	work->id = 0;
	work->fn_target = NULL;
	work->params = NULL;
	work->final_cb = NULL;
	work->prev = work->next = NULL;
	work->autofree = 1;
}

Work* work_processor_create_work
(Work_Processor* processor, WORKER_FN_TARGET fn_target, void** params, WORKER_FN_FINAL_CB final_cb)
{
	Work* work = NULL;

	// TODO: pool works.
	work = (Work*) malloc(sizeof(Work));

	init_work(work);
	work->mallocd = true;
	work->fn_target = fn_target;
	work->params = params;
	work->final_cb = final_cb;

	return work;
}

void work_free(Work* work)
{
	if (work->mallocd)
	{
		free(work);
	}
}

void work_processor_free(Work_Processor* processor)
{
	pthread_mutex_destroy(&processor->mutex);
	pthread_cond_destroy(&processor->cv);

	free(processor);
}

void _pair(Work* work1, Work* work2)
{
	if (work1) work1->prev = work2;
	if (work2) work2->next = work1;
}

// Workd is always appended to the tail.
void work_processor_append_work(Work_Processor *processor, Work* work)
{
	//HTTPD_PRINT("Appending job %p with id %u for processor %p\n", work, work->id, processor);
	Worker* worker = (Worker*) processor->worker;
	//HTTPD_PRINT("Aquiring mutex\n");
	pthread_mutex_lock(&processor->mutex);

	if (processor->is_running == false || worker->keep_running == false) {
		HTTPD_DEBUG_PRINT("Process not running, won't add the job!\n");
		return;
	}

	work->prev = NULL;
	_pair(processor->tail, work);

	if (processor->head == NULL) processor->head = work;
	else if (processor->tail == NULL)
	{
		processor->tail = work;
		_pair(processor->head, processor->tail);
	}
	else
	{
		_pair(processor->tail, work);
		processor->tail = work;
	}


	processor->num_works++;

	pthread_cond_signal(&processor->cv);
	//pthread_cond_broadcast(&worker->cv);
	//HTTPD_PRINT("Cond sent\n");

	pthread_mutex_unlock(&processor->mutex);
	//HTTPD_PRINT("Freed mutex\n");
}

// Should be called from worker threads with processor->mutex locked.
Work* work_processor_consume_work(Work_Processor *processor)
{
	//Worker* worker = (Worker*) processor->worker;

	//HTTPD_PRINT("Consuming work from processor %p!\n", processor);
	Work* work = NULL;
	//HTTPD_PRINT("..locking...\n");
	pthread_mutex_lock(&processor->mutex);
	work = processor->head;
	if (work)
	{
		processor->head = work->prev;
		if (processor->head)
		{
			processor->head->next = NULL;
			pthread_cond_signal(&processor->cv);
		}
		if (work == processor->tail)
		{
			processor->tail = NULL;
		}
	}
	//HTTPD_PRINT("..unlocking\n");

	//	HTTPD_PRINT("Found a job! :---)\n");
	//#ifdef HTTPD_DEBUG_
	if (work) 
		processor->num_works--;
	//#endif

	pthread_mutex_unlock(&processor->mutex);

	return work;
}


void work_processor_remove_works(Work_Processor *processor, WORK_ID id)
{
	pthread_mutex_lock(&processor->mutex);

	//HTTPD_PRINT("Removing jobs with id %u\n..", id);

	Work* work = processor->head;
	while (work && ((Worker*) processor->worker)->keep_running)
	{
		if (work->id && work->id == id)
		{
			HTTPD_PRINT_NOTICE("Found work %p with id %u to remove\n..", work, id);
			if (work->prev)
			{
				((Work*)work->prev)->next = work->next;
				if (work->prev == processor->head)
				{
					processor->head = work->next;
				}
			}
			if (work == processor->tail)
			{
				if (work->next)	
				{
					((Work*)work->next)->prev = NULL;
				}
				processor->tail = work->next;
			}
			processor->num_works--;
		}
		work = work->next;
	}
	pthread_mutex_unlock(&processor->mutex);
}

void worker_stop_processors(Worker *worker)
{
	worker->keep_running = false;
	for (uint32_t i = 0; i < worker->num_processors; i++)
	{
		Work_Processor* p = worker->processors[i];
		Work* work = p->head;
		p->head = NULL;
		p->is_running = false;

		while (work)
		{
			Work* next = work->next;
			work_free(work);
			HTTPD_DEBUG_PRINT("Työ vapautettu.");
			work = next;
		}

		pthread_cond_signal(&p->cv);
		while (p->is_running)
		{
			pthread_cond_wait(&p->cv, &p->mutex);
		}
	}
}
inline static void* _processor_loop(void* _processor_)
{
	Work_Processor* processor = (Work_Processor*) _processor_;
	Worker* worker = (Worker*) processor->worker;

	processor->is_running = true;

	while (worker->keep_running)
	{
		pthread_mutex_lock(&processor->mutex);
		while (processor->head == NULL)
		{
			//HTTPD_PRINT("Waiting for cond'd job %d\n", worker->keep_running);
			if (worker->keep_running == false)
			{
				break;
			}
			pthread_cond_wait(&processor->cv, &processor->mutex);
		}
		if (worker->keep_running == false) break;
		Work* work = work_processor_consume_work(processor);
		if (work == NULL)
		{
			//HTTPD_PRINT("Found no job?? :----/\n");
			pthread_mutex_unlock(&processor->mutex);
			continue;
		}

		pthread_mutex_unlock(&processor->mutex);
		WORKER_FN_TARGET cb = work->fn_target;

		cb(work->params);
		if (work->final_cb)
		{
			WORKER_FN_FINAL_CB final_cb = work->final_cb;
			(*(final_cb))(NULL, work);
		}
		if(work->autofree) work_free(work);
	}
	processor->is_running = false;
	pthread_cond_broadcast(&processor->cv); 
	
	return NULL;
}

