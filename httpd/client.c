#define _GNU_SOURCE 
#define _XOPEN_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <sys/poll.h>
#include <stdarg.h>

#include "client.h"
#include "worker.h"
#include "httpd.h"
#include "request.h"

#include "../misc/debug.h"
#include "../misc/deque.h"

Httpd_Client* httpd_client_create(Httpd* httpd)
{
	Httpd_Client* client = (Httpd_Client*) deque_pop_item(&httpd->stores.client_store);
	//Httpd_Client* client = NULL;
	if (client == NULL) {
		client = (Httpd_Client*) malloc(sizeof (Httpd_Client));
		HTTPD_PRINT("..mutex alustetaan?\n");
		pthread_mutex_init(&client->mutex, NULL);
	}

	HTTPD_DEBUG_ASSERT(client);

	//HTTPD_DEBUG_PRINT("asiakas %p aivan valmis!\n", client);
	return client;
}

// Doesn't init the mutex!
void httpd_client_init(Httpd* httpd, Httpd_Client* client)
{
	//pthread_mutex_init(&client->mutex, NULL);
	client->status			= CLIENT_INITALIZED;
	client->request			= NULL;
	client->httpd			= NULL;
	client->fd				= -1;
	client->processor		= NULL;
	client->httpd			= httpd;

	client->last_activity =  (uint32_t) (time(NULL) - client->httpd->stats.started_at); 
}

	
#include <dlfcn.h>


void httpd_client_refresh(Httpd_Client* client)
{
	client->last_activity =  (uint32_t) (time(NULL) - client->httpd->stats.started_at); 
	//HTTPD_PRINT("\nLast activity set.\n\n");

	typedef int	(FN)(void*);
	FN* __fn = dlsym(RTLD_NEXT, __FUNCTION__); 
	if (__fn) __fn(client);
}

void httpd_client_free(Httpd_Client* client)
{
	bool add_success = deque_add_item(
		&client->httpd->stores.client_store,
		client
	);
	if (add_success == false) {
		free(client);
	}
}
void httpd_client_free_real(Httpd_Client* client)
{
	pthread_mutex_destroy(&client->mutex);
	free(client);
}

void httpd_client_lock(Httpd_Client *client)
{
	return;
	pthread_mutex_lock(&client->mutex);
}

inline void httpd_client_unlock(Httpd_Client *client)
{
	return;
	pthread_mutex_unlock(&client->mutex);
}

void httpd_client_add_req(Httpd_Client* client, Httpd_Request* req)
{
	httpd_client_lock(client);
	req->client = client;
	httpd_client_unlock(client);
}

void httpd_client_halt_events(Httpd_Client* client)
{
	return;
	httpd_client_lock(client);
	struct epoll_event ev;
	ev.data.fd = client->fd;
	ev.events = 0;
	Httpd* httpd = client->httpd;
	//httpd_lock(httpd);
	int epoll_success = epoll_ctl(httpd->epoll_fd, EPOLL_CTL_DEL, client->fd, &ev);
	//httpd_unlock(httpd);

	if (epoll_success != 0)
	{
		perror("epoll_ctl_del:");
		HTTPD_DEBUG_ASSERT("epoll_tcl");
	}
	httpd_client_unlock(client);
}

void httpd_client_unhalt_events(Httpd_Client* client)
{
	return;
	httpd_client_lock(client);
	struct epoll_event ev;
	ev.data.fd = client->fd;
	ev.events = EPOLLIN | EPOLLET | EPOLLHUP | EPOLLRDHUP;

	Httpd* httpd = client->httpd;
	//httpd_lock(httpd);
	int epoll_success = epoll_ctl(httpd->epoll_fd, EPOLL_CTL_ADD, client->fd, &ev);
	//httpd_unlock(httpd);

	if (epoll_success != 0)
	{
		perror("epoll_ctl_add:");
		HTTPD_DEBUG_ASSERT("epoll_tcl");
		exit(0);
	}
	httpd_client_unlock(client);
}

void httpd_client_remove_req(Httpd_Client* client, Httpd_Request* req)
{
	httpd_client_lock(client);
	//client->req_queue = NULL;
	//httpd_req_free(req);
	httpd_client_unlock(client);
}

ssize_t httpd_client_read(Httpd_Client *client, char* buf, size_t nbytes)
{
	//httpd_client_lock(client);

	if (httpd_client_is_status(client, CLIENT_READING) == false) {
		//httpd_client_unlock(client);
		return -1;
	}

	ssize_t res = read(client->fd, buf, nbytes);
	httpd_client_refresh(client);

	if (res == 0)
	{
		HTTPD_PRINT_ERROR("Client %d closed the connection, can't read.?\n", client->fd);
	}

	if (res > 0) {
		//client->httpd->stats.bytes_read += res;
	}
	//httpd_client_unlock(client);
	return res;
}

ssize_t httpd_client_writef(
		Httpd_Client *client,
		const char* fmt,
		...)
{
	va_list va;
	va_start(va, fmt);
	const int size_full = vsnprintf(NULL, 0, fmt, va);
	va_end(va);
	char buf_full[size_full + 1];
	va_start(va, fmt);
	vsnprintf(buf_full, size_full, fmt, va);
	va_end(va);
	//HTTPD_DEBUG_PRINT("..writing soon?\n");
	//HTTPD_DEBUG_PRINT("%.*s\n", size_full, buf_full);
	//HTTPD_DEBUG_PRINT("Total size: %d\n", size_full);
	return httpd_client_write(client, buf_full, size_full);
}


inline static int 
_send_wrapper (Httpd_Client *client, const char* buf, size_t buf_len)
{
	const int cli_fd = client->fd;

	if (buf_len == 0)
	{
		HTTPD_PRINT_NOTICE("Attempted to send empty buffer\n");
		return 0;
	}

	for (uint8_t i = 0; i < 8; i++)
	{
		ssize_t res = send(client->fd, (void*) buf, buf_len, MSG_NOSIGNAL);

		if (res > 0) {
			return res;
		}

		if (res == -1 && errno) {
			HTTPD_PRINT_PERROR("send() to %d failed\n", cli_fd);
		}

		if (res == -1 && errno == EAGAIN)
		{
			HTTPD_PRINT_NOTICE("Out buffer full?\n");
			struct pollfd pfds[2];

			pfds[0].events = POLLOUT;
			pfds[0].fd = client->fd;

			pfds[1].events = POLLRDHUP | POLLHUP;
			pfds[1].fd = client->fd;

			const int poll_status = poll(
				pfds, 
				sizeof(pfds) / sizeof(struct pollfd),
				5 * 1000
			);

			HTTPD_DEBUG_PRINT("Polled a write.\n");

			if (poll_status == -1)
			{
				HTTPD_PRINT_PERROR("poll() %d\n", cli_fd);
				return -1;
			}
			if (pfds[1].revents & POLLERR)
			{
				//httpd_client_set_status(client, CLIENT
				httpd_client_request_close(client);
				HTTPD_DEBUG_PRINT("POLLERR while waiting send() to become ready.\n");
				HTTPD_PRINT_PERROR("fd %d\n", cli_fd);
				return -1;
			}

			if (pfds[1].revents &= (POLLHUP | POLLRDHUP))
			{
				HTTPD_DEBUG_PRINT(
						"Sock %d closed while poll()'d send() to become ready.\n",
						cli_fd
				);
				return -1;
			}
			if (poll_status == 0)
			{
				HTTPD_DEBUG_PRINT("timeout..?\n");
			}
			HTTPD_PRINT_NOTICE("poll()'d for send.\n");
			httpd_client_refresh(client);
			continue;
		}
	}
	HTTPD_DEBUG_PRINT("Send failed, tried 8 times\n");
	return -2;
}

ssize_t httpd_client_write(Httpd_Client *client, const char* buf, size_t buf_len)
{
	//httpd_client_lock(client);
	//assert(client->closed == false);
	assert(client);
	assert(buf_len);

	if (httpd_client_get_status(client) >= CLIENT_CLOSING)
	{
		HTTPD_DEBUG_PRINT("Tried to write to client %p(%d) with status %d\n", client, client->fd, client->status);
		httpd_client_unlock(client);
		return -1;
	}

	httpd_client_refresh(client);
	const int res = _send_wrapper(client, buf, buf_len);
	if (res > 0) {
		HTTPD_DEBUG_PRINT("..wrote %d\n", res);
		client->httpd->stats.bytes_sent += res;
	}
	if (res == -1)
	{
		//HTTPD_DEBUG_PRINT("send() for client %p failed!\n", client);
		//httpd_client_set_status(client, CLIENT_CLOSE_REQD);
		httpd_client_request_close(client);
		//httpd_client_close(client);
		return -1;
	}
	//httpd_client_unlock(client);

	return res;
}

void httpd_client_close(Httpd_Client* client)
{
	if (httpd_client_is_closed(client)) {
		HTTPD_DEBUG_PRINT("Already closed!\n");
		return;
	}
	httpd_client_set_status(client, CLIENT_FREE);
	httpd_client_lock(client);
	HTTPD_DEBUG_PRINT("Closing client %d\n\n", client->fd);
	HTTPD_DEBUG_ASSERT( (unsigned long) syscall(SYS_gettid) == client->httpd->main_thread_id);

	close(client->fd);
	//client->closed = true;
	httpd_client_set_status(client, CLIENT_FREE);
	httpd_client_unlock(client);

}

extern int httpd_client_is_closed(Httpd_Client* client)
{
	return httpd_client_is_status(client, CLIENT_CLOSED);
}

extern void
httpd_client_set_status(Httpd_Client* client, CLIENT_STATUS status)
{
	// mutex lock not really needed since client->status is atomic but it makes valgrind less annoying :--)
	httpd_client_lock(client);
	//HTTPD_DEBUG_PRINT("Changing client's %p(%d) status from %d to %d\n", client, client->fd, client->status, status);
	HTTPD_DEBUG_ASSERT(client->status == CLIENT_FREE || status >= client->status);
	client->status = status;
	httpd_client_unlock(client);
}
extern CLIENT_STATUS httpd_client_get_status(Httpd_Client* client)
{
	return client->status;
}

extern bool httpd_client_is_status(Httpd_Client* client, CLIENT_STATUS status)
{
	bool res = false;
	httpd_client_lock(client);
	res = client->status == status;
	httpd_client_unlock(client);
	return res;
}

extern bool httpd_client_is_status_or_more(Httpd_Client* client, CLIENT_STATUS status)
{
	bool res = false;
	httpd_client_lock(client);
	res = client->status >= status;
	httpd_client_unlock(client);
	return res;
}

extern bool httpd_client_is_status_or_less(Httpd_Client* client, CLIENT_STATUS status)
{
	bool res = false;
	httpd_client_lock(client);
	res = client->status <= status;
	httpd_client_unlock(client);
	return res;
}


extern void
httpd_client_request_close(Httpd_Client* client)
{
	//httpd_client_lock(client);
	if (httpd_client_is_status_or_more(client, CLIENT_CLOSE_REQD))
	//if (httpd_client_get_status(client) >= CLIENT_CLOSE_REQD)
	{
		HTTPD_PRINT_NOTICE("Trying to close already requested for client %p (%d)\n", client, client->fd);
		#ifdef HTTPD_DEBUG
		//print_trace();
		#endif
		return;
	}
	//httpd_client_unlock(client);

	httpd_client_set_status(
		client,
		CLIENT_CLOSE_REQD
	);
}


void httpd_client_start_offing(Httpd_Client* client)
{
	httpd_client_set_status(client, CLIENT_CLOSE_REQD);
}

void httpd_client_terminate(Httpd_Client* client)
{
	HTTPD_DEBUG_PRINT("Terminating %p (%d)\n", client, client->fd);

	httpd_client_set_status(client, CLIENT_CLOSED);
	int fd = client->fd;
	Httpd* httpd = client->httpd;
	Work_Processor* processor = client->processor;
	HTTPD_DEBUG_ASSERT( (unsigned long) syscall(SYS_gettid) == client->httpd->main_thread_id);
	httpd_unregister_client(client->httpd, client);
	//epoll_ctl(httpd->epoll_fd, EPOLL_CTL_DEL, fd, &httpd->ev);
	httpd_client_free(client);
	//httpd_client_unlock(client);
	work_processor_remove_works(processor, (WORK_ID) fd);
	// https://stackoverflow.com/a/8708712
	//httpd_client_close(client);
	close(fd);
	HTTPD_PRINT("closed %d\n", fd);
}




void httpd_client_add_work(Httpd_Client* client, WORKER_FN_TARGET target_fn, void* work_done_cb)
{
	void** params = malloc(1 * sizeof(void*));
	params[0] = client;

	Work* work = work_processor_create_work(
		client->processor,
		target_fn,
		params,
		work_done_cb
	);
	work->id = (WORK_ID) client->fd;

	work_processor_append_work(client->processor, work);
}
