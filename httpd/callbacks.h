#ifndef CALLBACK_H_
#define CALLBACK_H_

#include <stdint.h>

#include "request.h"
#include "httpd.h"


#define NUM_CALLBACKS 6

// Index must always be within NUM_CALLBACKS
typedef enum HTTPD_CALLBACK_ID
{
	HTTPD_CB_ID_PATH_FOUND				= 0,
	HTTPD_CB_ID_HEAD_FINISHED			= 1,
	HTTPD_CB_ID_REQ_CLOSED				= 2,
	HTTPD_CB_ID_PLUGINS_LOADED			= 3,
	HTTPD_CB_ID_CLIENT_REGISTERED		= 4,
	HTTPD_CB_ID_CLIENT_UNREGISTERING	= 5
} HTTPD_CALLBACK_ID;

typedef int	(*FN_HOOK_CB)						(void **params);


typedef int	(*FN_CALLBACK_HEAD_FINISHED)		(Httpd_Request*);
typedef int	(*FN_CALLBACK_URI_FOUND)			(Httpd_Request*);
typedef int	(*FN_CALLBACK_REQ_CLOSED)			(Httpd_Request*);
typedef int	(*FN_CALLBACK_PLUGINS_LOADED)		(Httpd*);
typedef int	(*FN_CALLBACK_CLIENT_REGISTERED)	(Httpd_Client*);
typedef int	(*FN_CALLBACK_CLIENT_UNREGISTING)	(Httpd_Client*);

typedef struct HTTPD_CALLBACK_TABLE_INDEX 
{
	HTTPD_CALLBACK_ID type;
	void** fns;
	uint8_t num_fns;
	pthread_mutex_t lock;
} HTTPD_CALLBACK_TABLE_INDEX;

typedef struct HTTPD_CALLBACK_TABLE 
{
	HTTPD_CALLBACK_TABLE_INDEX* items[NUM_CALLBACKS];
} HTTPD_CALLBACK_TABLE;


void httpd_callback_table_index_init(HTTPD_CALLBACK_TABLE_INDEX* item);
void httpd_callback_table_init(HTTPD_CALLBACK_TABLE* table);
void httpd_callback_table_free(HTTPD_CALLBACK_TABLE* table);

void httpd_callback_table_index_lock(HTTPD_CALLBACK_TABLE_INDEX *index);
void httpd_callback_table_index_unlock(HTTPD_CALLBACK_TABLE_INDEX *index);

uint8_t _httpd_callback_register(const Httpd* httpd, const HTTPD_CALLBACK_ID cb_id, const void* callback);
uint8_t httpd_callback_unregister(const Httpd* httpd, const HTTPD_CALLBACK_ID cb_id, const void* callback);



int httpd_callback_execute_head_finished(Httpd_Request* request);
int httpd_callback_execute_path_found(Httpd_Request* request);
int httpd_callback_execute_req_closed(Httpd_Request* request);
int httpd_callback_execute_plugins_loaded(Httpd* httpd);
int httpd_callback_execute_client_registered(Httpd_Client* client);
int httpd_callback_execute_client_unregistering(Httpd_Client* client);


int httpd_callback_register_head_finished			(Httpd* httpd, FN_CALLBACK_HEAD_FINISHED callback);
int httpd_callback_register_path_found				(Httpd* httpd, FN_CALLBACK_URI_FOUND callback);
int httpd_callback_register_req_closed				(Httpd* httpd, FN_CALLBACK_REQ_CLOSED callback);
int httpd_callback_register_plugins_loaded			(Httpd* httpd, FN_CALLBACK_PLUGINS_LOADED callback);
int httpd_callback_register_client_registerd		(Httpd* httpd, FN_CALLBACK_CLIENT_REGISTERED callback);
int httpd_callback_register_client_unregistering	(Httpd* httpd, FN_CALLBACK_CLIENT_UNREGISTING callback);




#endif