
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>

#include <netinet/in.h>

#include <errno.h>

#include <poll.h>
#include <fcntl.h>

#include "client.h"
#include "httpd.h"
#include "request.h"
#include "header.h"

#include "../misc/deque.h"
#include "../misc/url.h"

static const Httpd_Request_Options HTTPD_DEFAULT_REQ_OPTIONS = {
	.max_body_len = HTTPD_MAX_BODY_SIZE,
	.max_headers_len = HTTPD_MAX_HEADERS_SIZE,
};

void httpd_req_init(Httpd_Request *req)
{
	req->client 		= NULL;

	req->path 			= NULL;
	req->method 		= 0;
	req->fd 			= 0;

	//req->headers 		= NULL;
	req->headers		= realloc(req->headers, HTTPD_MIN_HEADERS_SIZE);
	HTTPD_DEBUG_ASSERT(req->headers);
	//req->headers 		= malloc(HTTPD_MIN_HEADERS_SIZE);
	req->headers_len 	= 0;
	//memset(req->headers, 0, HTTPD_MIN_HEADERS_SIZE);

	//req->head_out_buffer.buf = NULL;
	req->head_out_buffer.len = 0;

	req->next 			= req->prev = NULL;

	req->body 			= NULL;
	req->body_len 		= 0;

	// TODO: Break reference if options are changed.
	req->options 		= (Httpd_Request_Options *) &HTTPD_DEFAULT_REQ_OPTIONS;

	req->headers_ready 	= false;
	req->finished 		= false;
	req->found_provider = false;
	req->headers_out	= false;

}

Httpd_Request *httpd_req_alloc(Httpd_Client* client)
{
	Httpd_Request *request = (Httpd_Request *) deque_pop_item(
		&client->httpd->stores.request_store
	);
	if (request && false)
	{
		HTTPD_DEBUG_PRINT("Popped ununsed request %p from the store.\n", request);
		httpd_req_init(request);
	}
	else
	{
		request = (Httpd_Request*) malloc(sizeof(Httpd_Request));
		request->headers = NULL;
		httpd_req_init(request);
		//request->headers = realloc(request->headers, HTTPD_MIN_HEADERS_SIZE);
	}
	HTTPD_DEBUG_ASSERT(request)
	request->client = NULL;

	//HTTPD_PRINT("req malloc'd\n");
	return request;
}

void httpd_req_free(Httpd_Request *req)
{
	if (req->options != &HTTPD_DEFAULT_REQ_OPTIONS)
	{
		free(req->options);
		req->options = (Httpd_Request_Options*) &HTTPD_DEFAULT_REQ_OPTIONS;
	}
	if (deque_add_item(&req->client->httpd->stores.request_store, req) != 1)
	{
		// Store full or something.
		return httpd_req_free_real(req);
	}
}

void httpd_req_free_real(Httpd_Request *request)
{
	HTTPD_PRINT_NOTICE("Freeing req %p\n", request);
	free(request->headers);
	free(request->path);
	free(request->body);
	free(request);
}

static inline REQ_STATUS httpd_req_read_headers(Httpd_Request *request)
{
	Httpd_Client *client = request->client;
	size_t max_read = HTTPD_MIN_HEADERS_SIZE - 1 - request->headers_len;

	if (request->headers_len + 1 >= HTTPD_MIN_HEADERS_SIZE)
	{
		// Makes sure that there always room for the null terminator by treating 255 as 256.
		const size_t headers_len_aligned = 
			(request->headers_len % 2 == 0) 
				? request->headers_len 
				: (request->headers_len + 1);
		// https://www.geeksforgeeks.org/c-program-to-find-whether-a-no-is-power-of-two/
		bool is_power_of_2 = (ceill(log2(headers_len_aligned)) == floor(log2(headers_len_aligned)));
		if (is_power_of_2)
		{
			//  eg. is 8, 16, 32, 64, 128 or so
			max_read = headers_len_aligned - 1;
			// double capacibility to the next rounded pow2, eg 32 to 64.
			request->headers = realloc(request->headers, headers_len_aligned * 2);
			HTTPD_DEBUG_PRINT("Doubled buffer cap\n");
		}
		else
		{
			const size_t next_power_of2 = 1 << (32 - __builtin_clz(headers_len_aligned)); // jump to next pow2 
			max_read = next_power_of2 - request->headers_len - 1;
			assert(request->headers_len + max_read < next_power_of2);
		}
	}

	if (max_read > 1024)
		max_read = 1024;

	HTTPD_DEBUG_ASSERT(max_read);

	// ssize_t n = read(client->fd, request->headers + request->headers_len, max_read);
	ssize_t n_read = httpd_client_read(client, request->headers + request->headers_len, max_read);
	if (n_read == -1)
	{
		if (errno == EAGAIN || errno == EWOULDBLOCK)
		{
			HTTPD_DEBUG_PRINT("read() done\n");
			return REQ_WOULDBLOCK;
		}
		HTTPD_DEBUG_PRINT("read error\n");
		perror("read()");
		return REQ_CLOSE;
	}

	if (n_read == 0)
	{
		HTTPD_DEBUG_PRINT("read() returned 0 for fd %d, closing req\n", client->fd);
		return REQ_CLOSE;
	}

	request->headers_len += n_read;
	request->headers[request->headers_len] = '\0';

	if (request->headers_len > request->options->max_headers_len)
	{
		HTTPD_DEBUG_PRINT("Request headers over allowed, closing req");
		return REQ_CLOSE;
	}

	if (!request->method)
	{
		// TODO: Do this more efficiently and cleanly. Apparently strn* functions does null terminator check too.
		if (strncmp(request->headers, "GET", 3) == 0)
			request->method = HTTPD_GET;
		else if (strncmp(request->headers, "HEAD", 4) == 0)
			request->method = HTTPD_HEAD;
		else if (strncmp(request->headers, "POST", 4) == 0)
			request->method = HTTPD_POST;
		else if (strncmp(request->headers, "PUT", 3) == 0)
			request->method = HTTPD_PUT;
		else if (strncmp(request->headers, "TRACE", 5) == 0)
			request->method = HTTPD_TRACE;
		else if (strncmp(request->headers, "DELETE", 6) == 0)
			request->method = HTTPD_DELETE;
		else if (strncmp(request->headers, "CONNECT", 7) == 0)
			request->method = HTTPD_CONNECT;
		else if (strncmp(request->headers, "OPTIONS", 7) == 0)
			request->method = HTTPD_OPTIONS;

		if (request->method == 0 && request->headers_len > 8)
		{
			HTTPD_DEBUG_PRINT("Ei metodia!\n");
			return REQ_CLOSE;
		}
		if (request->method == 0) 
		{
			return REQ_OK;
		}
	}

	if (request->method && !request->path)
	{
		// TODO: implement strnchr or something.
		const char *uri_start = strchr(request->headers, ' ') + 1;
		//const char *uri_start = memchr(request->headers, ' ', 16) + 1;
		if (!uri_start)
		{
			if (request->headers_len > 16)
			{
				HTTPD_DEBUG_PRINT("Failed to find uri start in time, closing req.\n");
				return REQ_CLOSE;
			}
			return 0;
		}
		const char *uri_end = strchr(uri_start, ' ');
		if (uri_end)
		{
			const uint32_t uri_len = (size_t)((uintptr_t)uri_end - (uintptr_t)uri_start);
			request->path = malloc(uri_len + 1);
			uint32_t decoded_len = 0;

			if (decode_url_path(
						uri_start,
					   	(uint32_t) uri_len, 
						(unsigned char*) request->path, 
						&decoded_len) 
				== 0)
			{
				free(request->path);
				request->path = 0;

				HTTPD_PRINT_ERROR("Invalid url for req!\n");

				return REQ_CLOSE;
			}
			request->path[decoded_len] = '\0';
			httpd_callback_execute_path_found(request);
			HTTPD_PRINT("Found url: '%s'\n", request->path);
		}
	}

	if (request->path == NULL
		&& strchr(request->headers, '\n')
	)
	{
		HTTPD_DEBUG_PRINT("Viallinen pyyntö.");
		return REQ_CLOSE;
	}

	const char *fresh_chunk= request->headers + (request->headers_len - n_read);
	const char *headers_end_pos = strstr(fresh_chunk, "\r\n\r\n");

	if (headers_end_pos)
	{
		// Don't count "\r\n\r\n" in and anything after that, which is supposed to be body.
		const size_t headers_true_len = (size_t)
			((uintptr_t)headers_end_pos - (uintptr_t)request->headers);
		const int body_len = headers_true_len - request->headers_len + 4;
		// Check if there was body being sent too.
		if (body_len > 0)
		{
			request->body = malloc(body_len);
			strncpy(request->body, headers_end_pos + 4, body_len);
		}

		httpd_client_lock(client);	
		request->headers_len = headers_true_len;
		//request->headers[request->headers_len] = '\0';
		request->headers_ready = true;
		httpd_client_unlock(client);	


		return REQ_HEADERS_FINISHED;
	}

	return REQ_OK;
}

int httpd_req_read(Httpd_Client *client, Httpd_Request** _request)
{
	if (httpd_client_is_closed(client)) {
		HTTPD_DEBUG_PRINT("%p/%d jo suljettu?\n", client, client->fd);
		return 0;
	}

	if (client->request == NULL)
	{
		client->request = httpd_req_alloc(client);
		HTTPD_DEBUG_ASSERT(client->request);
		//httpd_req_init(client->request);
		client->request->client = client;
		HTTPD_DEBUG_PRINT("Created req\n");
	}
	*_request = client->request;

	for (int i = 0; i < 8 && httpd_client_is_status(client, CLIENT_READING) ; i++)
	{
		//HTTPD_PRINT("Luetaan fd %d...\n", client->fd);
		//if (client->closed) break;
		//assert(client->closed == false);
		if (client->request->headers_ready == false)
		{
			REQ_STATUS status = httpd_req_read_headers(client->request);
			switch (status)
			{
				case REQ_WOULDBLOCK:
					return REQ_WOULDBLOCK;
				case REQ_CLOSE:
					//HTTPD_PRINT("Suljetaan %d..\n", client->fd);
					//httpd_client_terminate(client);
					//HTTPD_PRINT("..suljettu!\n");
					httpd_client_request_close(client);
					break;
				case REQ_HEADERS_FINISHED:
					httpd_callback_execute_head_finished(client->request);
					break;
				case REQ_OK:
				case REQ_BODY_FINISHED:
					continue;
			}
			
		}
	}

	return 1;
}

#include <ctype.h>
#include <stdio.h>

void lowercase (char* s) 
{
	while(*s != '\0')
	{
		*s = tolower(*s);
		s++;
	}
}

extern int httpd_req_write(Httpd_Request *request, const char *buf, ssize_t buflen)
{
	if (buflen == 0) buflen = strlen(buf);
	return (int) httpd_client_write(request->client, buf, buflen);
}


//  https://en.wikipedia.org/wiki/Chunked_transfer_encoding#Example
extern int httpd_req_write_chunk(
		Httpd_Request *request,
		const char *buf,
		const ssize_t buf_size
	)
{
	//if (buf_size == 0) buf_size = strlen(buf);
	HTTPD_DEBUG_ASSERT(buf_size >= 0);
		
	// Makes sure hex string fits into 4 bytes.
	assert(buf_size <= UINT16_MAX);
	char out_chunk[8 + 2 + buf_size + 2]; // hexstr + \r\n + buf + \r\n
	//memset(out_chunk, 0, sizeof(out_chunk));

	// Get <hex>\r\n and save length.
	char head[8];
	const int chunk_head_len = snprintf(
		(char*) head,
		sizeof(head),
		"%02x\r\n", 
		(int) buf_size
	);

	HTTPD_DEBUG_PRINT("Head: '%s', len: %d\n", head, chunk_head_len);
	HTTPD_DEBUG_ASSERT(chunk_head_len);

	// Append head
	memcpy(out_chunk, head, chunk_head_len);
	// Append buffer
	memcpy(out_chunk + (char) chunk_head_len, buf, buf_size);   
	// Append the final \r\n 
	memcpy(out_chunk + chunk_head_len + buf_size, "\r\n", 2);

	HTTPD_DEBUG_ASSERT(request->headers_ready == true);

	const int chunk_size = chunk_head_len + buf_size + 2;
	/*HTTPD_DEBUG_PRINT(
			"Given buffer size: %d | Toal frame size: %d | strlen(out_chunk): %d, sizeof(out_chunk): %d\n", 
			(int) buf_size,
			chunk_size,
			(int)strlen((char*) out_chunk), 
			(int)sizeof(out_chunk)
	);*/

	return httpd_client_write(request->client, (char*) out_chunk, chunk_size);
}

extern int httpd_req_end_chunked(Httpd_Request *request)
{
	HTTPD_DEBUG_PRINT("Ending %d's chunked response\n", request->client->fd);
	static const char* chunk_tail = "0x0\r\n\r\n"; 
	return httpd_client_write(
			request->client, 
			chunk_tail,
			//sizeof(chunk_tail)
			7
	);
}

extern int httpd_req_flush(Httpd_Request* request)
{

	if (request->head_out_buffer.len == 0)
		return 0;

	//HTTPD_PRINT("Writing '%.*s'\n", request->head_out_buffer.len, request->head_out_buffer.buf)
	ssize_t bytes_written = httpd_client_write(request->client, request->head_out_buffer.buf, request->head_out_buffer.len);

	if (bytes_written == -1)
	{
		return -1;
	}

	HTTPD_DEBUG_ASSERT(bytes_written == request->head_out_buffer.len);
	HTTPD_PRINT("Flushing headers..\n");
	request->head_out_buffer.len = 0;

	return 1;
}

extern int httpd_req_write_header(Httpd_Request *request, const char *key, const char* val)
{

	char buf[strlen(key) + 2 + strlen(val) + 3];
	memset(buf, 0, sizeof(buf));
	uint32_t bytes_appended = 0;
	ssize_t buf_len = sprintf(buf, "%s: %s\r\n", key, val);


	//HTTPD_DEBUG_PRINT("Appending header %s:%s\n", key, val);

	while (buf_len > 0)
	{
		const int buf_cap = HEADER_BUF_MAX_LEN - request->head_out_buffer.len;
		const int bytes_to_append = (buf_len > buf_cap) ? (buf_cap - buf_len) : buf_len;

		//HTTPD_PRINT("len %d, cap: %d\n", (int) buf_len, (int) buf_cap );

		if (buf_cap == 0)
		{
			httpd_req_flush(request);
			continue;
		}
		strncpy(
			request->head_out_buffer.buf + request->head_out_buffer.len,
			buf + bytes_appended,
			bytes_to_append
		);

		request->head_out_buffer.len += bytes_to_append; 
		buf_len -= bytes_to_append;
		bytes_appended += bytes_to_append;

	}

	return 0;
}

void httpd_req_end_headers(Httpd_Request* request)
{
	HTTPD_PRINT_NOTICE("Ending headers for request %s\n", request->path);
	if (request->headers_out)
	{
		HTTPD_DEBUG_PRINT("Headers already out for path '%s' (fd: %d)\n", request->path, request->client->fd);
		print_trace();
	}
	if (request->head_out_buffer.len)
	{
		httpd_req_flush(request);
	}
	request->headers_out = true;
	httpd_req_write(request, "\r\n", 2);
}

int httpd_req_finished(Httpd_Request *request)
{
	httpd_client_remove_req(request->client, request);
	request->finished = true;

	return 1;
}

