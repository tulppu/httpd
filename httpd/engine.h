#ifndef _ENGINE_H_
#define _ENGINE_H_

#include <sys/epoll.h>

#include "httpd.h"


#define EVENT_TYPE_CALLBACK 1


int httpd_engine_register_event(Httpd* httpd, int fd, struct epoll_event *ev);
int httpd_engine_unpoll_event(Httpd* httpd, int fd);


void httpd_engine_poll_events(struct Httpd* httpd);
void httpd_engine_register_waker_fd(Httpd* httpd);
void httpd_engine_wake_up(Httpd* httpd);
/*
 * Adds work to the main loop, usefull for switching execution from worker thread back to the main.
 * httpd_engine_wake_up() should be called after should the work be executed without (much of) delay.
 */
void httpd_engine_add_work(Httpd_Client* client, WORKER_FN_TARGET target_fn, WORKER_FN_FINAL_CB* work_done_cb);




// Callbacked events. Not thread safe, use from main thread only.
typedef int (FN_EVENT_FIRED_CB)(Httpd* httpd, int fd, struct epoll_event* ev);
typedef struct Httpd_Engine_Callbackd_Event
{
	int fd;
	FN_EVENT_FIRED_CB *cb;
} Httpd_Engine_Callbackd_Event;


typedef struct Httpd_Engine_Callbackd_Event_Store
{
	Httpd_Engine_Callbackd_Event** items;
	uint32_t num_items;
} Httpd_Engine_Event_Store;

void
httpd_engine_store_init(
		Httpd_Engine_Event_Store* store);

Httpd_Engine_Callbackd_Event*
httpd_engine_store_get(
		Httpd_Engine_Event_Store* store, 
		int fd);

extern void 					
httpd_engine_store_remove(
		Httpd_Engine_Event_Store* store, 
		Httpd_Engine_Callbackd_Event *event);


extern void 
httpd_engine_event_register_on_cb(
		Httpd* httpd, 
		int fd, 
		struct epoll_event* ev, 
		FN_EVENT_FIRED_CB* cb);

extern void 
httpd_engine_free(
		Httpd* httpd);

void 
httpd_engine_start(
		struct Httpd* httpd);


typedef struct Httpd_Engine_Event_Data
{
	union 
	{	
		uint64_t store;
		struct {
			unsigned long long data:48;
			uint8_t flags:8;
			uint8_t type:8;
		} __attribute__((packed)) items;
	};
} __attribute__((packed))
Httpd_Engine_Event_Data;

struct epoll_event httpd_engine_create_event(unsigned long long u48_data, uint8_t u8_flags, uint8_t u8_type);

#endif
