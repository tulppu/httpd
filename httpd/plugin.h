#ifndef PLUGIN_H_
#define PLUGIN_H_

#include "httpd.h"


// First props must be 2 unused pointers for deque.
typedef struct Httpd_Plugin
{
	void* next;
	void* prev;

	char *plugin_name;
	char *plugin_desc;
	char *plugin_path;

	Httpd* httpd;

	void* exit_func;
	void* handle;
} Httpd_Plugin;


typedef int		(FN_PLUGIN_INIT)(Httpd*, Httpd_Plugin*);
typedef void	(FN_PLUGIN_EXIT)(Httpd_Plugin*);


void httpd_plugin_load_dir(Httpd* httpd, char* dir_path);
void httpd_plugin_load_file(Httpd* httpd, char* file_path);

Httpd_Plugin* httpd_plugin_get_by_path(Httpd* httpd, const char* path);
//void httpd_pluing_init(Httpd_Plugin** plugin);

Httpd_Plugin* httpd_plugin_alloc();
void httpd_plugin_init(Httpd* httpd,  Httpd_Plugin* plugin, char* plugin_name, char* plugin_desc);
void httpd_plugin_register(Httpd* httpd, Httpd_Plugin* plugin);
void httpd_plugin_free(Httpd_Plugin* plugin);
void httpd_plugin_unload(Httpd* httpd, Httpd_Plugin* plugin);
void httpd_plugin_unload_all(Httpd* httpd);

#endif
