#define _GNU_SOURCE 
#include <dlfcn.h>

#include <assert.h>
#include <pthread.h>
#include "callbacks.h"


// Init and building.
void httpd_callback_table_index_init(HTTPD_CALLBACK_TABLE_INDEX* item)
{
	item->fns = NULL;
	item->num_fns = 0;
	pthread_mutex_init(&item->lock, NULL);
}

HTTPD_CALLBACK_TABLE_INDEX* httpd_callback_table_index_create(HTTPD_CALLBACK_TABLE *table)
{
	HTTPD_CALLBACK_TABLE_INDEX* index = (HTTPD_CALLBACK_TABLE_INDEX*) malloc(sizeof(HTTPD_CALLBACK_TABLE_INDEX));
	httpd_callback_table_index_init(index);

	return index;
}

void httpd_callback_table_index_lock(HTTPD_CALLBACK_TABLE_INDEX *index)
{
	pthread_mutex_lock(&index->lock);
}

void httpd_callback_table_index_unlock(HTTPD_CALLBACK_TABLE_INDEX *index)
{
	pthread_mutex_unlock(&index->lock);
}

void httpd_callback_table_index_free(HTTPD_CALLBACK_TABLE_INDEX *index)
{
	free(index->fns);
	free(index);
}

void httpd_callback_table_init(HTTPD_CALLBACK_TABLE * table)
{
	for (int i = 0; i < NUM_CALLBACKS; i++)
	{
		table->items[i] = httpd_callback_table_index_create(table);
	}
}

void httpd_callback_table_free(HTTPD_CALLBACK_TABLE* table)
{
	for (int i = 0; i < NUM_CALLBACKS; i++)
	{
		httpd_callback_table_index_free(table->items[i]);
	}
	free(table);
}

uint8_t _httpd_callback_register(const Httpd* httpd, const HTTPD_CALLBACK_ID cb_id, const void* callback)
{

	HTTPD_CALLBACK_TABLE* table = httpd->callback_table;
	HTTPD_CALLBACK_TABLE_INDEX* index = table->items[cb_id];
	HTTPD_DEBUG_ASSERT(index);

	index->fns = (void**) realloc(index->fns, (index->num_fns + 1) * sizeof(callback));
	index->fns[index->num_fns] = (void*) callback;
	index->num_fns++;

	return 1;
}

// Registerings
inline static void httpd_callback_register(HTTPD_CALLBACK_TABLE_INDEX* index, void* callback )
{
	index->fns = (void**) realloc(index->fns, (index->num_fns + 1) * sizeof(callback));
	index->fns[index->num_fns] = (void*) callback;
	index->num_fns++;
}

// Clears index and readds callbacks. Function being inefficeint shouldn't matter.
uint8_t httpd_callback_unregister(const Httpd* httpd, const HTTPD_CALLBACK_ID cb_id, const void* callback)
{
	//HTTPD_DEBUG_PRINT("Unregistering callback-index %d\n", cb_id);

	HTTPD_CALLBACK_TABLE* table = httpd->callback_table;
	HTTPD_CALLBACK_TABLE_INDEX* index = table->items[cb_id];
	HTTPD_DEBUG_ASSERT(index);

	httpd_callback_table_index_lock(index);
	void** fns_old = index->fns;
	uint8_t num_fns_old = index->num_fns;

	uint8_t items_removed = 0;

	index->fns = NULL;
	index->num_fns = 0;

	for (uint8_t i = 0; i < num_fns_old; i++)
	{
		if (fns_old[i] != callback)
		{
			httpd_callback_register(index, (void*) fns_old[i]);
		} else items_removed++;
	}
	free(fns_old);

	#ifdef HTTPD_DEBUG
	if (items_removed == 0) {
		HTTPD_DEBUG_PRINT("Was asked to remove cb %p from index %d but callback wasn't indexed.\n", callback, index->type);
	} else if (items_removed > 1) {
		HTTPD_DEBUG_PRINT("Was asked to remove cb %p from index %d and found %d callbacks to remove", callback, index->type, items_removed);
	} else {
		HTTPD_DEBUG_PRINT("Seemingly succesfully unregistered callback-index %d\n", cb_id);
	}
	#endif

	httpd_callback_table_index_unlock(index);
	return items_removed;
}

int httpd_callback_register_head_finished(struct Httpd* httpd, FN_CALLBACK_HEAD_FINISHED callback)
{
	return _httpd_callback_register(httpd, HTTPD_CB_ID_HEAD_FINISHED, (const void*) callback);
}
int httpd_callback_register_path_found(struct Httpd* httpd, FN_CALLBACK_URI_FOUND callback)
{
	return _httpd_callback_register(httpd, HTTPD_CB_ID_PATH_FOUND, (const void*) callback);
}

int httpd_callback_register_req_closed(struct Httpd* httpd, FN_CALLBACK_REQ_CLOSED callback)
{
	return _httpd_callback_register(httpd, HTTPD_CB_ID_REQ_CLOSED, (const void*) callback);
}

int httpd_callback_register_plugins_loaded(struct Httpd* httpd, FN_CALLBACK_PLUGINS_LOADED callback)
{
	return _httpd_callback_register(httpd, HTTPD_CB_ID_PLUGINS_LOADED, (const void*) callback);
}

int httpd_callback_register_client_registerd(Httpd* httpd, FN_CALLBACK_CLIENT_REGISTERED callback)
{
	return _httpd_callback_register(httpd, HTTPD_CB_ID_CLIENT_REGISTERED, (const void*) callback);
}

int httpd_callback_register_client_unregistering(Httpd* httpd, FN_CALLBACK_CLIENT_UNREGISTING callback)
{
	return _httpd_callback_register(httpd, HTTPD_CB_ID_CLIENT_UNREGISTERING, (const void*) callback);
}

// Executors
int httpd_callback_execute(Httpd* httpd, HTTPD_CALLBACK_ID id, void** args, uint8_t n_args)
{
	HTTPD_CALLBACK_TABLE_INDEX* index = httpd->callback_table->items[id];
	for (uint8_t i = 0; i < index->num_fns; i++)
	{
		switch (id)
		{
			// So far all callbacks takes single pointer as parameter.
			case HTTPD_CB_ID_PATH_FOUND:
			case HTTPD_CB_ID_HEAD_FINISHED:
			case HTTPD_CB_ID_REQ_CLOSED:
			case HTTPD_CB_ID_PLUGINS_LOADED:
			case HTTPD_CB_ID_CLIENT_REGISTERED:
			case HTTPD_CB_ID_CLIENT_UNREGISTERING:
				int ((*cb)(void*)) =  index->fns[i];
				cb(args[0]);
				break;
			default:
				HTTPD_PRINT_CRITICAL("Unknown callback id %d\n", id);
				exit(1);
		}
	}
	return 1;
}

int httpd_callback_execute_path_found(Httpd_Request* request)
{
	return httpd_callback_execute(request->client->httpd, HTTPD_CB_ID_PATH_FOUND, (void**) &request, 1);
}

int httpd_callback_execute_head_finished(Httpd_Request* request) 
{
	return httpd_callback_execute(request->client->httpd, HTTPD_CB_ID_HEAD_FINISHED, (void**) &request, 1);
}

int httpd_callback_execute_req_closed(Httpd_Request* request) 
{
	return httpd_callback_execute(request->client->httpd, HTTPD_CB_ID_REQ_CLOSED, (void**) &request, 1);
}

int httpd_callback_execute_plugins_loaded(Httpd* httpd) 
{
	return httpd_callback_execute(httpd, HTTPD_CB_ID_PLUGINS_LOADED, (void**) &httpd, 1);
}

int httpd_callback_execute_client_registered(Httpd_Client* client)
{
	return httpd_callback_execute(client->httpd,  HTTPD_CB_ID_CLIENT_REGISTERED , (void**) &client, 1);
}

int httpd_callback_execute_client_unregistering(Httpd_Client* client)
{
	return httpd_callback_execute(client->httpd,  HTTPD_CB_ID_CLIENT_UNREGISTERING, (void**) &client, 1);
}
