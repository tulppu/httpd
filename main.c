#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "httpd/httpd.h"
#include "httpd/request.h"
#include "httpd/plugin.h"

void drop_priviledges(const char *target_usr)
{
	struct passwd *p;

	if ((p = getpwnam(target_usr)) == NULL)
	{
		perror(target_usr);
		exit(0);
	}
	HTTPD_DEBUG_PRINT("Asetetaan prosessi %d:%d:lle\n", (int)p->pw_uid, (int)p->pw_gid);
	// https://stackoverflow.com/questions/3357737/dropping-root-privileges
	// if (getuid() == 0) {
	/* process is running as root, drop privileges */
	if (setgid(p->pw_gid) != 0)
	{
		HTTPD_PRINT_ERROR("setgid: Unable to drop group privileges: %s", strerror(errno));
		exit(1);
	}
	if (setuid(p->pw_uid) != 0)
	{
		HTTPD_PRINT_ERROR("setuid: Unable to drop user privileges: %s", strerror(errno));
		exit(1);
	}

	assert(setuid(0) != 0);
	assert(getuid() != 0);
	assert(getgid() != 0);
	assert(getuid() == p->pw_uid);

	HTTPD_DEBUG_PRINT("Vaihdettu %d\n", getuid());
}

#include <signal.h>

Httpd *httpd = (Httpd *) NULL;

void sigint_handler(int sig)
{
	HTTPD_PRINT_NOTICE("Caught SIGINT!\n");
	if (httpd)
	{
		if (httpd->status == STATUS_CLOSING || httpd->status == STATUS_CLOSED)
		{
			HTTPD_DEBUG_PRINT("Hyvästi ystävä!\n");
			exit(0);
		}
		httpd_free(httpd);
		exit(0);
	}
	exit(0);
}
//uint8_t __HTTPD_VERBOSE_FLAGS = 0;

#include "httpd/engine.h"
#include "httpd/header.h"

int main()
{
	free(malloc(1024 * 32));

	__HTTPD_VERBOSE_FLAGS = UINT8_MAX;
	//__HTTPD_VERBOSE_FLAGS = 0;
	//HTTPD_LOGGING_ENABLE_TYPE(LOG_NOTICE);
	//HTTPD_LOGGING_ENABLE_TYPE(LOG_);

	httpd = (Httpd *)malloc(sizeof(Httpd));

	signal(SIGPIPE, SIG_IGN);
	signal(SIGINT, sigint_handler);

	httpd_init(httpd);

	worker_start(&httpd->worker);
	httpd_bind(httpd);
	if (getuid() == 0)
	{
		drop_priviledges("www-data");
	}
	httpd_plugin_load_dir(httpd, "build/plugins");
	httpd_listen(httpd);
	httpd_engine_register_waker_fd(httpd);
	httpd_engine_start(httpd);



	return 0;
}
