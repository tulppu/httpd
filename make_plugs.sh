#!/bin/sh

cd plugins
for d in */;do
	  cd "$d"
	  if [ -f Makefile ]; then
		echo Making plugin "$d"
		make
		ret_code=$?
		if [ $ret_code != 0 ]
		then
			echo "Make failed, stopping";
			exit -1;
		fi
	  fi
	  cd ..
done;

exit 0
