const filter	= document.createElement("input");
const links		= document.querySelectorAll("a");
const style		= document.createElement("style");
style.innerText = ".hidden { display: none; }"

document.head.appendChild(style);

document.body.insertBefore(
	filter,
	document.body.firstElementChild.nextElementSibling,
);

filter.placeholder = "Filter";
filter.style.display = "block";

filter.addEventListener("keyup", (e) => {
	const q = filter.value.toLocaleLowerCase();
	links.forEach((link) => {
		link.id = link.id || link.innerText.toLocaleLowerCase();
		if (link.id.includes(q) || q.length === 0)
		{
			link.classList.remove("hidden");
		} else
		{
			link.classList.add("hidden");
		}
	})
});

window.setTimeout(() => filter.focus());