# Compiler and Linker
CC = gcc

# Compiler Flags
CFLAGS = -std=gnu17 -Wall -Wextra -Wfatal-errors -Wshadow -g
CFLAGS += -Wno-unused-variable -Wno-unused-parameter -Wno-format-security
CFLAGS += -fmax-errors=1 -Werror -lpthread

# Linker Flags
LDFLAGS = -Wl,--export-dynamic -lpthread -lm -ldl

# Static Library
STATIC_LIB = ./build/libhttpd.a

# Directories
SRCDIR = httpd
MISCDIR = misc
BUILDDIR = build

# Phony Targets
.PHONY: all clean plugins

# Default Target
all: a.out plugins

rebuild:
	make clean
	make

# Main Executable
a.out: main.o $(STATIC_LIB)
	$(CC) $(CFLAGS) main.o -o $@ -Wl,--whole-archive $(STATIC_LIB) -Wl,--no-whole-archive $(LDFLAGS)

# Plugins
plugins:
	./make_plugs.sh

# Static Library
$(STATIC_LIB): $(patsubst %, $(BUILDDIR)/%, engine.o httpd.o request.o client.o worker.o header.o debug.o deque.o plugin.o callbacks.o endpoint.o mime.o url.o dyn_list.o)
	ar -rcT $(STATIC_LIB) $^

# Object Files
$(BUILDDIR)/%.o: $(SRCDIR)/%.c $(SRCDIR)/%.h $(SRCDIR)/httpd.h
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILDDIR)/%.o: $(MISCDIR)/%.c $(MISCDIR)/%.h
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) -c $< -o $@

# Clean Build Files
clean:
	rm -f a.out main.o
	rm -f $(BUILDDIR)/*.o
	rm -f $(BUILDDIR)/*.a
	find plugins/ -type f -name "*.o" -delete
	find plugins/ -type f -name "*.a" -delete
	find $(BUILDDIR)/ -type f -name "*.so" -delete
