#!/bin/bash

function post_start {
	sleep 1
	printf "\nExecuting some commands"
	printf "\r\n"
	#curl --silent --output /dev/null http://localhost:4488/favicon.ico
	curl --silent --output /dev/null/ http://localhost:4488/tereve_lazy/index.html
	#--verbose -I
	printf "\r\n"
}


function monitor_httpd {
	if [ -f pid.txt ]; then
		echo "Monitoring httpd"
		wait 
		#wait $(cat pid.txt)
		exit_code = $?
		printf "\n\nProcess exited with code $?\n"
		if [[ ! $exit_code == 0 ]]; then
			printf "\nProcess chashed\n?";
			printf "Restarting.."
			#start_httpd
		fi
		printf "\n\n"
	fi
}

function start_httpd {
  echo "Starting httpd.."
  if [ -f ./a.out ]; then
	#./a.out >dev/null & echo $! > pid.txt
	./a.out > /dev/null & echo $! > pid.txt
	echo "..started httpd."
	#monitor_httpd
	post_start &
  else
	echo "..binary not found."
  fi
}

function kill_httpd {
  echo "Killing httpd"
  if [ -f pid.txt ]; then
    kill $(cat pid.txt)
    rm pid.txt
	echo "..killed"
  else
	echo "pid.txt not found"
  fi
}

# https://stackoverflow.com/a/60806295
function gen_compile_commands {
	make --always-make --dry-run \
	 | grep -wE 'gcc|g\+\+' \
	 | grep -w '\-c' \
	 | jq -nR '[inputs|{directory:".", command:., file: match(" [^ ]+$").string[1:]}]' \
	 > compile_commands.json
}

function restart_httpd {
	kill_httpd
	sleep 0.5
	start_httpd
}

function gen_ctags {
	ctags --recurse=yes --exclude=.git --exclude=build* --exclude=log/*
}
cd ..

make
gen_ctags
ulimit -n 2048
start_httpd &

echo Starting monitoring..

#inotifywait build/ build/plugins a.out -m  -e close_write | 
#while read filename;
#do
#  echo $filename ;
#  kill_httpd;
#  start_httpd;
#done

inotifywait main.c Makefile plugins/ httpd/ misc/ -mr  -e close_write | 
while read filename;
do
	echo $filename ;
	if [[ $filename =~ Makefile|.(c|h)$ ]]
	then
		if [[ $filename =~ Makefile$ ]] then
			gen_compile_commands
		fi
		echo "Source file changed!"
		kill_httpd
		if [[ $filename =~ .h$ ]]
		then
			# In case struct or macro used by plugin changed.
			echo "Header changed, rebuilding everything."
			make rebuild
		else
			make
		fi	
		ret_code=$?
		if [ $ret_code == 0 ]
		then
			ctags --recurse=yes --exclude=.git --exclude=build* --exclude=log/*
			if [[ $? == 0 ]] then
				echo "Generated CTags";
			fi

			sleep 0.3
			#restart_httpd
			start_httpd
		else
			echo "Build failed, won't restart";
		fi
	fi
done
